@extends('layouts.app')

@section('contents')

    <div class="mad_section">

        <div class="container">

            <div class="mad_section_offset">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="align_center">
                            <h3 class="mad_title_style1">Leave</h3>
                            <h2>Your Feedback</h2>
                        </div>

                        <form id="contact_form" class="contact_form" method="post" action="{{route('reviewstore')}}">
                           {!! csrf_field() !!}
                            <ul>
                                <li class="row">

                                    <div class="col-sm-6">
                                        <input type="text" name="name" placeholder="Name" id="name">
                                        @if ($errors->has('name'))
                                            <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="email" name="email" id="email" placeholder="Email (Will not be published)">
                                        @if ($errors->has('email'))
                                            <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </li>
                                <li class="row">

                                    <div class="col-sm-6">
                                        <input type="text" name="title" id="title" placeholder="Title">
                                        @if ($errors->has('title'))
                                            <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" name="designation" id="designation" placeholder="Designation (Optional)">
                                        @if ($errors->has('designation'))
                                            <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('designation') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </li>
                                <li>
                                    <textarea name="message" id="message" placeholder="Message..."></textarea>
                                    @if ($errors->has('message'))
                                        <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                                </li>
                                <li>
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <img src="{{url('captcha/image/jpg')}}" alt="Captcha">
                                        </div>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control {{ $errors->has('captcha') ? ' is-invalid' : '' }}" placeholder="Enter Captcha" name="captcha" id="captcha" required>
                                            @if ($errors->has('captcha'))
                                                <span class="help-block alert_box error">
                                                    <strong>{{ $errors->first('captcha') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="align_center">
                                <button type="submit" class="mad_button">Sumbit Your Comment</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div class="align_center">
                        <h3 class="mad_title_style1">What</h3>
                        <h2 class="mad_item_offset_2">Customers Say</h2>
                    </div>

                    <div class="mad_flex_list mad_testimonials_section">
                        <div class="row">
                            @foreach($reviews as $review)
                            <div class="col-sm-4">

                                <div class="mad_widget widget_quote style2">

                                    <h5>{{$review->title}}</h5>

                                    <p>{!! Str::limit($review->details,178) !!}</p>

                                    <div class="mad_blockquote_author">- {{$review->name}}</div>
                                    @if($review->designation != null)
                                    <p class="font-weight-light text-center">{{$review->designation}}</p>
                                    @endif
                                </div>

                            </div>
                            @endforeach

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection