@extends('layouts.app')

@section('contents')

    <div class="mad_section">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <!-- Content Title -->
                    <div class="align_center mad_item_offset_2">
                        <h3 class="mad_title_style1">{!! nl2br(Label::ofValue('membership:page_head')) !!}</h3>
                        <h3>{!! nl2br(Label::ofValue('membership:page_subhead')) !!}</h3>
                        <p class="mad_item_offset_none">{!! nl2br(Label::ofValue('membership:page_head_body')) !!}</p>
                    </div>

                    <!-- Pricing Table -->
                    <h3>Membership price list</h3>
                </div>
                <div class="mad_item_offset_2">
                    <div class="row">
                        @foreach($memberships as $membership)
                        <div class="col-md-3 col-sm-6" style="margin-bottom: 10px;">
                            <div class="p_table">
                                <div class="price">
                                    <div>&pound;{{$membership->price}} <span>00</span></div>
                                </div>
                                <div class="p_table_text">
                                    <h5 class="widget_title">{{$membership->title}}</h5>
                                    <div class="widget_categories">
                                        <p class="plan-text">{!! Str::limit($membership->details,175) !!}</p>
                                        <!-- <ul>
                                          <li>Photoshop Designs</li>
                                          <li>CMS Web Templates</li>
                                          <li>HTML Templates</li>
                                          <li>Graphic Designs</li>
                                          <li>WordPress</li>
                                        </ul> -->
                                    </div>
                                    <a href="{{route('membership.details',encrypt($membership->id))}}" class="mad_button">Join Us</a>
                                </div>
                            </div>

                        </div>
                        @endforeach

                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Call to Action -->
    <div class="full_width_bg_pattern_2 full_screen bg_pattern_dark mad_banner_1 size2 mad_item_offset">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="v_align_center_section">
                        <div><h2 class="mad_title_style1">Please Read our terms and condition</h2></div>
                        <div class="align_right"><a href="#" class="mad_button style2">Read All</a></div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <!-- Questions or Concerns -->
                <div class="mad_item_offset_2">
                    <div class="align_center"><h4>FAQ (Frequently Asked Question(s))</h4></div>
                    <div class="row">
                        <div class="col-sm-6">

                            <div class="mad_item_offset_4">
                                <h5 class="mad_title_normal">WHERE DO I GET STARTED?</h5>
                                <p class="mad_text_style1 mad_item_offset_none">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                            </div>
                            <div class="mad_item_offset_4">
                                <h5 class="mad_title_normal">WHAT HAPPENS AT THE END OF THE PAID PERIOD?</h5>
                                <p class="mad_text_style1 mad_item_offset_none">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi.</p>
                            </div>
                            <div>
                                <h5 class="mad_title_normal">DO YOU ACCEPT PURCHASE ORDERS?</h5>
                                <p class="mad_text_style1 mad_item_offset_none">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidat proident, sunt in culpa qui officia mollit anim id est. </p>
                            </div>

                        </div>
                        <div class="col-sm-6">

                            <div class="mad_item_offset_4">
                                <h5 class="mad_title_normal">HOW CAN I CANCEL MY SUBSCRIPTION?</h5>
                                <p class="mad_text_style1 mad_item_offset_none">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.</p>
                            </div>
                            <div class="mad_item_offset_4">
                                <h5 class="mad_title_normal">WHAT ABOUT ONE TIME EVENTS?</h5>
                                <p class="mad_text_style1 mad_item_offset_none">Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia. </p>
                            </div>
                            <div>
                                <h5 class="mad_title_normal">DO I HAVE TO PAY FOR EACH MAG I WANT TO PUBLISH?</h5>
                                <p class="mad_text_style1 mad_item_offset_none">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidat proident, sunt in culpa qui officia mollit anim id est. </p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">

                        <a href="#" class="full_width_bg_pattern bg_pattern_red mad_banner_1 size2">
                            <div class="v_align_center_section">
                                <div><h2 class="mad_title_style1 icon-info-circled-alt">Have More Questions?</h2></div>
                            </div>
                        </a>

                    </div>
                    <div class="col-md-6">

                        <a href="#" class="full_width_bg_pattern_2 bg_pattern_dark mad_banner_1 size2">
                            <div class="v_align_center_section">
                                <div><h2 class="mad_title_style1 icon-cog-outline">Open a Ticket</h2></div>
                            </div>
                        </a>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection