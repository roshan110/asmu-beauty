@extends('layouts.app_secondary')
@section('page_header')
{{$membershiped->title}}
@endsection

@section('contents')


    <div class="mad_section_2">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <div class="align_center mad_item_offset_2">
                        <h3 class="mad_title_style1">{{$membershiped->title}}</h3>
                        <p class="mad_item_offset_none">{!! nl2br(Label::ofValue('membership:innerpage_head_body')) !!}</p>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="align_center mad_item_offset_2">
                                <h4 class="mad_title_style1">Membership Enrollment Details</h4>
                                {{--<p class="mad_item_offset_none">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500ss.Lorem Ipsum has been the industry's standard dummy text.</p>--}}
                            </div>
                            <form id="contact_form" class="contact_form" action="{{route('membership.checkout')}}" method="POST" role="form">
                                {!! csrf_field() !!}
                                <ul>
                                    <li>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <div class="custom_select" style="margin-bottom: 20px;">
                                                    <div class="select_title">Select Service</div>
                                                    <ul id="menu_type" class="select_list"></ul>
                                                    <select class="d_none" name="service">
                                                        @foreach($services_c as $sc)
                                                            @php $service_value=ucwords(mb_strtolower($sc->title)); @endphp
                                                        <option value="{{$service_value}}">{{$service_value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control {{ $errors->has('fname') ? ' is-invalid' : '' }}" placeholder="First Name" name="fname" id="fname" required>
                                                @if ($errors->has('fname'))
                                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control {{ $errors->has('lname') ? ' is-invalid' : '' }}" placeholder="Last Name" name="lname" id="lname" required>
                                                @if ($errors->has('lname'))
                                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" id="email" required>
                                                @if ($errors->has('email'))
                                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="Address" name="address" id="address" required>
                                                @if ($errors->has('address'))
                                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group row">
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="Phone" name="phone" id="phone" required>
                                                @if ($errors->has('phone'))
                                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control {{ $errors->has('postal') ? ' is-invalid' : '' }}" placeholder="Postal" name="postal" id="postal" required>
                                                @if ($errors->has('postal'))
                                                    <span class="help-block alert_box error">
                                                    <strong>{{ $errors->first('postal') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group row">
                                            <div class="col-sm-2">
                                                <img src="{{url('captcha/image/jpg')}}" alt="Captcha">
                                            </div>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control {{ $errors->has('captcha') ? ' is-invalid' : '' }}" placeholder="Enter Captcha" name="captcha" id="captcha" required>
                                                @if ($errors->has('captcha'))
                                                    <span class="help-block alert_box error">
                                                    <strong>{{ $errors->first('captcha') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </li>


                                </ul>
                                <input type="hidden" name="membership_id" value="{{$membershiped->id}}">
                                <div class="membership_d_joinbtn">
                                    <button type="submit" class="mad_button small_button">Join</button>
                                </div>

                            </form>
                        </div>
                        <div class="col-sm-6 membership_info_blocked" >
                            <div class="align_center mad_item_offset_2">
                                <h4 class="mad_title_style1">Membership Info</h4>
                            </div>
                            @if($membershiped->image != null)
                                <figure class="mad_item_offset_2">
                                    <img src="{{$membershiped->image('769x295')}}" alt="{{$membershiped->title}}">
                                </figure>
                            @endif
                            <div class="mad_item_offset">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="mad_no_space page page_contents_blocked">{!! $membershiped->details !!}</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
