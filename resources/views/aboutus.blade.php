@extends('layouts.app')

@section('contents')

    <div class="mad_section_2">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <div class="align_center">
                        <h2 class="mad_section_offset">{{$page->title}}</h2>
                    </div>
                    @if($page->image != null)
                    <figure class="mad_item_offset_2"><img src="{{$page->image('1170x410')}}" alt="{{$page->title}}}"></figure>
                    @endif
                    <div class="mad_item_offset">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="mad_no_space">{!! $page->details !!}</p>
                            </div>
                        </div>
                    </div>



                    {{--<!-- Carousel -->--}}
                    {{--<div class="carousel_type_2 with_separator">--}}

                        {{--<div class="owl-carousel style2 mad_item_inset_1" data-max-items="3">--}}

                            {{--<!-- Slide -->--}}
                            {{--<div>--}}
                                {{--<!-- Carousel Item -->--}}
                                {{--<div class="mad_item_hover">--}}
                                    {{--<figure>--}}
                                        {{--<img src="{{asset('images/370x276_img1.jpg')}}" alt="">--}}
                                    {{--</figure>--}}
                                    {{--<div class="item_overlay">--}}
                                        {{--<div class="text_holder">--}}
                                            {{--<a href="{{asset('images/370x276_img1.jpg')}}" class="mad_icon_plus gallery" rel="category"></a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- /Carousel Item -->--}}
                            {{--</div>--}}
                            {{--<!-- /Slide -->--}}
                            {{--<!-- Slide -->--}}
                            {{--<div>--}}
                                {{--<!-- Carousel Item -->--}}
                                {{--<div class="mad_item_hover">--}}
                                    {{--<figure>--}}
                                        {{--<img src="{{asset('images/370x276_img2.jpg')}}" alt="">--}}
                                    {{--</figure>--}}
                                    {{--<div class="item_overlay">--}}
                                        {{--<div class="text_holder">--}}
                                            {{--<a href="{{asset('images/370x276_img2.jpg')}}" class="mad_icon_plus gallery" rel="category"></a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- /Carousel Item -->--}}
                            {{--</div>--}}
                            {{--<!-- /Slide -->--}}
                            {{--<!-- Slide -->--}}
                            {{--<div>--}}
                                {{--<!-- Carousel Item -->--}}
                                {{--<div class="mad_item_hover">--}}
                                    {{--<figure>--}}
                                        {{--<img src="{{asset('images/370x276_img3.jpg')}}" alt="">--}}
                                    {{--</figure>--}}
                                    {{--<div class="item_overlay">--}}
                                        {{--<div class="text_holder">--}}
                                            {{--<a href="{{asset('images/370x276_img3.jpg')}}" class="mad_icon_plus gallery" rel="category"></a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- /Carousel Item -->--}}
                            {{--</div>--}}
                            {{--<!-- /Slide -->--}}

                        {{--</div>--}}

                    {{--</div>--}}

                </div>
            </div>
        </div>

    </div>

@endsection