@extends('layouts.app_secondary')

@section('title')
404 not found
@endsection
@section('page_header')
	404 not found
@endsection


@section('contents')



	<div class="page_404 paralax_image_bg6" style="background: url('{{asset('images/bg1_1920x965.jpg')}}');">

		<div class="page_404_section">
			<h3 class="mad_title_style1">Oops!</h3>
			<h2>404</h2>
			<h5 class="mad_separator_bottom">SORRY! PAGE NOT FOUND!</h5>
			<p class="mad_text_style1">You can also go back to the <a href="{{route('_root_')}}"><b>{{config('app.name')}}</b> </a> <br>
				and start browsing from here</p>
		</div>

		<svg class="separator_type_5_path" preserveAspectRatio="none" viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
			<path d="M0 0 L70 100 L100 0 L100 100 L0 100 Z" fill="#fff"></path>
			<path d="M0 -1 L0 0 L50 100 L100 0 L100 -1 Z" fill="rgba(255,255,255,0.01)"></path>
			<path d="M0 0 L50 100 L100 0" fill="none" stroke="" stroke-width="0"></path>
		</svg>

	</div>

@endsection


