@extends('admin.app')

@section('title')
    Activity
@endsection

@section('content')
    @if($loged !=null )
    <h3 class="page-title">Log of {{$loged->user->name}} <a href="{{ route('admins.index') }}" class="btn btn-primary pull-right"><i class="lnr lnr-arrow-left"></i> <span>Back</span></a></h3>

    <div class="panel">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Login Date Time</th>
                    <th>Ip address</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($admins as $admin)
                    <tr>
                        <td>{{ $admin->login_time->format('M d,Y h:i:s A') }}</td>
                        <td>{{ $admin->user_ip }}</td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer">{{ $admins->links() }}</div>
    </div>
    @else
        <a href="{{ route('admins.index') }}" class="btn btn-primary pull-right"><i class="lnr lnr-arrow-left"></i> <span>Back</span></a>
        <h3>No activity available.</h3>
    @endif
@endsection