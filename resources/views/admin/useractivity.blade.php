@extends('admin.app')

@section('title')
    Admin Log
@endsection

@section('content')




    <h3 class="page-title">Activity of {{$loged->user->name}}</h3>

    <div class="panel">
        <div class="panel-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Login Date Time</th>
                    <th>Ip Address</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($logs as $log)
                    <tr>
                        <td>{{ $log->login_time->format('M d, Y h:i:s A')}}</td>
                        <td>{{ $log->user_ip }}</td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="panel-footer">{{ $logs->links() }}</div>
    </div>
@endsection

