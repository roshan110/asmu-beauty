@extends('admin.app')

@section('title')
Settings
@endsection

@section('content')

	@if(count($logos) > 0)
		<h4>Logo</h4>
		@foreach($logos as $logo)
			<img src="{{ asset('uploads/logo/'.$logo->value) }}" height="100" width="150">
		@endforeach
		<form method="POST" action="{{ route('change.logo') }}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="logo-add">
				<input type="file" name="logo" onclick="myFunction()">
				<button type="submit" id="submit-btn" style="display: none;"> Submit</button>
			</div>
		</form>
	@else
		<img src="{{ asset('images/logo.png') }}" height="100" width="150">
		<form method="POST" action="{{ route('change.logo') }}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="logo-add">
				<input type="file" name="logo" onclick="myFunction()">
				<button type="submit" id="submit-btn" style="display: none;"> Submit</button>
			</div>
		</form>
	@endif
	<br>
	@if(count($headerbgs) > 0)
		<h4>Page Header Bg [current: 1920x1110]</h4>
		@foreach($headerbgs as $headerbg)
			<img src="{{ asset('uploads/headerbg/'.$headerbg->value) }}" height="100" width="150">
		@endforeach
		<form method="POST" action="{{ route('change.headerbg') }}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="logo-add">
				<input type="file" name="headerbg" onclick="myFunctions()">
				<button type="submit" id="submit-btns" style="display: none;"> Submit</button>
			</div>
		</form>
	@else
		<img src="{{ asset('images/bg_image_1920x1110.jpg') }}" height="100" width="150">
		<form method="POST" action="{{ route('change.headerbg') }}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="logo-add">
				<input type="file" name="headerbg" onclick="myFunctions()">
				<button type="submit" id="submit-btns" style="display: none;"> Submit</button>
			</div>
		</form>
	@endif


	<h3 class="page-title">Settings <a href="{{ route('settings.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>Title</th>
					<th>Value</th>
					<th>Created At</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($settings as $setting)
				<tr>
					<td>{{ $setting->title }}</td>
					<td>{{ $setting->value }}</td>
					<td>{{ $setting->created_at->format('M d, Y') }}</td>
					<td class="text-right">
						
							<a class="btn btn-primary btn-sm" href="{{ route('settings.edit', $setting->id) }}"><i class="lnr lnr-pencil"></i></a>

							<div class="pull-right" style="margin-left: 10px;"> 
								<form onsubmit="return confirm('Are you sure?')" action="{{ route('settings.destroy', $setting->id) }}" method="post">
									{{ method_field('DELETE') }}
									{{ csrf_field() }}		
									<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
								</form>
							</div>
					</td>
				</tr>			
				@endforeach
			</tbody>
		</table>
	</div>

	<div class="panel-footer">{{ $settings->links() }}</div>
</div>
@endsection

@section('script')
	<script type="text/javascript">
//		logo
		function myFunction() {
			var x = document.getElementById("submit-btn");
			if (x.style.display === "none") {
				x.style.display = "block";
			} else {
				x.style.display = "none";
			}
		}
		//		Headerbg
		function myFunctions() {
			var x = document.getElementById("submit-btns");
			if (x.style.display === "none") {
				x.style.display = "block";
			} else {
				x.style.display = "none";
			}
		}
	</script>
@endsection