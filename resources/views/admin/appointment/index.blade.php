@extends('admin.app')

@section('title')
Appointments
@endsection

@section('content')

<h3 class="page-title">Appointments</h3>

<div class="panel">
	<div class="panel-heading">
        <h3 class="panel-title">Today made appointments</h3>
    </div>
	<div class="panel-body">		
		<table class="table">
			<thead>
				<tr>
					<th>#app.Id</th>
					<th>Payment</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($today_appointments as $appointment)
				<tr>
					<td><a href="{{ route('appointments.show', $appointment->id) }}">#{{$appointment->id}}</a></td>
					<td>{{$appointment->payment}}({!!$appointment->paid? '<span class="text-success">Paid</span>' : '<span class="text-danger">Unpaid</span>'!!})</td>
					<td>{{ $appointment->name }}</td>
					<td>{{ $appointment->email }}</td>
					<td>{{ $appointment->phone }}</td>
					<td>{{ $appointment->appointment_status }}</td>
					<td class="text-right">
						<a class="btn btn-warning btn-sm" href="{{ route('appointments.show', $appointment->id) }}"><i class="lnr lnr-eye"></i></a>
						<div class="pull-right" style="margin-left: 10px;">
							<form onsubmit="return confirm('Are you sure?')" action="{{ route('appointments.destroy', $appointment->id) }}" method="post">
								{{ method_field('DELETE') }}
								{{ csrf_field() }}
								<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
							</form>
						</div>

					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="panel-footer">{{$today_appointments->links()}}</div>
</div>

<div class="panel">
	<div class="panel-heading">
        <h3 class="panel-title">All Appointments</h3>
    </div>
	<div class="panel-body">		
		<table class="table">
			<thead>
				<tr>
					<th>#app.Id</th>
					<th>Payment</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Status</th>
					<th>Payment</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($appointments as $appointment)
				<tr>
					<td><a href="{{ route('appointments.show', $appointment->id) }}">#{{$appointment->id}}</a></td>
					<td>{{$appointment->payment}}({!!$appointment->paid? '<span class="text-success">Paid</span>' : '<span class="text-danger">Unpaid</span>'!!})</td>
					<td>{{ $appointment->name }}</td>
					<td>{{$appointment->email}}</td>
					<td>{{ $appointment->phone }}</td>
					<td>{{ $appointment->appointment_status }}</td>
					<td>
						@if($appointment->paid == 0)
							<div class="pull-left" style="margin-right: 20px;">
								<form class="form-group" action="{{ route('appointment.paid.approve',encrypt($appointment->id)) }}" method="post">
									{{csrf_field()}}
									<button type="submit" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Paid"><i class="lnr lnr-checkmark-circle"></i></button>
								</form>
							</div>

						@elseif($appointment->paid == 1)
							<p class="text-success"><i class="lnr lnr-checkmark-circle"></i> Paid</p>
						@else
							<p class="text-danger"><i class="lnr lnr-cross-circle"></i> Unpaid</p>
						@endif
					</td>
					<td class="text-right">
							<a class="btn btn-warning btn-sm" href="{{ route('appointments.show', $appointment->id) }}"><i class="lnr lnr-eye"></i></a>
						<div class="pull-right" style="margin-left: 10px;">
							<form onsubmit="return confirm('Are you sure?')" action="{{ route('appointments.destroy', $appointment->id) }}" method="post">
								{{ method_field('DELETE') }}
								{{ csrf_field() }}
								<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
							</form>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="panel-footer">{{$appointments->links()}}</div>
</div>
@endsection

