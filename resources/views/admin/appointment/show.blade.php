@extends('admin.app')

@section('title')
Appointment [#{{$appointment->id}}]
@endsection

@section('content')

<h3 class="page-title">Appointment Details</h3>

<div class="panel">
	<div class="panel-heading">
        <h3 class="panel-title">Appointment Details [#{{$appointment->id}}]</h3>
    </div>
	<div class="panel-body">		
		<p>

			<b>Payment Type:</b> {{$appointment->payment}} ({!!$appointment->paid? '<span class="text-success">Paid</span>' : '<span class="text-danger">Unpaid</span>'!!})<br>
			<b>Name:</b> {{$appointment->name}}<br>
			<b>Phone:</b> {{$appointment->phone}}<br>
			<b>Email:</b> {{$appointment->email}}<br>

		</p>
		<br>

		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th class="text-right">Date</th>
						<th class="text-right">Time</th>
						<th class="text-right">Employee Name</th>
						<th class="text-right">Employee Email</th>
						<th class="text-right">Employee Phone</th>
						<th class="text-right">Service</th>
						<th class="text-right">Price</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($appointment->items as $item)
					<tr>
						<td class="text-right">{{$item->appointment_date}}</td>
						<td class="text-right">{{$item->appointment_time}}</td>
						<td class="text-right">{{$item->employee->name}}</td>
						<td class="text-right">{{$item->employee->email}}</td>
						<td class="text-right">{{$item->employee->phone}}</td>
						<td class="text-right">{{$item->item}}</td>
						<td class="text-right">&pound;{{number_format($item->price,2)}}</td>
					</tr>
					@endforeach
					<tr style="background: #f1f1f1;">
						<td class="text-right" colspan="6"><strong>Total</strong></td>
						<td class="text-right">&pound;{{$appointment->total}}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>	
</div>
@endsection

