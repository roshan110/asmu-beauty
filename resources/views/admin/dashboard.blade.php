@extends('admin.app') 
@section('title') Dashboard
@endsection
 
@section('content')
<h3 class="page-title">Dashboard</h3>

<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-headline" style="border: 1px solid #c6c6c6;">
            <div class="panel-heading" style="background: #f1f1f1;">
                <h3 class="panel-title" style="text-align: center; font-size:24px;"><span class="lnr lnr-users"></span>Today Made Membership Requests</h3>
                <p class="panel-subtitle" style="text-align: center; font-size:22px;">Date: {{date('d M, Y')}}</p>
            </div>
            <div class="panel-body">
                @if ($memberships->count() > 0)
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Membership Type</th>
                                <th>Applicant</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($memberships as $membership)
                                <tr>
                                    <td>{{$membership->membership->title}} </td>
                                    <td>{{ $membership->name }}</td>
                                    <td>{{ $membership->email }}</td>
                                    <td>{{ $membership->phone }}</td>
                                    <td>{{ $membership->address }}</td>
                                    {{--<td><a href="" class="btn btn-primary btn-xs"><i class="lnr lnr-eye"></i></a></td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <p>Records not found</p>
                @endif

            </div>

            <div class="panel-footer">
                <a href="{{route('membershipjoin.request')}}" class="btn btn-primary">View all</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-headline" style="border: 1px solid #c6c6c6;">
            <div class="panel-heading" style="background: #f1f1f1;">
                <h3 class="panel-title" style="text-align: center; font-size:24px;"><span class="lnr lnr-users"></span>Today Made Appointments</h3>
                <p class="panel-subtitle" style="text-align: center; font-size:22px;">Date: {{date('d M, Y')}}</p>
            </div>
            <div class="panel-body">
                @if ($appointments->count() > 0)
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#app.Id</th>
                                <th>Payment</th>
                                <th>Name</th>
                                <th>Phone</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($appointments as $appointment)
                                <tr>
                                    <td><a href="{{ route('appointments.show', $appointment->id) }}">#{{$appointment->id}}</a></td>
                                    <td>{{$appointment->payment}}({!!$appointment->paid? '<span class="text-success">Paid</span>' : '<span class="text-danger">Unpaid</span>'!!})

                                    </td>
                                    <td>{{ $appointment->name }}</td>
                                    <td>{{ $appointment->phone }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <p>Records not found</p>
                @endif

            </div>

            <div class="panel-footer">
                <a href="{{ route('appointments.index') }}" class="btn btn-primary">View all</a>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-headline">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="fa fa-users fa-fw"></span>Active Users</h3>
                {{--
                <p class="panel-subtitle">Date: {{date('d M, y')}}</p> --}}
            </div>
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Type</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($user_details as $user)

                                <tr>
                                    <td>{{$user->id}}</td>
                                    @if($user->groups == 100)
                                      <td>Admin</td>
                                    @else
                                        <td>User</td>
                                    @endif
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>
                                        <ul class="online-status">
                                            @if($user->isOnline())
                                            <li class="active-status">Online</li>
                                            @else
                                                <li class="offline-status">Offline</li>
                                            @endif
                                        </ul>
                                    </td>
                                </tr>


                        @endforeach
                        </tbody>
                    </table>
                </div>


            </div>

        </div>
    </div>
</div>
@endsection