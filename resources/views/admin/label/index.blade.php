@extends('admin.app')

@section('title')
Labels
@endsection

@section('content')

<h3 class="page-title">Labels <a href="{{ route('labels.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-heading">
		<form class="form-horizontal">
			<div class="form-group">
				<label for="" class="col-sm-1 control-label">Filter</label>
				<div class="col-sm-3">
					<select name="filter" class="form-control" onchange="this.form.submit()">
						<option value="0">All</option>
						@foreach ($pageds as $parent)

								<option {{ request()->filter == $parent ? 'selected=selected' : null }} value="{{ $parent}}">{{ $parent }}</option>

						@endforeach

					</select>
				</div>

				@if(request()->filter)
					<div class="col-sm-3">
						<a href="{{ route('labels.index') }}" class="btn btn-danger pull-right">Clear filter</a>
					</div>
				@endif

			</div>
		</form>
	</div>
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>Page</th>
					<th>Label Id</th>
					<th>Value</th>
					<th>Created At</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($labels as $label)				
				<tr>
					<td>{{ $label->page }}</td>
					<td>{{ $label->labelid }}</td>
					<td>{{ $label->value }}</td>
					<td>{{ $label->created_at->format('M d, Y') }}</td>
					<td class="text-right">
						
							<a class="btn btn-primary btn-sm" href="{{ route('labels.edit', $label->id) }}"><i class="lnr lnr-pencil"></i></a>

							<div class="pull-right" style="margin-left: 10px;"> 
								<form onsubmit="return confirm('Are you sure?')" action="{{ route('labels.destroy', $label->id) }}" method="post">
									{{ method_field('DELETE') }}
									{{ csrf_field() }}		
									<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
								</form>
							</div>
					</td>
				</tr>			
				@endforeach
			</tbody>
		</table>
	</div>

	<div class="panel-footer">{{ $labels->links() }}</div>
</div>
@endsection