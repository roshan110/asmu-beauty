<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Title</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Title" name="title" value="{{ old('title', isset($partner) ? $partner->title : null) }}" type="text" id="title">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Image [294*120]</label>
	<div class="col-sm-10">
		@if(isset($partner) && $partner->image)
			<img src="{{asset('/uploads/partners/'.$partner->image)}}" width="160" height="100">
			<a href="{{route('partners.photodelete',$partner->id)}}" class="btn btn-danger">Delete this image</a>
		@endif
		<input type="file" name="image" id="image">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="link">Link</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Link" name="link" value="{{ old('link', isset($partner) ? $partner->link : null) }}" type="text" id="link">
	</div>
</div>

