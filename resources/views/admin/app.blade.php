
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title', 'Dashboard') || {{ config('app.name') }}</title>
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/icon/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.css???') }}">
    <link rel="stylesheet" href="{{ asset('css/admin-responsive.css???') }}">
    <link rel="icon" href="{{ asset('images/admin/w3web.jpg') }}" type="image/x-icon" />

    <style media="screen">
        .bg-init,
        .animsition-overlay {
            background-color: #fff;
        }
    </style>

    <style type="text/css">
        table.table>tbody>tr>td {
            vertical-align: middle;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="background: #ccc;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span> </button>
            <a class="navbar-brand hidden-xs" href="{{ route('admin.dashboard') }}" style="font-size: 16px;">Asmu Beautyparlor</a>
        </div>
        <div class="header_left">
            <img src="{{ App\Setting::getLogo() }}" alt="Asmu Beauty"
                 class="img-responsive" height="50" />
        </div>
        <ul class="header_right">
            {{-- <li class="user_image"><img src="{{ asset(config('cms.userimage')) }}" alt="" /></li> --}}
            <li class="name"><b>{{ Auth::user()->name }}</b></li>
            <li class="logout"><a href="{{ route('admin.logout') }}"> Logout <i class="lnr lnr-exit"></i></a></li>
            <li class="logout"><a href="{{ route('password.change') }}"> Password Change <i class="lnr lnr-lock"></i></a></li>
            <li class="logout"><a href="{{ route('admin.activity') }}"> Log <i class="lnr lnr-keyboard"></i></a></li>
            <li><a href="{{ route('settings.index') }}"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
        </ul>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="side-bar" id="side-bar">
                <ul>
                    <li><a href="{{ route('admin.dashboard') }}" {!!(!request()->segment(2) ? 'class="active"' : null) !!} ><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>

                    <li><a href="{{ route('pages.index') }}" {!!(request()->segment(2) == 'pages' ? 'class="active"' : null) !!}><i class="lnr lnr-file-empty"></i> <span>Pages</span></a></li>

                    <li><a href="{{ route('labels.index') }}" {!!(request()->segment(2) == 'labels' ? 'class="active"' : null) !!}><i class="lnr lnr-spell-check"></i> <span>Labels</span></a></li>

                    <li><a href="{{ route('categories.index') }}" {!!(request()->segment(2) == 'categories' ?'class="active"' : null) !!}><i class="lnr lnr-menu"></i> <span>Categories</span></a></li>

                    <li><a href="{{ route('items.index') }}" {!!(request()->segment(2) == 'items' ? 'class="active"' : null) !!}><i class="lnr lnr-store"></i> <span>Services</span></a></li>

                    <li><a href="{{ route('employees.index') }}" {!!(request()->segment(2) == 'employees' ? 'class="active"' : null) !!}><i class="lnr lnr-shirt"></i> <span>Employees</span></a></li>

                    <li><a href="{{ route('slides.index') }}" {!!(request()->segment(2) == 'slides' ? 'class="active"' : null) !!}><i class="lnr lnr-picture"></i> <span>Slides</span></a></li>

                    <li><a href="{{ route('reviews.index') }}" {!!(request()->segment(2) == 'reviews' ? 'class="active"' : null) !!}><i class="lnr lnr-bubble"></i> <span>Reviews</span></a></li>

                    <li><a href="{{ route('blogs.index') }}" {!!(request()->segment(2) == 'blogs' ? 'class="active"' : null) !!}><i class="lnr lnr-list"></i> <span>Blogs</span></a></li>

                    <li><a href="{{ route('memberships.index') }}" {!!(request()->segment(2) == 'memberships' ? 'class="active"' : null) !!}><i class="lnr lnr-file-add"></i> <span>Memberships</span></a></li>

                    <li><a href="{{ route('membershipjoin.request') }}" {!!(request()->segment(2) == 'membershipjoin.request' ? 'class="active"' : null) !!}><i class="lnr lnr-download"></i> <span>Membership Requests</span></a></li>

                    <li><a href="{{ route('albums.index') }}" {!!(request()->segment(2) == 'albums' ? 'class="active"' : null) !!}><i class="lnr lnr-camera"></i> <span>Photo Album</span></a></li>

                    <li><a href="{{ route('videos.index') }}" {!!(request()->segment(2) == 'videos' ? 'class="active"' : null) !!}><i class="lnr lnr-camera-video"></i> <span>Videos</span></a></li>

                    <li><a href="{{ route('socials.index') }}" {!!(request()->segment(2) == 'socials' ? 'class="active"' : null) !!}><i class="lnr lnr-link"></i> <span>Socials</span></a></li>

                    <li><a href="{{ route('partners.index') }}" {!!(request()->segment(2) == 'partners' ? 'class="active"' : null) !!}><i class="lnr lnr-flag"></i> <span>Partners</span></a></li>

                    <li><a href="{{ route('admins.index') }}" {!!(request()->segment(2) == 'admins' ? 'class="active"' : null) !!}><i class="lnr lnr-user"></i> <span>Admins</span></a></li>

                </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<div class="content">

    {{--<h1>@yield('title', 'Dashboard')</h1>--}}

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p>
                {{ Session::get('success') }}
            </p>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p>
                {{ Session::get( 'error') }}
            </p>
        </div>
    @endif

    @yield('content')

</div>

<div class="footer">
    <p>2013 -
        <?php echo date("Y");?>, &copy; & Power by: <a href="https://w3web.co.uk/" target="_blank">W3 Web Technology</a></p>
</div>


<div class="modal fade" id="jsModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content"></div>
    </div>
</div>

<script src="{{ asset('vendor/jquery-ui/external/jquery/jquery.js') }}"></script>
<script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/enscroll.min.js') }}"></script>
<script src="{{ asset('js/admin.js??') }}"></script>
<script src="{{ asset('/vendor/ckeditor/ckeditor.js?ref_' . str_random()) }}"></script>
<script>
    CKEDITOR.replace('details', {
        height: 500,
        extraPlugins: 'autoembed,embed,image2',
        embed_provider: '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}',
        // embed_provider: '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}',
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&responseType=json&_token=' +
        '{{ csrf_token() }}',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&responseType=json&_token=' + '{{ csrf_token() }}'
    });
</script>

<script>
    $(function () {
        $('.topbar').dropdown();
    });
    $(function () {
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        });
    });
</script>

@yield('script')
</body>

</html>