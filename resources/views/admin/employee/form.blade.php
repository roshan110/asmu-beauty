<div class="form-group">
	<label class="col-sm-2 control-label" for="name">Name</label>
	<div class="col-sm-4">
		<input class="col-md-4 form-control" placeholder="Name" name="name" value="{{ old('name', isset($employee) ? $employee->name : null) }}" type="text" id="name">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="email">Email</label>
	<div class="col-sm-4">
		<input class="col-md-4 form-control" placeholder="Email" name="email" value="{{ old('email', isset($employee) ? $employee->email : null) }}" type="email" id="email">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="address">Address</label>
	<div class="col-sm-4">
		<input class="col-md-4 form-control" placeholder="Address" name="address" value="{{ old('address', isset($employee) ? $employee->address : null) }}" type="text" id="address">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="phone">Phone</label>
	<div class="col-sm-4">
		<input class="col-md-4 form-control" placeholder="Phone" name="phone" value="{{ old('phone', isset($employee) ? $employee->phone : null) }}" type="text" id="phone">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Image [360x395]</label>
	<div class="col-sm-4">
		@if(isset($employee) && $employee->image)
			<img src="{{asset('/uploads/employees/'.$employee->image)}}" width="160" height="100">
			<a href="{{route('employees.photodelete',$employee->id)}}" class="btn btn-danger">Delete this image</a>
		@endif
		<input class="col-md-4 form-control" name="image" type="file" id="image">
	</div>

</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-8">
		<textarea class="col-md-8 form-control" rows="8" placeholder="Details" name="details" id="details">{{ old('details', isset($employee) ? $employee->details : null) }}</textarea>
	</div>
</div>

{{--<div class="form-group">--}}
	{{--<label class="col-sm-2 control-label" for="holiday">Holiday</label>--}}
	{{--<div class="col-sm-4">--}}
		{{--<select class="form-control" name="holiday" id="holiday">--}}
			{{--<option value="">Select Day</option>--}}
			{{--<option value="Sunday" {{ (isset($employee) && $employee->holiday == "Sunday") ? 'selected' : "Sunday"  }}>Sunday</option>--}}
			{{--<option value="Monday" {{ (isset($employee) && $employee->holiday == "Monday") ? 'selected' : "Monday"  }}>Monday</option>--}}
			{{--<option value="Tuesday" {{ (isset($employee) && $employee->holiday == "Tuesday") ? 'selected' : "Tuesday"  }}>Tuesday</option>--}}
			{{--<option value="Wednesday" {{ (isset($employee) && $employee->holiday == "Wednesday") ? 'selected' : "Wednesday"  }}>Wednesday</option>--}}
			{{--<option value="Thursday" {{ (isset($employee) && $employee->holiday == "Thursday") ? 'selected' : "Thursday"  }}>Thursday</option>--}}
			{{--<option value="Friday" {{ (isset($employee) && $employee->holiday == "Friday") ? 'selected' : "Friday"  }}>Friday</option>--}}
			{{--<option value="Saturday" {{ (isset($employee) && $employee->holiday == "Saturday") ? 'selected' : "Saturday"  }}>Saturday</option>--}}
		{{--</select>--}}
	{{--</div>--}}
{{--</div>--}}

<div class="form-group row">
	<div class="col-md-2 col-sm-2 col-xs-6" style="text-align: right;">
		<label for="sr-only" class="control-label">Holiday</label>
	</div>
	<div class="col-md-8 col-sm-8 col-xs-6">
		<div class="form-check-inline">
			<label class="form-check-label">
				@foreach($days as $holiday)
					<input name="holiday[]" type="checkbox" class="form-check-input" value="{{$holiday}}"
						   @if(isset($employee) && $employee->holiday)

							{{ in_array($holiday, $values) ? 'checked=checked' : null }}
					@endif
							>{{$holiday}}
					&nbsp;&nbsp;
				@endforeach

			</label>
		</div>

	</div>
</div>


<div class="form-group">
	<label class="col-sm-2 control-label" for="start_time">Start Time</label>
	<div class="col-sm-4">
		<input class="col-md-4 form-control" placeholder="Start Time" name="start_time" value="{{ old('start_time', isset($employee) ? $employee->start_time : null) }}" type="time" id="start_time">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="end_time">End Time</label>
	<div class="col-sm-4">
		<input class="col-md-4 form-control" placeholder="End Time" name="end_time" value="{{ old('end_time', isset($employee) ? $employee->end_time : null) }}" type="time" id="end_time">
	</div>
</div>

<h4 class="text-muted" style="text-align: center">Choose service</h4>
<hr>
<div class="form-group row">
	<div class="col-md-3 col-sm-3 col-xs-6">
		<label for="sr-only"></label>
	</div>
	<div class="col-md-9 col-sm-8 col-xs-6">
		<div class="form-check-inline">
			<label class="form-check-label">
				@foreach($items as $item)
					<input name="item[]" type="checkbox" class="form-check-input" value="{{$item->id}}" {{ is_array(old('item', isset($employee) ? $employee->items()->allRelatedIds()->toArray():null)) && in_array($item->id, old('item', isset($employee) ? $employee->items()->allRelatedIds()->toArray():null)) ? 'checked=checked': null }} >{{$item->title}}
					&nbsp;&nbsp;
				@endforeach

			</label>
		</div>

	</div>
</div>
