@extends('admin.app') 
@section('title') Employee
@endsection
 
@section('content')

	<h3 class="page-title">Employees <a href="{{ route('employees.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

	<div class="panel">
		<div class="panel-heading">
			<form class="form-horizontal">
				<div class="form-group">
					<div class="col-sm-3">
						<input type="text" class="form-control form-control-sm" name="keyword" placeholder="Enter Employee Id or Keyword" value="{{ request()->keyword }}">
					</div>
					<div class="col-sm-2">
						{{-- <button type="submit" class="btn btn-primary"> Search </button> --}}
						<input type="submit" class="btn btn-primary" value="Search">
					</div>


				</div>
			</form>
		</div>
		<div class="panel-body">
			<table class="table">
				<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Show/Hide</th>
					<th>Status</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
				@php $i=1;
				@endphp

				@foreach ($employees as $employee)
					<tr id="{{ $employee->id }}">
						<td>{{$i}}</td>
						<td>{{ $employee->name}}</td>
						<td>{{$employee->email}}</td>
						<td>{{$employee->phone}}</td>
						<td>
							@if ($employee->show_in_page)
								<a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click to hide" href="{{ route('employees.showhidepage', $employee->id) }}">
									<i class="lnr lnr-checkmark-circle"></i>
									Show
								</a>
							@else
								<a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click to show" href="{{ route('employees.showhidepage', $employee->id) }}">
									<i class="lnr lnr-cross-circle"></i>
									Hide
								</a>
							@endif
						</td>
						<td>
							@if ($employee->status)
								<a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click to draft" href="{{ route('employees.showhide', $employee->id) }}">
									<i class="lnr lnr-checkmark-circle"></i>
									Active
								</a>
							@else
								<a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click to active" href="{{ route('employees.showhide', $employee->id) }}">
									<i class="lnr lnr-cross-circle"></i>
									Draft
								</a>
							@endif
						</td>
						<td class="text-right">

							<a class="btn btn-primary btn-sm" href="{{ route('employees.edit', $employee->id) }}"><i class="lnr lnr-pencil"></i></a>

							<div class="pull-right" style="margin-left: 10px;">
								<form onsubmit="return confirm('Are you sure?')" action="{{ route('employees.destroy', $employee->id) }}" method="post">
									{{ method_field('DELETE') }} {{ csrf_field() }}
									<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
								</form>
							</div>
						</td>
					</tr>
					@php $i++; @endphp
				@endforeach


				</tbody>
			</table>
		</div>
		<div class="panel-footer">{{ $employees->links() }}</div>
	</div>


@endsection