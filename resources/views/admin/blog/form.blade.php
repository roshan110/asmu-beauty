<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Title</label>
	<div class="col-sm-4">
		<input class="col-md-4 form-control" placeholder="Title" name="title" value="{{ old('title', isset($blog) ? $blog->title : null) }}" type="text" id="title">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Feature Image [769x500]</label>
	<div class="col-sm-4">
		@if(isset($blog) && $blog->image)
			<img src="{{asset('/uploads/blogs/'.$blog->image)}}" width="160" height="100">
			<a href="{{route('blogs.photodelete',$blog->id)}}" class="btn btn-danger">Delete this image</a>
		@endif
		<input class="col-md-4 form-control" name="image" type="file" id="image">
	</div>

</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-8">
		<textarea class="col-md-8 form-control" rows="8" placeholder="Details" name="details" id="details">{{ old('details', isset($blog) ? $blog->details : null) }}</textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="author">Author [Default admin]</label>
	<div class="col-sm-4">
		<input class="col-md-4 form-control" placeholder="Author" name="author" value="{{ old('author', isset($blog) ? $blog->author : null) }}" type="text" id="author">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="meta_title">Meta title</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="4" placeholder="Meta title" name="meta_title" id="meta_title">{{ old('meta_title', isset($blog) ? $blog->meta_title : null) }}</textarea>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="meta_description">Meta description</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="4" placeholder="Meta description" name="meta_description" id="meta_description">{{ old('meta_description', isset($blog) ? $blog->meta_description : null) }}</textarea>
	</div>

</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="meta_keyword">Meta keyword</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="4" placeholder="Meta keyword" name="meta_keyword" id="meta_keyword">{{ old('meta_keyword', isset($blog) ? $blog->meta_keyword : null) }}</textarea>
	</div>
</div>

