@extends('admin.app') 
@section('title') Blog
@endsection
 
@section('content')

	<h3 class="page-title">Blogs <a href="{{ route('blogs.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

	<div class="panel">
		<div class="panel-heading">
			<form class="form-horizontal">
				<div class="form-group">
					<div class="col-sm-3">
						<input type="text" class="form-control form-control-sm" name="keyword" placeholder="Enter Blog Id or Keyword" value="{{ request()->keyword }}">
					</div>
					<div class="col-sm-2">
						{{-- <button type="submit" class="btn btn-primary"> Search </button> --}}
						<input type="submit" class="btn btn-primary" value="Search">
					</div>


				</div>
			</form>
		</div>
		<div class="panel-body">
			<table class="table">
				<thead>
				<tr>
					<th>#</th>
					<th>Title</th>
					<th>Created By</th>
					<th>Status</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
				@php $i=1;
				@endphp

				@foreach ($blogs as $blog)
					<tr id="{{ $blog->id }}">
						<td>{{$i}}</td>
						<td>{{ str_limit($blog->title, 50) }}</td>
						<td>{{$blog->author_name}}[{{ $blog->created_by }}]</td>
						<td>
							@if ($blog->status)
								<a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click to hide" href="{{ route('blogs.showhide', $blog->id) }}">
									<i class="lnr lnr-checkmark-circle"></i>
									Active
								</a>
							@else
								<a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click to show" href="{{ route('blogs.showhide', $blog->id) }}">
									<i class="lnr lnr-cross-circle"></i>
									Draft
								</a>
							@endif
						</td>
						<td class="text-right">

							<a class="btn btn-primary btn-sm" href="{{ route('blogs.edit', $blog->id) }}"><i class="lnr lnr-pencil"></i></a>

							<div class="pull-right" style="margin-left: 10px;">
								<form onsubmit="return confirm('Are you sure?')" action="{{ route('blogs.destroy', $blog->id) }}" method="post">
									{{ method_field('DELETE') }} {{ csrf_field() }}
									<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
								</form>
							</div>
						</td>
					</tr>
					@php $i++; @endphp
				@endforeach


				</tbody>
			</table>
		</div>
		<div class="panel-footer">{{ $blogs->links() }}</div>
	</div>


@endsection