@extends('admin.app')

@section('title')
Membership Request
@endsection

@section('content')
<h3 class="page-title">Membership Request of <b>{{ $membershipjoins->name }}</b> <a href="{{ route('membershipjoin.request') }}" class="btn btn-primary pull-right"><span><i class="lnr lnr-arrow-left"></i> Back</span></a></h3>

<div class="panel">
	<div class="panel-body">
		<div class="table">
			<div>
				<label>Name:</label>
				<p>{{ $membershipjoins->name }}</p>
			</div>

			<div>
				<label>Email:</label>
				<p>{{ $membershipjoins->email }}</p>
			</div>

			<div>
				<label>Phone No:</label>
				<p>{{ $membershipjoins->phone }}</p>
			</div>

			<div>
				<label>Address:</label>
				<p>{{ $membershipjoins->address }}</p>
			</div>

			<div>
				<label>Requested Membership:</label>
				<p>{{$membershipjoins->membership->title}}->&pound;{{$membershipjoins->membership->price}}</p>
			</div>
			<div>
				<label>Service:</label>
				<p>{{ $membershipjoins->service }}</p>
			</div>
		</div>
	</div>
</div>
@endsection