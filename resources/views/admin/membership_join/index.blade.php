@extends('admin.app')

@section('title')
Membership Request
@endsection

@section('content')

<h3 class="page-title">Membership Requests</h3>


<div class="panel">
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>Membership Type</th>
					<th>Applicant</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Address</th>
					<th>Created At</th>
					<th>Payment</th>
					<th>Status</th>
					<th>

					</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($membershipjoins as $membershipjoin)
				<tr>
					<td>{{$membershipjoin->membership->title}} </td>
					<td>{{ $membershipjoin->name }}</td>
					<td>{{ $membershipjoin->email }}</td>
					<td>{{ $membershipjoin->phone }}</td>
					<td>{{$membershipjoin->address}}</td>
					<td>{{ $membershipjoin->created_at->format('M d, Y') }}</td>
					<td>
						@if($membershipjoin->paid == 0)
							<p class="text-danger"><i class="lnr lnr-cross-circle"></i> Unpaid </p>
						@elseif($membershipjoin->paid == 1)
							<p class="text-success"><i class="lnr lnr-checkmark-circle"></i> Paid </p>
						@else
							<p class="text-danger"><i class="lnr lnr-circle-minus"></i> Unpaid &amp; Declined </p>
						@endif
					</td>
					<td>
						@if($membershipjoin->status == 0)
							<div class="pull-left" style="margin-right: 20px;">
								<form class="form-group" action="{{ route('membershipjoin.request.approve',encrypt($membershipjoin->id)) }}" method="post">
									{{csrf_field()}}
									<button type="submit" class="btn btn-success"><i class="lnr lnr-checkmark-circle"></i></button>
								</form>
							</div>

							<div class="pull-left" style="margin-right: 20px;">
								<form class="form-group" action="{{ route('membershipjoin.request.decline',encrypt($membershipjoin->id)) }}" method="post">
									{{csrf_field()}}
									<button type="submit" class="btn btn-warning"><i class="lnr lnr-cross-circle"></i></button>
								</form>
							</div>
						@elseif($membershipjoin->status == 1)
							<p class="text-success"><i class="lnr lnr-checkmark-circle"></i> Approved </p>
						@else
							<p class="text-danger"><i class="lnr lnr-cross-circle"></i> Declined </p>
						@endif
					</td>

					<td><a href="{{ route('membershipjoin.request.view', $membershipjoin->id) }}" class="btn btn-primary btn-xs"><i class="lnr lnr-eye"></i></a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<div class="panel-footer">{{ $membershipjoins->links() }}</div>
</div>
@endsection