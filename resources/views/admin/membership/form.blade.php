<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Title</label>
	<div class="col-sm-4">
		<input class="col-md-4 form-control" placeholder="Title" name="title" value="{{ old('title', isset($membership) ? $membership->title : null) }}" type="text" id="title">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Feature Image [769x295]</label>
	<div class="col-sm-4">
		@if(isset($membership) && $membership->image)
			<img src="{{asset('/uploads/memberships/'.$membership->image)}}" width="160" height="100">
			<a href="{{route('memberships.photodelete',$membership->id)}}" class="btn btn-danger">Delete this image</a>
		@endif
		<input class="col-md-4 form-control" name="image" type="file" id="image">
	</div>

</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-8">
		<textarea class="col-md-8 form-control" rows="8" placeholder="Details" name="details" id="details">{{ old('details', isset($membership) ? $membership->details : null) }}</textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="price">Price </label>
	<div class="col-sm-6">
		<div class="input-group">
			<span class="input-group-addon">&pound;</span>
			<input class="col-md-4 form-control" placeholder="Price" name="price" value="{{ old('price', isset($membership) ? $membership->price : null) }}" type="number" id="price" min="1">
		</div>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="meta_title">Meta title</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="4" placeholder="Meta title" name="meta_title" id="meta_title">{{ old('meta_title', isset($membership) ? $membership->meta_title : null) }}</textarea>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="meta_description">Meta description</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="4" placeholder="Meta description" name="meta_description" id="meta_description">{{ old('meta_description', isset($membership) ? $membership->meta_description : null) }}</textarea>
	</div>

</div>
<div class="form-group">
	<label class="col-sm-2 control-label" for="meta_keyword">Meta keyword</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="4" placeholder="Meta keyword" name="meta_keyword" id="meta_keyword">{{ old('meta_keyword', isset($membership) ? $membership->meta_keyword : null) }}</textarea>
	</div>
</div>

