@extends('layouts.app')
@section('is_thankyou', 1)
@section('contents')

    <div class="mad_section_2">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <div class="align_center">
                        <h2 class="mad_section_offset">Thank You</h2>

                    </div>

                    <div class="mad_item_offset">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <p class="mad_no_space">Your request has been posted successfully. We will respond you shortly after your request is reviewed.</p>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

@endsection