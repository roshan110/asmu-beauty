@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.1.0/jquery.datetimepicker.min.css">
@endsection
@section('contents')
    <div class="mad_section">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <!-- Appointment Field -->
                    <div class="align_center">
                        <h3 class="mad_title_style1">Book an</h3>
                        <h2>Appointment</h2>

                        @if($errors->any())
                            <br>
                            <div class="alert alert-danger">
                                {{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (\Session::has('error'))
                            <br>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>{{\Session::get('error')}}</strong>
                            </div>
                        @endif

                    </div>


                    <div id="helpdeskform" class="mad_section_offset">

                        <!-- Progress Bar -->
                        <ul id="progressbar">
                            <li class="app_active first">1. Service and Time</li>
                            <li>2. Details</li>
                            <li>3. Payment</li>
                            <li>4. Checkout</li>
                        </ul>
                        <form method="post" action="{{route('appointment_book')}}" class="appointment_form_block">
                            {{csrf_field()}}
                            <div class="field app_selected">

                                <div class="mad_separator_bottom">

                                    <h6>Please select service: <br class="next_line_appointment"><br
                                            class="next_line_appointment"><strong
                                            style="background: #ce3d5f;padding: 4px;">Note:- Please fill all the fields
                                            to continue.</strong></h6>
                                    <div class="form-group row">
                                        <div class="col-sm-4">
                                            <label>Step 1: Category</label>
                                            <div class="">
                                                <select class=" form-control dynamic-1" data-dependent="item-1"
                                                        name="category[]" id="category-1">
                                                    <option>Select category</option>
                                                    @foreach ($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                                        @foreach ($category->child as $child)
                                                            <option value="{{ $child->id }}">
                                                                --{{ $child->title }}</option>
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">


                                            <label>Step 2: Service</label>
                                            <div class="">
                                                <select class="form-control dynamic-1 dynamical-1" name="item[]"
                                                        id="item-1"
                                                        data-dependent="employee-1">
                                                    <option>Select Service</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">


                                            <label>Step 3: Employee</label>
                                            <div class="">
                                                <select class="form-control dynamical"  name="employee[]"
                                                        id="employee-1">
                                                    <option>Select Employee</option>
                                                </select>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-3">

                                            <label>Step 4: Date</label>
                                            <input type="text" placeholder="Select date" class="datepicker" id="date-1"
                                                   name="date[]">

                                        </div>
                                        <div class="col-md-4 mad_col_width">

                                            <div class="mad_week_list">
                                                <ul class="clearfix">
                                                    <li>

                                                        <h6>Mon</h6>
                                                        <div class="icon_check"></div>

                                                    </li>
                                                    <li>

                                                        <h6>Tue</h6>
                                                        <div class="icon_check"></div>

                                                    </li>
                                                    <li>

                                                        <h6>Wed</h6>
                                                        <div class="icon_check"></div>

                                                    </li>
                                                    <li>

                                                        <h6>Thu</h6>
                                                        <div class="icon_check"></div>

                                                    </li>
                                                    <li>

                                                        <h6>Fri</h6>
                                                        <div class="icon_check"></div>

                                                    </li>
                                                    <li>

                                                        <h6>Sat</h6>
                                                        <div class="icon_check"></div>

                                                    </li>
                                                    <li>

                                                        <h6>Sun</h6>
                                                        <div class="icon_check"></div>
                                                        {{--<div class="icon_asterick"></div>--}}

                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                        <div class="col-md-2">
                                            <label>Step 5: Time</label>
                                            <div class="">
                                                <select class="form-control time_check" id="time-1" name="time[]">
                                                    <option value="0" selected="selected">Select Time</option>
                                                </select>
                                            </div>


                                        </div>
                                        <div class="col-md-2">


                                        </div>
                                    </div>
                                    <div id="more_service">

                                    </div>
                                    <div class="row add_more_service_block">
                                        <span class="mad_button style3 add_more_service_a" id="add_service">Add more service</span>
                                    </div>

                                </div>

                                <div class="app_nav_buttons clearfix">

                                    <button type="button" class="mad_button style3 app_next" id="appointment_next_btn"
                                            disabled="disabled">Next

                                    </button>
                                </div>

                            </div>
                            <div class="field">

                                <div class="mad_separator_bottom">
                                    <p>Please provide your <span>details</span> in the form below to proceed with <span>booking.</span>
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <ul>
                                                <li class="row">

                                                    <div class="col-sm-4">

                                                        <label for="name">Name</label>
                                                        <input type="text" placeholder="Enter Name" id="name" name="name">

                                                    </div>
                                                    <div class="col-sm-4">

                                                        <label for="phone">Phone</label>
                                                        <input type="text" placeholder="Enter Phone" id="phone" name="phone">
                                                    </div>
                                                    <div class="col-sm-4">

                                                        <label>Email</label>
                                                        <input type="email" placeholder="Enter Email" id="email" name="email">

                                                    </div>

                                                </li>
                                                <li>
                                                    <label for="note">Notes</label>
                                                    <textarea id="note" placeholder="Enter Note" name="note"></textarea>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <table class="table table-bordered " id="appointment_table">
                                                <thead>
                                                <tr>
                                                    <th>S.N</th>
                                                    <th>Service</th>
                                                    <th>Price</th>
                                                </tr>
                                                </thead>
                                                <tbody id="detail_cost">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    {{--<table class="table table-bordered " id="appointment_table">--}}
                                        {{--<thead>--}}
                                        {{--<tr>--}}
                                            {{--<th>S.N</th>--}}
                                            {{--<th>Service</th>--}}
                                            {{--<th>Price</th>--}}
                                        {{--</tr>--}}
                                        {{--</thead>--}}
                                        {{--<tbody id="detail_cost">--}}

                                        {{--</tbody>--}}
                                      {{--</table>--}}
                                    {{--<br>--}}
                                    {{--<br>--}}
                                    {{--<ul>--}}
                                        {{--<li class="row">--}}

                                            {{--<div class="col-sm-4">--}}

                                                {{--<label for="name">Name</label>--}}
                                                {{--<input type="text" placeholder="Enter Name" id="name" name="name">--}}

                                            {{--</div>--}}
                                            {{--<div class="col-sm-4">--}}

                                                {{--<label for="phone">Phone</label>--}}
                                                {{--<input type="text" placeholder="Enter Phone" id="phone" name="phone">--}}
                                            {{--</div>--}}
                                            {{--<div class="col-sm-4">--}}

                                                {{--<label>Email</label>--}}
                                                {{--<input type="email" placeholder="Enter Email" id="email" name="email">--}}

                                            {{--</div>--}}

                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<label for="note">Notes</label>--}}
                                            {{--<textarea id="note" placeholder="Enter Note" name="note"></textarea>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}

                                </div>

                                <div class="app_nav_buttons clearfix">
                                    <button type="button" class="mad_button style3 app_prev">Back</button>
                                    <button type="button" class="mad_button style3 app_next" id="appointment_next_btntwo">Next
                                    </button>
                                </div>

                            </div>
                            <div class="field">

                                <div class="mad_separator_bottom">
                                    <h6>Please tell us how you would like to pay:</h6>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                    <div class="mad_vertical_list">
                                        <ul>
                                            <li>

                                                <div class="control-group form-elements">
                                                    <input type="radio" id="radio_51" name="radio" value="locally"
                                                           class="hide" checked>
                                                    <label for="radio_51">I will pay locally</label>
                                                </div>

                                            </li>
                                            <li>

                                                <div class="control-group form-elements">
                                                    <input type="radio" id="radio_52" name="radio" class="hide"
                                                           disabled>
                                                    <label for="radio_52">I will pay now with Pal<i
                                                            class="icon-paypal"></i></label>
                                                </div>

                                            </li>
                                            <li>

                                                <div class="control-group form-elements">
                                                    <input type="radio" id="radio_53" name="radio" class="hide"
                                                           disabled>
                                                    <label for="radio_53">I will pay now with Credit Card<i
                                                            class="icon-credit-card"></i></label>
                                                </div>

                                            </li>
                                        </ul>
                                    </div>
                                    <br>
                                    <h6>Bank Details</h6>
                                    <p> Please transfer Your fee in this Bank account.<br>
                                        <span>Organization Name: </span>{!! nl2br(\App\Label::ofValue('global:org_name')) !!}
                                        <br>
                                        <span>Bank Name:</span> {!! nl2br(\App\Label::ofValue('global:bank_name')) !!}
                                        <br><br>

                                        <span>Sort Code:</span> {!! nl2br(\App\Label::ofValue('global:sort_code')) !!}
                                        <br>
                                        <span>Account Name:</span> {!! nl2br(\App\Label::ofValue('global:account_name')) !!}
                                    </p>
                                        </div>

                                        <div class="col-md-6 col-sm-12">
                                            <table class="table table-bordered " id="appointment_table">
                                                <thead>
                                                <tr>
                                                    <th>S.N</th>
                                                    <th>Service</th>
                                                    <th>Price</th>
                                                </tr>
                                                </thead>
                                                <tbody id="detail_costs">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="app_nav_buttons clearfix">
                                    <button type="button" class="mad_button style3 app_prev">Back</button>
                                    <button type="button" class="mad_button style3 app_next">Next</button>
                                </div>

                            </div>
                            <div class="field">
                                <h6>Thank you! Your booking process will be now complete. Click the Book button to
                                    complete process. An email with details of your booking will be sent to you.</h6>
                                <h6 style="color:#000;"><strong style="color:#CE3D5F;">Note:- </strong>Accept the terms
                                    and condition to book the appointment.</h6>
                                <table class="table table-bordered " id="appointment_table">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Service</th>
                                        <th>Price</th>
                                    </tr>
                                    </thead>
                                    <tbody id="detail_costed">

                                    </tbody>
                                </table>
                                <br>
                                <br>
                                <div class="form-group row">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="">
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="checkbox" id="myCheck" onclick="myFunction()"
                                                       style="display: inline-block; width: 20px; height: 20px; -moz-appearance:checkbox; -webkit-appearance: checkbox;">
                                                <span class="accept_terms_checkbox" style=" margin-left: 10px;">I accept the <a
                                                        href="{{url('terms-and-condition')}}" style="color:#ccc;">terms and conditions</a> of Asmu Beauty Parlor</span>
                                            </label>
                                        </div>
                                    </div>
                                    {{--<div class="col-md-11 col-sm-11 col-xs-8">--}}
                                    {{--<label for="sr-only" class="control-label">I accept the terms and conditions of Asmu Beauty Parlor</label>--}}

                                    {{--</div>--}}
                                </div>
                                {{--<input type="checkbox" id="myCheck" onclick="myFunction()" style="display: block; width: 20px; height: 20px;"> I accept the terms and condition.--}}

                                <div class="app_nav_buttons clearfix">
                                    <button type="button" class="mad_button style3 app_prev">Back</button>
                                    <button type="submit" class="mad_button style3" style="float: right; display: none;"
                                            id="text">Book Now
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>

                    <!-- Our Experts -->
                    <div class="align_center mad_item_offset">
                        <h3 class="mad_title_style1">Team</h3>
                        <h2>Our Experts</h2>
                    </div>

                    <div class="row">
                        @foreach($teams as $team)
                            <div class="col-sm-4">
                                <div class="mad_gallery_item">

                                    <a href="#" class="mad_item_hover style2">

                                        <figure>
                                            <img
                                                src="{{$team->image ? $team->image('360x395') : asset('images/women_model1.png')}}"
                                                alt="{{$team->name}}">
                                        </figure>
                                        <div class="mad_item_desc with_bg_img">
                                            <div class="mad_author style2">
                                                <h3 class="mad_title_style2"
                                                    style="font-size: 38px;">{{$team->name}}</h3>
                                                {{--<ul style="display: inline-block">--}}

                                                {{--</ul>--}}
                                                <br>
                                                @foreach($team->items as $item)
                                                    <span>{{$item->title}}</span>
                                                @endforeach
                                            </div>

                                        </div>

                                    </a>
                                    <div class="mad_gallery_text" style="min-height: 209px;">
                                        {!! $team->details !!}
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>

        </div>

    </div>
@endsection
@section('script')

    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.1.0/jquery.datetimepicker.min.js"></script>
    <script>
        $(document).ready(function () {
            category(1);

            function category(id) {
                $('.dynamic-' + id).change(function () {
                    reset_function(id);
                    if ($(this).val() != '') {
                        var select = $(this).attr("id");
                        var value = $(this).val();
                        var dependent = $(this).data('dependent');
                        var _token = $('input[name="_token"]').val();
                        // console.log(select);
                        // console.log(value);
                        // console.log(dependent);
                        // console.log(_token);
                        $.ajax({
                            url: "{{ route('dynamindependent.fetch_item') }}",
                            method: "POST",
                            data: {select: select, value: value, _token: _token, dependent: dependent},
                            start: $('#' + dependent).LoadingOverlay('show'),
                            success: function (result) {

                                $('#' + dependent).html(result);
                                $('#' + dependent).LoadingOverlay('hide');
                            }

                        })
                    }
                });

                $('#category').change(function () {
                    $('#item').val('');

                });

                $('.dynamical-' + id).change(function () {
                    if ($(this).val() != '') {
                        var select = $(this).attr("id");
                        var value = $(this).val();
                        var dependent = $(this).data('dependent');
                        var _token = $('input[name="_token"]').val();
                        // console.log(select);
                        // console.log(value);
                        // console.log(dependent);
                        console.log(_token);
                        $.ajax({
                            url: "{{ route('dynamindependent.fetch_employee') }}",
                            method: "POST",
                            data: {select: select, value: value, _token: _token, dependent: dependent},
                            start: $('#' + dependent).LoadingOverlay('show'),
                            success: function (result) {

                                $('#' + dependent).html(result);
                                $('#' + dependent).LoadingOverlay('hide');
                            }

                        })
                    }
                });

                $('#item').change(function () {
                    $('#employee-' + id).val('');
                });

                $(document).on('change', '#employee-' + id, function (e) {
                    e.preventDefault();
                    let employee = $('#employee-' + id).val();
                    // console.log(employee);
                    appointments_dates(employee, id);
                });

                $(document).on('change', '#date-' + id, function (e) {
                    e.preventDefault();
                    let date = $('#date-' + id).val();
                    let employee = $('#employee-' + id).val();
                    // console.log(date);
                    // console.log(employee);
                    appointment_date(employee, date, id);
                });

//                var fields = ["#category-"+id, "#item-"+id, "#employee-"+id, "#date-"+id, "#time-"+id];
//                console.log(fields);
//                $(fields).on('change', function () {
//                    alert('hello');
//                    if (allFilled()) {
//                        $('#appointment_next_btn').removeAttr('disabled');
//
//                    } else {
//                        $('#appointment_next_btn').attr('disabled', 'disabled');
//                    }
//                });
//
//                function allFilled() {
//                    var filled = true;
//                    $(fields).each(function () {
//                        if ($(this).val() == '') {
//                            filled = false;
//
//                        }
//                    });
//                    return filled;
//                    ;
//                }


                    $('#appointment_next_btn').attr('disabled', 'disabled');
                    $('#time-' + id).change(function () {
                        let time_flag=time_check();
//                        alert(time_flag);
                        if(time_flag==0)
                        {
                            $('#appointment_next_btn').attr('disabled', 'disabled');
                        }
                        else{
                            $('#appointment_next_btn').removeAttr('disabled');
                        }

                    });
            }

            let service_id = 2;

            $('#add_service').on('click', function () {
                $('#appointment_next_btn').removeAttr('disabled');
                $('#more_service').append(`<br>
            <div class="form-group row">
                    <div class="col-sm-4">
                    <label>Step 1: Category</label>
            <div class="">
                    <select class=" form-control dynamic-${service_id}" data-dependent="item-${service_id}" name="category[]" id="category-${service_id}">
                    <option>Select category</option>
            @foreach ($categories as $category)
                <option value="{{$category->id}}">{{$category->title}}</option>
                    @foreach ($category->child as $child)
                <option value="{{ $child->id }}">--{{ $child->title }}</option>
                    @endforeach
                @endforeach
                </select>
                        </div>
                        </div>
                        <div class="col-sm-4">
                        <label>Step 2: Service</label>
                <div class="">
                        <select class="form-control dynamic-${service_id} dynamical-${service_id}" name="item[]" id="item-${service_id}" data-dependent="employee-${service_id}">
                    <option>Select Service</option>
            </select>
            </div>
            </div>
            <div class="col-sm-4">
                    <label>Step 3: Employee</label>
            <div class="">
                    <select class="form-control dynamical" name="employee[]" id="employee-${service_id}">
                    <option>Select Employee</option>
            </select>
            </div>


            </div>
            </div>
            <div class="form-group row">
                    <div class="col-md-3">

                    <label>Step 4: Date</label>
            <input type="text" placeholder="Select date" class="datepicker" id="date-${service_id}" name="date[]">

                    </div>
                    <div class="col-md-4 mad_col_width">

                    <div class="mad_week_list">
                    <ul class="clearfix">
                    <li>

                    <h6>Mon</h6>
                    <div class="icon_check"></div>

                    </li>
                    <li>

                    <h6>Tue</h6>
                    <div class="icon_check"></div>

                    </li>
                    <li>

                    <h6>Wed</h6>
                    <div class="icon_check"></div>

                    </li>
                    <li>

                    <h6>Thu</h6>
                    <div class="icon_check"></div>

                    </li>
                    <li>

                    <h6>Fri</h6>
                    <div class="icon_check"></div>

                    </li>
                    <li>

                    <h6>Sat</h6>
                    <div class="icon_check"></div>

                    </li>
                    <li>

                    <h6>Sun</h6>
                    <div class="icon_check"></div>
                    {{--<div class="icon_asterick"></div>--}}

                </li>
                </ul>
                </div>

                </div>
                <div class="col-md-2">
                <label>Step 5: Time</label>
        <div class="">
                <select class="form-control time_check" id="time-${service_id}" name="time[]">
                <option  value="0" selected="selected">Select Time</option>
        </select>
        </div>


        </div>
        <div class="col-md-2">



                </div>
                </div>`);
                category(service_id);

                service_id++;
            });
        });

        function appointments_dates(_employee, id) {
            _employee = _employee;
            var closes = "{{\App\Appointment::closedDay()}}";
            url = "{{ route('appointment_dates_load') }}";
            // console.log(closes);
            $.ajax(url, {
                type: 'post',
                start: $('#date-' + id).LoadingOverlay('show'),
                data: {
                    _token: "{{csrf_token()}}",
                    _employee: _employee,
                },
                success: function (response) {
                    // console.log(response);
                    var length = Object.keys(response).length;
                    // console.log(length);
                    $('.datepicker').datepicker({
                        minDate: new Date(),
                        beforeShowDay: function (d) {
//                            let getDays=[];
                            var day = d.getDay();
                            var isDisabled = ($.inArray(day, response) != -1);
                            return [day != closes && !isDisabled];
//                            return [(day != closes && day != response)];
                        }
                    });
                    $('#date-' + id).LoadingOverlay('hide');

                }
            });

        }


        function appointment_date(_employee, _date, id) {
            _date = _date;
            _employee = _employee;
            url = "{{ route('appointment_time_load') }}";
            $.ajax(url, {
                type: 'post',
                start: $('#time-' + id).LoadingOverlay('show'),
                data: {
                    _token: "{{csrf_token()}}",
                    _date: _date,
                    _employee: _employee,
                },
                success: function (response) {
                    // console.log(response);
                    $('#time-' + id).html(response);
                    $('#time-' + id).LoadingOverlay('hide');
                }
            });
        }

        function time_check()
        {
            let time_id=[];
            let flag=1;
            $('.time_check').each(function(){
                time_id.push(this.id);
            });
//            console.log(time_id);
            $(time_id).each(function(k,v){
//            console.log(v);
                let valu=$('#'+v).val();
                if(valu=='0')
                {
                    flag=0;
                }
//                console.log(valu);

            });


            return flag;
        }
        //reset function
        function reset_function(id)
        {

            $('#employee-' + id).html('<option>Select Employee</option>');
//            $('#time-' + id).html('<option value="0">Select Time</option>');
////            $('.datepicker').datepicker(
////                'setDate', null
////            );
        }
    </script>
    <script !src="">

        function myFunction() {
            // Get the checkbox
            var checkBox = document.getElementById("myCheck");
            // Get the output text
            var text = document.getElementById("text");

            // If the checkbox is checked, display the output text
            if (checkBox.checked == true) {
                text.style.display = "block";
            } else {
                text.style.display = "none";
            }
        }
    </script>
    <script !src="">
        $('#appointment_next_btn').on('click', function () {
            var item_array = $("select[name='item[]']")
                .map(function () {
                    return $(this).val();
                }).get();

            console.log(item_array);
            $.ajax({
                data: {
                    _token: '{{csrf_token()}}',
                    items: item_array
                },
//                start: $('#detail_cost','#detail_costs','#detail_costed').LoadingOverlay('show'),
                url: "{{route('appointmentService')}}",
                success: function (result) {
                    console.log(result);
                    $('#detail_cost').html(result);
//                    $('#detail_cost').LoadingOverlay('hide');
                    $('#detail_costs').html(result);
//                    $('#detail_costs').LoadingOverlay('hide');
                    $('#detail_costed').html(result);
//                    $('#detail_costed').LoadingOverlay('hide');

                }
            });
        });



    </script>

@endsection
