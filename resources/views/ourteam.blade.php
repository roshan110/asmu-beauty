@extends('layouts.app')

@section('contents')
<div class="mad_section">

    <div class="container">

        <div class="mad_section_offset_2">
            <div class="row">
                <div class="col-sm-12">

                    <h3>{{\App\Label::ofValue('ourteam:page_head')}}</h3>
                    <p class="mad_text_style1 mad_section_offset_2">{!! nl2br(\App\Label::ofValue('ourteam:page_subhead')) !!}</p>

                </div>
            </div>
            <div class="row">
                @foreach($teams as $team)
                <div class="col-sm-4">
                    <div class="mad_gallery_item">

                        <a href="#" class="mad_item_hover style2">

                            <figure>
                                <img src="{{$team->image ? $team->image('360x395') : asset('images/women_model1.png')}}" alt="{{$team->name}}">
                            </figure>
                            <div class="mad_item_desc with_bg_img">
                                <div class="mad_author style2">
                                    <h3 class="mad_title_style2" style="font-size: 38px;">{{$team->name}}</h3>
                                    {{--<ul style="display: inline-block">--}}
                                    @php $counted=$team->items()->count(); $br='<br>'; @endphp
                                    @php if($counted>2){ echo $br;} @endphp
                                    @foreach($team->items as $item)
                                            <span>{{$item->title}}</span>
                                    @endforeach
                                    {{--</ul>--}}
                                </div>
                            </div>

                        </a>
                        <div class="mad_gallery_text" style="min-height: 209px;">
                            {!! $team->details !!}
                            {{--<div class="social_icon_list type2 style2">--}}
                                {{--<ul>--}}
                                    {{--<li><a href="#" class="soc_icon icon-facebook"></a></li>--}}
                                    {{--<li><a href="#" class="soc_icon icon-twitter"></a></li>--}}
                                    {{--<li><a href="#" class="soc_icon icon-gplus"></a></li>--}}
                                    {{--<li><a href="#" class="soc_icon icon-tumblr"></a></li>--}}
                                    {{--<li><a href="#" class="soc_icon icon-instagram"></a></li>--}}
                                    {{--<li><a href="#" class="soc_icon icon-pinterest"></a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        </div>

                    </div>
                </div>
                @endforeach
            </div>
        </div>



    </div>
</div>
@endsection