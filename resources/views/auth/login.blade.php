@extends('layouts.app_secondary')
@section('page_header')
    Login
@endsection

@section('contents')

    <div class="mad_section_2">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <div class="mad_item_offset_2">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="login_credential_heading login_credential_heading_left">Login Form</h3>
                            </div>
                            <div class="col-sm-6">
                                <h3 class="login_credential_heading login_credential_heading_right">New User? <a href="{{route('register')}}">Register Here</a></h3>
                            </div>

                        </div>


                        <p class="mad_item_offset_none align_center">{!! nl2br(\App\Label::ofValue('login:page_head_body')) !!}</p>
                    </div>

                    <div class="mad_item_offset" style="margin-top: 30px;">
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <form id="contact_form" class="contact_form" action="{{route('login')}}" method="POST" role="form">
                                    {!! csrf_field() !!}
                                    <ul>
                                        <li>
                                            <input type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" id="email" required>
                                            @if ($errors->has('email'))
                                                <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </li>
                                        <li>
                                            <input type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" id="password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block alert_box error">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                 </span>
                                            @endif
                                        </li>

                                    </ul>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="control-group form-elements login_credential_heading_left">

                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="remember"><label for="remember">Remember Me</label>

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <p class="login_credential_heading_right"><a href="{{ route('password.request') }}" style="color: #2965b0;">Forget Password??</a></p>
                                        </div>

                                    </div>
                                    <div class="align_center">
                                        <button type="submit" class="mad_button small_button">Login Now</button>
                                    </div>
                                </form>

                            </div>
                            <div class="col-sm-3"></div>

                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

@endsection