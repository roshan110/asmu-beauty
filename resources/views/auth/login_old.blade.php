<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Login :: {{ config('app.name') }}</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="{{ asset('images/admin/w3web.jpg') }}" type="image/x-icon" />

    <style>
        body {
            margin: 50px 0 0 0;
        }
    </style>
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <script>
        $(function(){ $('.topbar').dropdown(); });
    </script>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="login-wrap">
                <div class="logo"><img src="{{App\Setting::getLogo()  }}" alt="{{ config('app.name') }}" class="img-responsive" width="200" /></div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <center>
                            <h2>Welcome</h2>
                            <p>Please enter your login details below to authenticate.</p>
                        </center>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('admin.login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span> @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>                                        @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span> @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('password.request') }}">Forget Password</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer"><span class="pull-left">IP Logged: <?=Request::ip();?></span><span class="pull-right">&copy; &amp; Powered by <a href="http://w3web.co.uk/" target="_blank">W3 Web Technology</a></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3"></div>
    </div>
</div>


</body>

</html>