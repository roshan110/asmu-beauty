@extends('layouts.app_secondary')
@section('page_header')
    Register
@endsection

@section('contents')

    <div class="mad_section_2">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <div class="mad_item_offset_2">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="login_credential_heading login_credential_heading_left">Registration Form</h3>
                            </div>
                            <div class="col-sm-6">
                                <h3 class="login_credential_heading login_credential_heading_right">Already User? <a href="{{route('login')}}">Login Here</a></h3>
                            </div>

                        </div>


                        <p class="mad_item_offset_none align_center">{!! nl2br(\App\Label::ofValue('register:page_head_body')) !!}</p>
                    </div>

                    <div class="mad_item_offset" style="margin-top: 30px;">
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <form id="contact_form" class="contact_form" action="{{route('register')}}" method="POST" role="form">
                                    {!! csrf_field() !!}
                                    <ul>
                                        <li>
                                            <input type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Enter Name" name="name" id="name" value="{{ old('name') }}" required>
                                            @if ($errors->has('name'))
                                                <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </li>
                                        <li>
                                            <input type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" id="email" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                                <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </li>
                                        <li>
                                            <input type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" id="password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block alert_box error">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                 </span>
                                            @endif
                                        </li>
                                        <li>
                                            <input type="password" class="" placeholder="Confirm Password" name="password_confirmation" id="password-confirm" required>
                                        </li>
                                    </ul>

                                    <div class="align_center">
                                        <button type="submit" class="mad_button small_button">Register Now</button>
                                    </div>
                                </form>

                            </div>
                            <div class="col-sm-3"></div>

                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

@endsection