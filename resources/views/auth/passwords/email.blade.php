@extends('layouts.app_secondary')
@section('page_header')
    Reset Password
@endsection

@section('contents')

    <div class="mad_section_2">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <div class="mad_item_offset_2">
                        <div class="row">
                            <div class="col-sm-12 align_center">
                                <h3 class="login_credential_heading">Reset Password</h3>
                                @if (\Session::has('status'))
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{\Session::get('status')}}</strong>
                                    </div>
                                @endif

                            </div>

                        </div>
                    </div>

                    <div class="mad_item_offset" style="margin-top: 30px;">
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">

                                <form id="contact_form" class="contact_form" action="{{ route('password.email') }}" method="POST" role="form">
                                    {!! csrf_field() !!}
                                    <ul>
                                        <li>
                                            <input type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="E-mail Address" name="email" id="email" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                                <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </li>


                                    </ul>

                                    <div class="align_center">
                                        <button type="submit" class="mad_button small_button">Send Password Reset Link</button>
                                    </div>
                                </form>

                            </div>
                            <div class="col-sm-3"></div>

                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

@endsection

@section('script')
    <script src="{{asset('frontend/plugins/bootstrap.js')}}"></script>


@endsection