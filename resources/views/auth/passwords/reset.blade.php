@extends('layouts.app_secondary')
@section('page_header')
    Password Reset
@endsection

@section('contents')

    <div class="mad_section_2">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <div class="mad_item_offset_2">
                        <div class="row">
                            <div class="col-sm-12 align_center">
                                <h3 class="login_credential_heading login_credential_heading_left">Password Reset</h3>
                            </div>

                        </div>
                    </div>

                    <div class="mad_item_offset" style="margin-top: 30px;">
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <form id="contact_form" class="contact_form" action="{{ route('password.request') }}" method="POST" role="form">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="token" value="{{ $token }}">
                                    <ul>
                                        <li>
                                            <input type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="E-mail Address" name="email" id="email" value="{{ $email or old('email') }}" required autofocus>
                                            @if ($errors->has('email'))
                                                <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </li>
                                        <li>
                                            <input type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" id="password" required>
                                            @if ($errors->has('password'))
                                                <span class="help-block alert_box error">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                 </span>
                                            @endif
                                        </li>
                                        <li>
                                            <input type="password" class="{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"  placeholder="Confirm Password" name="password_confirmation" id="password-confirm" required>
                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block alert_box error">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                 </span>
                                            @endif
                                        </li>

                                    </ul>

                                    <div class="align_center">
                                        <button type="submit" class="mad_button small_button">Reset Password</button>
                                    </div>
                                </form>

                            </div>
                            <div class="col-sm-3"></div>

                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

@endsection