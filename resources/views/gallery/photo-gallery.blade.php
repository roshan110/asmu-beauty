@extends('layouts.app')

@section('contents')

    <div class="mad_section">

        <div class="container">

            <div class="row">

                <div class="col-sm-12">

                    <div class="align_center">
                        <h3 class="mad_title_style1">Our</h3>
                        <h2>Photo Gallery</h2>
                    </div>

                    <div class="tabs_sort post_area var2 type2">

                        <div id="options">
                            <div id="filters" class="button-group js-radio-button-group">
                                <button class="is-checked" data-filter="*">All</button>
                                @foreach($albums as $album)
                                <button data-filter=".category_{{$album->id}}">{{$album->title}}</button>
                                @endforeach
                            </div>
                        </div>

                        <div class="isotope three_collumn clearfix post_news" data-isotope-options='{"itemSelector" : ".item","layoutMode" : "fitRows","transitionDuration":"0.7s","fitRows" : {"columnWidth":".item"}}'>
                            @foreach($photos as $photo)
                            <div class="item category_{{$photo->album_id}}">

                                <div class="mad_gallery_item">

                                    <div class="mad_item_hover">
                                        <figure>
                                            <img src="{{ $photo->image('400x300') }}" alt="">
                                        </figure>
                                        <div class="item_overlay">
                                            <div class="text_holder">
                                                <a href="{{ asset('uploads/photo/' . $photo->image) }}" class="mad_icon_plus gallery" rel="category"></a>
                                                <a href="{{ asset('uploads/photo/' . $photo->image) }}" class="mad_icon_link"></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            @endforeach

                        </div>

                    </div>

                    {{--<div class="align_center">--}}
                        {{--<a href="#" id="load_more" class="mad_button">Load More</a>--}}
                    {{--</div>--}}

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script src="{{asset('frontend/plugins/isotope.pkgd.min.js')}}"></script>
@endsection