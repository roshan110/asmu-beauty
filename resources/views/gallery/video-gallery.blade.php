@extends('layouts.app')

@section('contents')

    <div class="mad_section">

        <div class="container">

            <div class="row">

                <div class="col-sm-12">

                    <div class="align_center">
                        <h3 class="mad_title_style1">Our</h3>
                        <h2>Video Gallery</h2>
                    </div>

                    <div class="tabs_sort post_area var2 type2">

                        <div id="options">
                            <div id="filters" class="button-group js-radio-button-group">
                                <button class="is-checked" data-filter="*">All</button>
                                <button data-filter=".category_2">Youtube</button>
                                <button data-filter=".category_3">Facebook</button>
                            </div>
                        </div>

                        <div class="isotope three_collumn clearfix post_news" data-isotope-options='{"itemSelector" : ".item","layoutMode" : "fitRows","transitionDuration":"0.7s","fitRows" : {"columnWidth":".item"}}'>
                            @foreach($videos as $video)
                                <div class="item category_2">
                                    <div class="mad_gallery_item">

                                        <div class="mad_item_hover">
                                            <figure>
                                                <iframe width="100%" height="200" src="https://www.youtube.com/embed/{{$video->video_id}}" frameborder="0" allowfullscreen></iframe>
                                            </figure>

                                        </div>

                                    </div>

                                </div>
                            @endforeach

                            @foreach($fbvideos as $fbvideo)
                            <div class="item category_3">

                                <div class="mad_gallery_item">

                                    <div class="mad_item_hover">
                                        <figure>
                                            <div class="fb-video"
                                                 data-href="{{$fbvideo->link}}"
                                                 data-width="500"
                                                 data-allowfullscreen="true" style="height: 250px;">
                                            </div>
                                        </figure>

                                    </div>

                                </div>

                            </div>
                            @endforeach
                        </div>

                    </div>

                    {{--<div class="align_center">--}}
                        {{--<a href="#" id="load_more" class="mad_button">Load More</a>--}}
                    {{--</div>--}}

                </div>

            </div>

        </div>
    </div>

@endsection

@section('script')
    <script src="{{asset('frontend/plugins/isotope.pkgd.min.js')}}"></script>
@endsection