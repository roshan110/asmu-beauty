{{--Footer--}}
    <footer id="footer" class="footer">
        <div id="contact2" class="mad_section with_bg_2">

            <div class="container">

                <div class="mad_contact_items mad_flex_list">

                    <div class="row">
                        <div class="col-md-3 col-sm-6">

                            <div class="mad_contact_item">
                                <h4 class="lnr lnr-location">{{\App\Label::ofValue('home:footer_contact_head')}}</h4>
                                <p>{!! nl2br(Label::ofValue('global:address'))  !!}</p>
                                <a href="mailto: {!! nl2br(Label::ofValue('global:email'))  !!}">{!! nl2br(Label::ofValue('global:email'))  !!}</a>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-6">

                            <div class="mad_contact_item">
                                <h4 class="lnr lnr-clock">{{\App\Label::ofValue('home:footer_opening_head')}}</h4>
                                <p>{!! nl2br(Label::ofValue('home:footer_opening_body')) !!}</p>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-6">

                            <div class="mad_contact_item">
                                <h4 class="lnr lnr-calendar-full">{{\App\Label::ofValue('home:footer_appointment_head')}}</h4>
                                <p>{!! nl2br(Label::ofValue('home:footer_appointment_body')) !!}</p>
                                <a href="tel: {!! nl2br(Label::ofValue('global:mobile'))  !!}">Phone: {!! nl2br(Label::ofValue('global:mobile'))  !!}</a>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-6 align-center">

                            <div class="fb-page" data-href="{{ $facebook->link }}" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="{{ $facebook->link }}" class="fb-xfbml-parse-ignore"><a href="{{ $facebook->link }}">{{ config('app.name') }}</a></blockquote></div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
        <!-- Footer section -->
        <div class="mad_top_footer">

            <div class="v_align_center_blocks">

                <div class="container">

                    <div class="row">

                        <div class="col-sm-4">
                            <span>&copy; {{date('Y')}} Asmu Beauty Spa</span>
                        </div>
                        <div class="col-sm-4">
                            <div class="mad_nav_list text-center">
                                <ul>
                                    @foreach ($footer_link as $item)
                                        <li class=""><a href="{{ url($item->slug) }}">{{$item->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- <a href="#" class="mad_logo_container">
                              <img src="images/logo.png" alt="">
                            </a> -->

                        </div>
                        <div class="col-sm-4">

                            <div class="align_right">
                                <div class="developer_info">
                                    <B>Developed By:</B> <a href="https://w3web.co.uk/" target="_blank"><b>W3 Web Technology</b></a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </footer>
{{--End Footer--}}

{{--Cookie Consent--}}
<div class="cookie_message" id="cookie_message">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 align_center">
                <p>We use cookies to make your experience better. To comply with the new e-Privacy directive, we need to ask for your consent to set the cookies.</p>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <button id="cookie_yes" class="mad_button">Yes I consent </button>
                        <a href="{{url('terms-and-condition')}}" class="mad_button style2">Learn More</a>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>

        </div>
    </div>
</div>
{{--End Cookie Consent--}}