<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9" lang="en"><![endif]-->
<html lang="en">
<head>

    <!-- Google Web Fonts
    ================================================== -->

    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,300italic,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Mrs+Saint+Delafield' rel='stylesheet' type='text/css'>

    <!-- Basic Page Needs
    ================================================== -->

    <title>Asmu Beauty Spa</title>

    <meta name = "format-detection" content = "telephone=no" />
    <meta charset="utf-8">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!--meta info-->
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- Favicons
  ================================================== -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/fav_icon.ico')}}">
    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/fontello.css')}}">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('frontend/css/jquery.fancybox.css')}}">

    <!-- RS5.0 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/revolution/css/settings.css')}}">
    @yield('css')
    <!-- HTML5 Shiv
    ================================================== -->
    <script src="{{asset('frontend/plugins/jquery.modernizr.js')}}"></script>
    {{--Font Awesome--}}
    <link rel="stylesheet" href="{{asset('fontawesome/css/font-awesome.css')}}">
</head>
<body id="" class="wide_layout">
{{--<body id="@hasSection('is_home')'index' @else ''@endif" class="wide_layout">--}}



<div class="loader"></div>

<!-- <div class="loader"></div> -->

<!--[if (lt IE 9) | IE 9]>
<div class="ie_message_block">
    <div class="container">
        <div class="wrapper">
            <div class="clearfix"><i class="fa fa-exclamation-triangle f_left"></i><b>Attention!</b> This page may   not display correctly. You are using an outdated version of Internet Explorer. For a faster, safer browsing experience.<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode" class="button button_type_3 button_grey_light f_right" target="_blank">Update Now!</a></div>
        </div>
    </div>
</div>
<![endif]-->

<!--cookie-->
<!-- <div class="cookie">
        <div class="container">
          <div class="clearfix">
            <span>Please note this website requires cookies in order to function correctly, they do not store any specific information about you personally.</span>
            <div class="f_right"><a href="#" class="button button_type_3 button_orange">Accept Cookies</a><a href="#" class="button button_type_3 button_grey_light">Read More</a></div>
          </div>
        </div>
      </div>-->

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper" class="wrapper_container">

    <!-- - - - - - - - - - - - - Mobile Menu - - - - - - - - - - - - - - -->

    <nav id="mobile-advanced" class="mobile-advanced"></nav>

    <!-- - - - - - - - - - - - / Mobile Menu - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

    @include('layouts.header_secondary')

            <!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->

    <!--#################################
    - REVOLUTION SLIDER -
    #################################-->

    <div class="mad_page_title paralax_image_bg1 v_align_center_blocks" style="background:url('{{App\Setting::getBg()  }}');">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <h2>@yield('page_header')</h2>
                    <div class="mad_breadcrumbs">

                        <nav>
                            <a href="{{route('_root_')}}">Home</a>@yield('page_header')
                        </nav>

                    </div>

                </div>
            </div>

        </div>

    </div>


    <!-- - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->

    <div id="content">
        @yield('contents')
    </div>
            <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->
    @include('layouts.footer')
            <!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->


</div>
<!-- - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - -->

<!--scripts include-->
<script src="{{asset('frontend/js/jquery-2.1.0.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('frontend/plugins/jquery.queryloader2.min.js')}}"></script>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0"></script>
<script src="{{asset('frontend/plugins/owl.carousel.min.js')}}"></script>
{{--<script src="{{asset('frontend/plugins/retina.js')}}"></script>--}}
<script src="{{asset('frontend/plugins/twitter/jquery.tweet.js')}}"></script>
<script src="{{asset('frontend/plugins/jquery.fancybox.js')}}"></script>
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script src="{{asset('frontend/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}}"></script>
<script src="{{asset('frontend/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0')}}"></script>

{{--<script src="{{asset('frontend/plugins/twitter/jquery.tweet.js')}}"></script>--}}
<script src="{{asset('frontend/plugins/instafeed.min.js')}}"></script>
<script src="{{asset('frontend/js/plugins.js')}}"></script>
<script src="{{asset('frontend/js/script.js')}}"></script>
<script src="{{ asset('frontend/js/jquery.cookie.js') }}"></script>

<script>
    $(function () {
        //cookies message
        $('#cookie_yes').on('click', function () {
            $.cookie('asmu_message', 'yes', {
                expiry: 0,
                domain: '',
                path: ''
            });
            $('#cookie_message').slideToggle('hide');
        });
        if (typeof $.cookie('asmu_message') == 'undefined') {
            $('#cookie_message').slideToggle('slow');
        }
    });
</script>
@yield('script')

</body>
</html>