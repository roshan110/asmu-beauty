
    <div class="tp-banner-container">

        <!-- START REVOLUTION SLIDER 5.0 -->
        <div class="rev_slider_wrapper">

            <div id="slider1" class="rev_slider">

                <ul>
                    @foreach($slides as $slide)
                    <li data-transition="zoomout" data-slotamount="default" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-fstransition="fade" data-fsmasterspeed="1500">

                        <!-- MAIN IMAGE -->
                        <img src="{{asset('uploads/slide/'.$slide->image)}}"  alt="{{$slide->title}}">
                        <div class="tp-caption text1 {{$slide->order_by % 2 != 0 ? 'align_center' : 'align_left'}} fadeout"
                             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-start="500"
                             data-x="{{$slide->order_by % 2 != 0 ? 'center' : 'left'}}"
                             data-y="top">
                            <h5>- {{$slide->title}} -</h5>
                            <h2>{!! $slide->details !!}</h2>
                            {{--<a href="appointment_page.html" class="mad_button">Register Now</a>--}}
                        </div>

                    </li>
                    @endforeach


                </ul>

                <svg class="separator_type_5_path" preserveAspectRatio="none" viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 0 L70 100 L100.1 0 L100.1 100 L0 100 Z" fill="#fff" stroke="#fff" stroke-width="-1"></path>
                    <path d="M0 0 L0 0 L50 100 L100 0 L100 0 Z" fill="transparent"></path>
                </svg>



            </div><!-- END REVOLUTION SLIDER -->
        </div><!-- END OF SLIDER WRAPPER -->

    </div>

