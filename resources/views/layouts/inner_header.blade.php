
    <div class="mad_page_title paralax_image_bg1 v_align_center_blocks" style="background:url('{{App\Setting::getBg()  }}');">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <h2>{{ $page->title }}</h2>
                    <div class="mad_breadcrumbs">

                        <nav>
                            <a href="{{route('_root_')}}">Home</a>{{ $page->title }}
                        </nav>

                    </div>

                </div>
            </div>

        </div>

    </div>
