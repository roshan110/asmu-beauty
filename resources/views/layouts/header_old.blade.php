<header id="header" class="header-main rsAbsoluteEl">

    <!-- top-header -->

    <div class="mad_top_header">

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <a href="#" class="logo"><img src="{{asset('images/logo.png')}}" alt=""></a>

                </div>
                <div class="col-md-8">

                    <div class="header_info_section">
                        <div class="head_socials">

                            <p class="icon_location">121 King Street, London</p>
                            <div class="social_icon_list">
                                <ul>
                                    <li><a href="#" class="soc_icon icon-facebook"></a></li>
                                    <li><a href="#" class="soc_icon icon-twitter"></a></li>
                                    <li><a href="#" class="soc_icon icon-gplus"></a></li>
                                    <li><a href="#" class="soc_icon icon-tumblr"></a></li>
                                    <li><a href="#" class="soc_icon icon-instagram"></a></li>
                                    <li><a href="#" class="soc_icon icon-pinterest"></a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="head_phone">

                            <div><p class="icon_mobile">+44 7428 581 136</p></div>
                            <a href="#" class="mad_button">Make an Appointment</a>

                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!--main menu-->

    <div class="menu_holder">

        <div class="container">

            <div class="menu_wrap">

                <!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->

                <div class="nav_item">

                    <nav id="main_navigation" class="main_navigation">
                        <ul id="menu" class="clearfix">
                            <li class="current"><a href="#">Home</a></li>
                            <li class="drop"><a href="#">services</a>
                                <!--sub menu-->
                                <div class="sub_menu_wrap clearfix">
                                    <ul>
                                        <li><a href="#">Threading</a></li>
                                        <li><a href="#">Eyelash Extensions</a></li>
                                        <li><a href="#">Manicure</a></li>
                                        <li><a href="#">Pedicure</a></li>
                                        <li><a href="#">Hair Cut</a></li>
                                        <li><a href="#">Waxcing</a></li>
                                    </ul>
                                    <li class="drop"><a href="#">About</a>

                                        <!--sub menu-->
                                        <div class="sub_menu_wrap clearfix">
                                            <ul>
                                                <li><a href="about_us.html">About Us</a></li>
                                                <li><a href="team.html">Our Team</a></li>


                                            </ul>
                                        </div>

                                    </li>

                                    <li class="drop"><a href="photo-gallery.html">Gallery</a>
                                        <!-- <a href="#" class="mad_button style2">##</a> -->
                                        <!--sub menu-->
                                        <div class="sub_menu_wrap clearfix">
                                            <ul>
                                                <li><a href="photo-gallery.html">Photo Gallery</a></li>
                                                <li><a href="#">Video Gallery</a></li>
                                            </ul>
                                            <li><a href="testimonials.html">Testimonials</a></li>

                                            <!--sub menu-->
                                            <div class="sub_menu_wrap clearfix">
                                                <ul>
                                                    <li><a href="portfolio_sortable_masonry_3columns.html">Sortable Masonry - 3 Columns</a></li>
                                                    <li><a href="portfolio_sortable_3columns.html">Sortable Grid - 3 Columns</a></li>
                                                    <li><a href="portfolio_classic_sortable_3columns.html">Sortable Classic - 3 Columns</a></li>
                                                    <li><a href="portfolio_single_page_v1.html">Single Portfolio Page v1</a></li>
                                                    <li><a href="portfolio_single_page_v2.html">Single Portfolio Page v2</a></li>
                                                </ul>
                                            </div>

                                    </li>
                                    <li><a href="blog.html">Blog</a></li>

                                    <li><a href="contact.html">Contact</a></li>

                                    <li><a href="membership.html">Membership</a></li>
                        </ul>
                    </nav>

                </div>

                <!-- - - - - - - - - - - - - end Navigation - - - - - - - - - - - - - - - -->

            </div>

        </div>

    </div>

</header>