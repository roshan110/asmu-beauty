
    <header id="header" class="header-main {{ $page->slug=='/' ? 'rsAbsoluteEl' : '' }}">

    <!-- top-header -->

    <div class="mad_top_header">

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <a href="{{route('_root_')}}" class="logo"><img src="{{ App\Setting::getLogo() }}" alt=""></a>

                </div>
                <div class="col-md-8">

                    <div class="header_info_section">
                        <div class="head_socials">
                            {{--<div class="emails_mobiles_list">--}}
                                {{--<ul>--}}
                                    {{--<li class="icon_emails"><a href="mailto:{!! nl2br(Label::ofValue('global:email'))  !!}">{!! nl2br(Label::ofValue('global:email'))  !!}</a></li>--}}
                                    {{--<li class="icon_mobiles"><a href="tel:{!! nl2br(Label::ofValue('global:mobile'))  !!}" >{!! nl2br(Label::ofValue('global:mobile'))  !!}</a></li>--}}

                                {{--</ul>--}}
                            {{--</div>--}}
                            <p class="icon_emails"><a href="mailto:{!! nl2br(Label::ofValue('global:email'))  !!}">{!! nl2br(Label::ofValue('global:email'))  !!}</a></p>
                            <p class="icon_mobiles"><a href="tel:{!! nl2br(Label::ofValue('global:mobile'))  !!}" style="margin-top: -2px;">{!! nl2br(Label::ofValue('global:mobile'))  !!}</a></p>
                            <div class="social_icon_list" style="text-align: center;">
                                <ul>
                                    @foreach($socials as $social)
                                    <li><a href="{{ $social->link }}" class="soc_icon" style="background: {{$social->color}}; "><i class="fa fa-{{ $social->icon }}" aria-hidden="true" style="margin-top: 6px;"></i></a></li>
                                    @endforeach
                                </ul>
                            </div>

                        </div>
                        <div class="head_phone">
                            @auth
                            <div class="dropdowned">
                                <button class="dropbtned"><span class="icon-user" style="color:#CE3D5F;display: inherit;font-size: 24px;padding-right: 10px;"></span>{{Auth::user()->name}}</button>
                                <div class="dropdowned-content">
                                    <a href="{{route('user.profile')}}">Profile</a>
                                    <a href="{{ route('admin.logout') }}" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                            @else
                            <div>
                                <p class="icon-user"><a href="{{route('login')}}" style="color:#fff;">Login/Register</a></p>
                            </div>
                            @endif
                            <a href="{{route('appointment')}}" class="mad_button" style="margin-top: -1px;">Make an Appointment</a>

                        </div>
                    </div>

                </div>


            </div>

        </div>

    </div>

    <!--main menu-->

    <div class="menu_holder">

        <div class="container">

            <div class="menu_wrap">

                <!-- - - - - - - - - - - - - - Navigation - - - - - - - - - - - - - - - - -->

                <div class="nav_item">

                    <nav id="main_navigation" class="main_navigation">
                        <ul id="menu" class="clearfix">
                            @foreach ($main_menu as $link=>$menu)
                                @if (is_array($menu))
                                    <li class="drop">
                                        <a class="" href="#">{{ $menu[$link] }}</a>
                                        <div class="sub_menu_wrap clearfix">
                                            <ul>
                                                @foreach ($menu['sub'] as $sub_link=>$sub_menu)
                                                <li><a href="{{ url($sub_link) }}">{{ $sub_menu }}</a></li>


                                                @endforeach
                                            </ul>
                                        </div>

                                    </li>
                                @else
                                    <li class="">
                                        <a class="" href="{{ url($link) }}">
                                            {{ $menu }}
                                        </a>
                                    </li>
                                @endif
                            @endforeach



                        </ul>
                    </nav>

                </div>

                <!-- - - - - - - - - - - - - end Navigation - - - - - - - - - - - - - - - -->

            </div>

        </div>

    </div>

</header>
