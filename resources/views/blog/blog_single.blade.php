@extends('layouts.app')

@section('contents')

    <div class="mad_section">

        <div class="container">

            <div class="row">

                <div class="col-sm-12">

                    <div class="mad_post_nav clearfix">

                        <div class="mad_post_options">
                            <a href="{{route('blogs.main')}}" class="icon-th">Back to Blog</a>
                            {{--<a href="#" class="icon-share-1 share_popup">Share</a>--}}
                        </div>

                        {{--<div class="mad_post_dir">--}}
                            {{--<a href="#" class="prev_post">Previous Post</a>--}}
                            {{--<a href="#" class="next_post">Next Post</a>--}}
                        {{--</div>--}}

                    </div>

                </div>

            </div>

            <div class="row">

                <!-- Main content -->
                <main id="main" class="col-md-8">

                    <!-- Image post -->
                    <div class="mad_blog_post blog_single_post">

                        <figure>
                            <img src="{{$blog->image('769x500')}}" alt="">
                            <div class="mad_post_date">

                                <div class="date">{{$blog->created_at->format('d / M / Y')}}</div>

                            </div>
                        </figure>
                        <div class="mad_post_content clearfix">
                            <div class="mad_post_info">

                                <h2><a href="#">{{$blog->title}}</a></h2>
                                <div class="mad_post_action">
                                    <a href="#" class="admin">{{$blog->author_name}}</a>
                                    <a href="#" class="days">{{$blog->time==0 ?'Today':$blog->time.' days ago' }}</a>
                                    <a href="#" class="comment">{{$blog->visitors}} hits</a>
                                </div>
                                {!!$blog->details!!}

                            </div>
                        </div>

                        <div class="mad_post_text">



                        </div>





                    </div>

                </main>

                <!-- Sidebar -->
                <aside id="sidebar" class="col-md-4">



                    <!-- Categories -->
                    <div class="mad_widget widget_categories">

                        <h5 class="widget_title">Categories</h5>
                        <ul>
                            @php $i=1; @endphp
                            @foreach($services_c as $sc)
                                <li><a href="{{$sc->slug}}">{{$sc->title}}<span>#{{$i}}</span></a></li>
                                @php $i++; @endphp
                            @endforeach
                        </ul>

                    </div>

                    <!-- Recent Posts -->
                    <div class="mad_widget widget_posts">

                        <h5 class="widget_title">Recent blogs</h5>
                        <div class="mad_post_section">
                            @foreach($latest_blogs as $latest_blog)
                            <div class="mad_post_item">

                                <figure><a href="#"><img src="{{ $latest_blog->image('70x70') }}" alt=""></a></figure>
                                <div class="post_text_block">
                                    <a href="{{route('singleblog',$latest_blog->id)}}">{{$latest_blog->title}}</a>
                                </div>

                            </div>
                            @endforeach

                        </div>

                    </div>




                </aside>

            </div>
        </div>
    </div>

@endsection