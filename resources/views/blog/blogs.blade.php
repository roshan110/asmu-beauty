@extends('layouts.app')

@section('contents')

    <div class="mad_section">

        <div class="container">

            <div class="row">

                <!-- Sidebar -->
                <div id="sidebar" class="col-md-4">



                    <!-- Categories -->
                    <div class="mad_widget widget_categories">

                        <h5 class="widget_title">Categories</h5>
                        <ul>
                            @php $i=1; @endphp
                            @foreach($services_c as $sc)
                            <li><a href="{{$sc->slug}}">{{$sc->title}}<span>#{{$i}}</span></a></li>
                                @php $i++; @endphp
                            @endforeach
                        </ul>

                    </div>

                    <!-- Recent Posts -->
                    <div class="mad_widget widget_posts">

                        <h5 class="widget_title">Recent blogs</h5>
                        <div class="mad_post_section">
                            @foreach($latest_blogs as $latest_blog)
                                <div class="mad_post_item">

                                    <figure><a href="#"><img src="{{ $latest_blog->image('70x70') }}" alt=""></a></figure>
                                    <div class="post_text_block">
                                        <a href="{{route('singleblog',$latest_blog->id)}}">{{$latest_blog->title}}</a>
                                    </div>

                                </div>
                            @endforeach

                        </div>

                    </div>


                    <!-- Instagram -->
                    <div class="mad_widget widget_instagram">

                        <h5 class="widget_title">Instagram</h5>
                        <ul data-instagram="6" data-instagram-username="mrgicucaus" class="instagram-feed"></ul>

                    </div>

                    <!-- Tags -->
                    <div class="mad_widget widget_tags">

                        <h5 class="widget_title">Twitter</h5>
                        <div>
                            <a href="#" class="tag">Beauty </a>
                            <a href="#" class="tag">Massage</a>
                            <a href="#" class="tag">Hair Cut</a>
                            <a href="#" class="tag">Meditation</a>
                            <a href="#" class="tag">Beauty Training</a>
                        </div>

                    </div>

                    <!-- Instagram -->
                    <div class="mad_widget widget_instagram">

                        <h5 class="widget_title">Facebook</h5>
                        <ul data-instagram="6" data-instagram-username="mrgicucaus" class="instagram-feed"></ul>

                    </div>

                </div>

                <!-- Main content -->
                <div id="main" class="col-md-8">

                    <!-- Image post -->
                    @foreach($blogs as $blog)
                    <div class="mad_blog_post">
                        @if($blog->image)
                        <figure>
                            <img src="{{$blog->image('769x500')}}" alt="{{$blog->title}}">
                            <div class="mad_post_date">

                                <div class="date">{{$blog->created_at->format('d / M / Y')}}</div>

                            </div>
                        </figure>
                        @endif
                        <div class="mad_post_content clearfix">
                            <div class="mad_post_info">

                                <h2><a href="{{ route('singleblog', $blog->id) }}">{{$blog->title}}</a></h2>
                                <div class="mad_post_action">
                                    <a href="#" class="admin" style="text-transform: uppercase;">{{$blog->author_name}}</a>
                                    <a href="#" class="days">{{$blog->time==0 ?'Today':$blog->time.' days ago' }}</a>
                                    <a href="#" class="comment">{{$blog->visitors}} hits</a>
                                </div>
                                {!! Str::limit($blog->details,185) !!}

                            </div>
                        </div>

                    </div>
                    @endforeach



                    {{--<div class="mad_pagination_section">--}}
                        {{--<a href="#" class="current">1</a>--}}
                        {{--<a href="#">2</a>--}}
                        {{--<a href="#">3</a>--}}
                        {{--<a href="#">...</a>--}}
                        {{--<a href="#" class="icon-left-open-1"></a>--}}
                        {{--<a href="#" class="icon-right-open-1"></a>--}}
                    {{--</div>--}}
                    <div class="mad_pagination_section">{{$blogs->links('pagination')}}</div>
                </div>

            </div>
        </div>
    </div>
@endsection