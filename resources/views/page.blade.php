@extends('layouts.app')

@section('contents')

    <div class="mad_section_2">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <div class="align_center">
                        <h2 class="mad_section_offset">{{ $page->title }}</h2>
                    </div>
                    @if($page->image != null)
                    <figure class="mad_item_offset_2">
                        <img src="{{$page->image('1170x410')}}" alt="{{$page->title}}">
                    </figure>
                    @endif
                    <div class="mad_item_offset">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="mad_no_space">{!! $page->details !!}</p>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
@endsection