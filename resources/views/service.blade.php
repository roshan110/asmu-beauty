@extends('layouts.app_secondary')
@section('page_header')
    Services
@endsection

@section('contents')
    <div id="about">
        <div class="mad_section" style="padding: 45px 0 0;">

            <div class="container">

                <div class="mad_section_offset_2">
                    <div class="row">
                        <div class="col-sm-12">

                            <h3 class="align_center">Our Services</h3>
                            <p class="mad_text_style1 mad_section_offset_2 align_center">Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="mad_section inset_none">

            <div class="mad_full_width_grid">
                @php $i=1; @endphp
                @foreach($services as $service)
                @if($i%2 != 0)
                <div class="row pattern_section">
                    <div id="mad_item_second" class="col-md-6">

                        <div class="mad_pattern bg_pattern_red clearfix">
                            <article class="f_right">
                                <h3 style="color:white;">{{$service->title}}</h3>
                                <p>{!! Str::limit($service->details,175) !!}</p>
                                <a href="{{url($service->slug)}}" class="mad_button style2">Know More</a>
                            </article>
                        </div>

                    </div>
                    <div id="mad_item_first" class="col-md-6">
                        <figure class="mad_img_wrap">
                            <img src="{{$service->image ? $service->image('765x460'):asset('images/img_women1.jpg')}}" alt="{{$service->title}}" height="461">
                        </figure>
                    </div>
                </div>
                @else
                <div class="row pattern_section">
                    <div class="col-md-6">
                        <figure>
                            <img src="{{$service->image ? $service->image('765x460'):asset('images/img_women1.jpg')}}" alt="{{$service->title}}" height="461">
                        </figure>
                    </div>
                    <div class="col-md-6">

                        <div class="mad_pattern bg_pattern_dark">
                            <article>
                                <h3 style="color:white;">{{$service->title}}</h3>
                                <p>{!! Str::limit($service->details,175) !!}</p>
                                <a href="{{url($service->slug)}}" class="mad_button style2">Know More</a>
                            </article>
                        </div>

                    </div>
                </div>
                @endif
                @php $i++; @endphp
                @endforeach
            </div>

        </div>



    </div>
@endsection