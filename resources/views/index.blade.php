@extends('layouts.app')
@section('is_home', 1)

@section('contents')
        <!-- Greetings -->
<div id="about">

    <div class="mad_section inset_none v_align_center_blocks">
        <div class="container">

            <div class="row">

                <div class="col-md-6">

                    <figure class="section_image align_right">
                        <img src="{{$page->image ? $page->image('457x524') : asset('images/women_457x524.png')}}" alt="{{$page->title}}">
                    </figure>

                </div>
                <div class="col-md-6">

                    <div class="mad_section_container">
                        <article>
                            <h3 class="mad_title_style1">{{\App\Label::ofValue('home:welcome_top_header')}}</h3>
                            <h2>{{\App\Label::ofValue('home:welcome_sub_header')}}</h2>
                            {!! Str::limit($page->details,208) !!}}
                            <div class="mad_author">
                                <h3 class="mad_title_style2">{{\App\Label::ofValue('home:welcome_founder_name')}}.</h3>
                                <span>{{\App\Label::ofValue('home:welcome_founder_designation')}}</span>
                            </div>
                        </article>

                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="mad_section inset_none">

        <div class="mad_full_width_grid">

            <div class="row pattern_section">
                <div id="mad_item_second" class="col-md-6">

                    <div class="mad_pattern bg_pattern_red clearfix">
                        <article class="f_right">
                            <h2>{{\App\Label::ofValue('home:service_one_head')}}</h2>
                            <p>{{Str::limit(\App\Label::ofValue('home:service_one_body'),175)}}</p>
                            <a href="{{route('service.main')}}" class="mad_button style2">View all Services</a>
                        </article>
                    </div>

                </div>
                <div id="mad_item_first" class="col-md-6">
                    @if($video || $fbvideo)
                    @if($video)
                    <figure class="mad_img_wrap">
                        <iframe width="100%" height="320" src="https://www.youtube.com/embed/{{$video->video_id}}" frameborder="0" allowfullscreen></iframe>
                    </figure>

                        <div class="align_center" style="padding: 45px 0;">
                            <a href="{{route('videod')}}" class="mad_button">View all Videos</a>
                        </div>
                    @endif
                    @if($fbvideo)
                        <figure>
                            <div class="fb-video"
                                 data-href="{{$fbvideo->link}}"
                                 data-height="320"
                                 data-allowfullscreen="true" style="">
                            </div>
                        </figure>
                            <div class="align_center" style="padding: 45px 0;">
                                <a href="{{route('videod')}}" class="mad_button">View all Videos</a>
                            </div>
                    @endif
                    @endif
                </div>
            </div>
            <div class="row pattern_section">
                <div class="col-md-6">

                    <figure>
                        <img src="{{asset('images/img_women2.jpg')}}" alt="" height="455">
                    </figure>

                </div>
                <div class="col-md-6">

                    <div class="mad_pattern bg_pattern_dark">
                        <article>
                            <h2>{{\App\Label::ofValue('home:service_two_head')}}</h2>
                            <p>{{Str::limit(\App\Label::ofValue('home:service_two_body'),175)}}</p>
                            <a href="{{route('service.main')}}" class="mad_button style2">View all Services</a>
                        </article>
                    </div>

                </div>
            </div>

        </div>

    </div>

    <!-- Testimonials -->
    @if(count($reviews)>0)
    <div class="mad_section">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="mad_section_container mad_testimonials">
                        <article>
                            <h3 class="mad_title_style1 align_center" ><u style="border-bottom: 1px solid #ce3d5f;">{{\App\Label::ofValue('home:testimonial_title')}}</u></h3>

                            <!-- Carousel -->
                            <div class="carousel_type_1">

                                <div class="owl-carousel" data-max-items="2" data-autoplay="true">
                                    @foreach($reviews as $review)
                                    <!-- Slide -->
                                    <div class="mad_testimonials_div_box">
                                        <!-- Carousel Item -->
                                        <blockquote>
                                            <p style="text-align: justify;">
                                                {!! $review->details !!}
                                            </p>
                                            <div class="mad_author style2">
                                                <h3 class="mad_title_style2">{{$review->name}}</h3>
                                                @if($review->designation != null)
                                                <span>{{$review->designation}}</span>
                                                @endif
                                            </div>
                                        </blockquote>
                                        <!-- /Carousel Item -->
                                    </div>
                                    <!-- /Slide -->
                                    @endforeach
                                    {{--<!-- Slide -->--}}
                                    {{--<div>--}}
                                        {{--<!-- Carousel Item -->--}}
                                        {{--<blockquote>--}}
                                            {{--<p>--}}
                                                {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam euismod dignissim massa ut venenatis. Vivamus eleifend sem id ligula sollicitudin feugiat.--}}
                                            {{--</p>--}}
                                            {{--<div class="mad_author style2">--}}
                                                {{--<h3 class="mad_title_style2">Alex Jhon</h3>--}}
                                                {{--<span> Founder and CEO</span>--}}
                                            {{--</div>--}}
                                        {{--</blockquote>--}}
                                        {{--<!-- /Carousel Item -->--}}
                                    {{--</div>--}}
                                    {{--<!-- /Slide -->--}}
                                    {{--<!-- Slide -->--}}
                                    {{--<div>--}}
                                        {{--<!-- Carousel Item -->--}}
                                        {{--<blockquote>--}}
                                            {{--<p>--}}
                                                {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam euismod dignissim massa ut venenatis. Vivamus eleifend sem id ligula sollicitudin feugiat.--}}
                                            {{--</p>--}}
                                            {{--<div class="mad_author style2">--}}
                                                {{--<h3 class="mad_title_style2">Alex Jhon</h3>--}}
                                                {{--<span> Founder and CEO</span>--}}
                                            {{--</div>--}}
                                        {{--</blockquote>--}}
                                        {{--<!-- /Carousel Item -->--}}
                                    {{--</div>--}}
                                    {{--<!-- /Slide -->--}}

                                </div>
                            </div>

                        </article>

                    </div>

                </div>
                {{--<div class="col-md-6">--}}

                    {{--<figure class="section_image align_right">--}}
                        {{--<img src="{{asset('images/565x520_img.jpg')}}" alt="">--}}
                    {{--</figure>--}}

                {{--</div>--}}

            </div>

        </div>

    </div>
    @endif

</div>

<!-- Services -->
@if(count($services) >0)
<div id="services">

    <div class="paralax_image_bg2" style="background: url('{{asset('images/bg_image_1920x1419.jpg')}}')">

        <svg class="separator_type_5_path top" preserveAspectRatio="none" viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0 L30 100 L100 0 L100 100 L0 100 Z" fill="#fff" stroke="#fff" stroke-width="-1"></path>
            <path d="M0 0 L0 0 L50 100 L100 0 L100 0 Z" fill="rgba(255,255,255,0.01)"></path>
        </svg>

        <div class="container">

            <div class="row">

                <div class="col-sm-12">

                    <div class="mad_section_container">
                        <h3 class="mad_title_style1">{{\App\Label::ofValue('home:service_parallax_head')}}</h3>
                        <h2 class="mad_separator">{{\App\Label::ofValue('home:service_parallax_subhead')}}</h2>
                    </div>

                    <!-- Carousel -->
                    <div class="carousel_type_2">

                        <div class="owl-carousel" data-max-items="3">
                                @foreach($services as $service)
                            <!-- Slide -->
                            <div>
                                <!-- Carousel Item -->
                                <div class="mad_item_hover">
                                    <figure>
                                        <img src="{{$service->image ? $service->image('360x205') :asset('images/370x370_img1.jpg')}}" alt="">
                                    </figure>
                                    <div class="mad_item_desc with_bg_img">
                                        <h5>{{$service->title}}</h5>
                                    </div>
                                    <a href="{{url($service->slug)}}" class="item_overlay">
                                        <div class="text_holder">
                                            <p>{!! Str::limit($service->details,50) !!}</p>
                                        </div>
                                    </a>
                                </div>
                                <!-- /Carousel Item -->
                            </div>
                            <!-- /Slide -->
                            @endforeach

                        </div>

                        <div class="align_center">
                            <a href="{{route('service.main')}}" class="mad_button style2">View all Services</a>
                        </div>

                    </div>

                </div>

            </div>

        </div>

        <svg class="separator_type_5_path" preserveAspectRatio="none" viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0 L70 100 L100 0 L100 100 L0 100 Z" fill="#fff" stroke="#fff" stroke-width="-1"></path>
            <path d="M0 -1 L0 0 L50 100 L100 0 L100 -1 Z" fill="rgba(255,255,255,0.01)"></path>
        </svg>

    </div>

    <!-- Services3 -->
    <div id="services3" class="mad_section inset_none v_align_center_blocks">

        <div class="container">

            <div class="row">

                <div class="col-md-6">

                    <figure class="section_image align_center">
                        <img src="{{asset('images/500x385_img.jpg')}}" alt="">
                    </figure>

                </div>
                <div class="col-md-6">

                    <div class="mad_section_container">
                        <article>
                            <h3 class="mad_title_style1">{{\App\Label::ofValue('home:service_pricing_head')}}</h3>
                            <h2>{{\App\Label::ofValue('home:service_pricing_subhead')}}</h2>
                            <p>{{\App\Label::ofValue('home:service_pricing_body')}}</p>
                        </article>

                    </div>
                </div>

            </div>

        </div>

        <!-- Tabs section -->
        <div class="mad_section">

            <div class="container">

                <div class="row">

                    <div class="col-sm-12">

                        <div class="tabs tabs-section vertical clearfix">
                            <!--tabs navigation-->
                            <ul class="tabs_nav clearfix">
                                @php $i=1; @endphp
                                @foreach($services as $service)
                                <li class=""><a href="#tab_{{$i}}" class="{{ config('service_icons.'. $i) }}">{{$service->title}}</a></li>
                                @php $i++; @endphp
                                @endforeach

                            </ul>
                            <!--tabs content-->

                            <div class="tabs_content">
                                @php $i=1; @endphp
                                @foreach($services as $service)
                                <div id="tab_{{$i}}">

                                    <div class="tabs tabs-section style2">

                                        <!--tabs navigation-->
                                        <div class="clearfix tabs_conrainer">
                                            <ul class="tabs_nav clearfix">
                                                <li class=""><a href="#tab{{$i}}">{{$service->title}}</a></li>

                                            </ul>
                                        </div>

                                        <!--tabs content-->
                                        <div class="tabs_content" style="min-height: 370px;">
                                            <div id="tab{{$i}}">

                                                <div class="service_tab_contents">
                                                    @if($service->image)
                                                        <figure>
                                                        <img src="{{$service->image('200x220')}}" alt="{{$service->title}}" class="" style="float: right; padding-left: 1rem;">
                                                        </figure>

                                                   <div class="page_contents_blocked" style="text-align: justify;">{!! Str::limit($service->details,430) !!}</div>
                                                    @else
                                                        <div class="page_contents_blocked" style="text-align: justify;">{!! Str::limit($service->details,580) !!}</div>
                                                    @endif

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                                    @php $i++; @endphp
                                @endforeach

                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
@endif

<!-- Gallery -->
@if(count($photos)>0)
<div id="gallery">

    <div class="mad_section with_bg_1" style="padding: 90px 0 0;">

        <div class="container relative">

            <div class="row">

                <div class="col-sm-12">

                    <div class="mad_section_container">
                        <h3 class="mad_title_style1">{{\App\Label::ofValue('home:gallery_head')}}</h3>
                        <h2 class="mad_separator style2">{{\App\Label::ofValue('home:gallery_subhead')}}</h2>

                        <div class="owl_custom_buttons">

                            <button class="mad_owl_prev"></button>

                            <button class="mad_owl_next"></button>

                        </div>
                    </div>

                </div>

            </div>

        </div>

        <!-- Carousel -->
        <div class="carousel_type_2 full_width_gallery">

            <div class="owl-carousel gallery_carousel" data-max-items="4">
                @foreach($photos as $photo)
                <!-- Slide -->
                <div>
                    <!-- Carousel Item -->
                    <div class="mad_item_hover">
                        <figure>
                            <img src="{{$photo->image ? $photo->image('384x295') : asset('images/480x370_img1.jpg')}}" alt="" height="295">
                        </figure>
                        <div class="item_overlay">
                            <div class="text_holder">
                                <a href="{{asset('uploads/photo/'.$photo->image)}}" class="mad_icon_plus gallery" rel="category"></a>
                                <a href="#" class="mad_icon_link"></a>
                            </div>
                        </div>
                    </div>

                    {{--<div class="mad_item_hover">--}}
                        {{--<figure>--}}
                            {{--<img src="{{asset('images/480x370_img5.jpg')}}" alt="">--}}
                        {{--</figure>--}}
                        {{--<div class="item_overlay">--}}
                            {{--<div class="text_holder">--}}
                                {{--<a href="{{asset('images/480x370_img5.jpg')}}" class="mad_icon_plus gallery" rel="category"></a>--}}
                                {{--<a href="#" class="mad_icon_link"></a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <!-- /Carousel Item -->
                </div>
                <!-- /Slide -->
                @endforeach


            </div>

        </div>
        <div class="align_center" style="padding:40px 0;">
            <a href="{{route('photod')}}" class="mad_button">View all Photos</a>
        </div>
    </div>

</div>
@endif

<!-- Team -->
@if(count($partners)>0)
<div id="team">

    <div class="mad_section">

        <div class="container">

            <div class="row">

                <div class="col-sm-12">

                    <div class="align_center mad_item_offset">

                        <div class="mad_section_container">
                            <h3 class="mad_title_style1">{{\App\Label::ofValue('home:partner_head')}}</h3>
                        </div>

                    </div>
                    <!-- Carousel -->
                    <div class="carousel_type_1 style2">

                        <div class="owl-carousel" data-max-items="4" data-autoplay="true">
                            @foreach($partners as $partner)
                            <!-- Slide -->
                            <div>
                                <!-- Carousel Item -->
                                <a href="{{$partner->link ? $partner->link : '#'}}" class="mad_item_hover style2" style="height: 185px; padding: 0;">

                                    <figure>
                                        <img src="{{$partner->image('294x120')}}" alt="{{$partner->title}}" height="120">
                                    </figure>
                                    <div class="mad_item_desc with_bg_img">
                                        <div class="mad_author style2">
                                            <h3 class="mad_title_style2" style="font-size: 22px;font-family: 'Raleway', sans-serif;">{{$partner->title}}</h3>
                                            {{--<span>Hair Stylist</span>--}}
                                        </div>
                                    </div>

                                </a>
                                <!-- /Carousel Item -->
                            </div>
                            <!-- /Slide -->
                        @endforeach

                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
@endif

<!-- Offers & News -->
<div id="news">

    <div class="mad_section paralax_image_bg4" style="background:url('{{asset('images/bg_image_1920x1278.jpg')}}')">

        <div class="container">

            <div class="row">
                <div class="col-lg-5"></div>
                <div class="col-lg-7">

                    <div class="mad_section_container mad_item_offset">
                        <h3 class="mad_title_style1">{{\App\Label::ofValue('home:news_head')}}</h3>
                        <h2>Latest News & Offers</h2>
                    </div>

                </div>
            </div>

        </div>
        <div class="full_width_bg_pattern bg_pattern_red">

            <div class="container">

                <div class="row">
                    <div class="col-md-5">

                        <figure><img src="{{asset('images/magazine_620x480.png')}}" alt=""></figure>

                    </div>
                    <div class="col-md-7">

                        <div class="mad_section_container">
                            <h3 class="mad_title_style1">See the Latest News Of &quot;Asmu Beauty Parlor&quot;</h3>
                            <a href="{{route('blogs.main')}}" class="mad_button style2">Read All</a>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <svg class="separator_type_5_path" preserveAspectRatio="none" viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0 L70 100 L100 0 L100 100 L0 100 Z" fill="#fff" stroke="#fff" stroke-width="-1"></path>
            <path d="M0 -1 L0 0 L50 100 L100 0 L100 -1 Z" fill="rgba(255,255,255,0.01)"></path>
        </svg>

    </div>

</div>

<!-- Blog Comment from W3 Web Technology--><!--
      <div id="blog">

        <div class="mad_section paralax_image_bg1">

          <div class="container">

            <div class="row">

              <div class="col-sm-12">

                <div class="mad_section_container">
                  <h3 class="mad_title_style1">Blog</h3>
                  <h2 class="mad_separator style2">Latest News</h2>
                </div>

                <!-- Carousel -->
<!-- <div class="carousel_type_2">

  <div class="owl-carousel style2" data-max-items="2">

    <!-- Slide -->
<!--  <div>
   <!-- Carousel Item -->
<!-- div class="mad_blog_post">
  <figure>
    <img src="images/570x370_img1.jpg" alt="">
    <div class="mad_post_date">

      <div class="date">18 / JAN / 2016</div>

    </div>
  </figure>
  <div class="mad_post_content clearfix">
    <div class="mad_post_info">

      <h2><a href="#">Slider Blog Post Title Here</a></h2>
      <div class="mad_post_action">
        <a href="#" class="admin">Admin</a>
        <a href="#" class="days">6 days ago</a>
        <a href="#" class="comment">10 comments</a>
      </div>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>

    </div> -->
<!-- </div> -->

<!-- </div> -->
<!-- /Carousel Item -->
<!--  </div>
<!-- /Slide -->
<!-- Slide -->
<!--  <div> -->
<!-- Carousel Item -->
<!-- <div class="mad_blog_post">
  <figure>
    <img src="images/570x370_img2.jpg" alt="">
    <div class="mad_post_date">

      <div class="date">18 / JAN / 2016</div>

    </div>
  </figure>
  <div class="mad_post_content clearfix">
    <div class="mad_post_info">

      <h2><a href="#">Slider Blog Post Title Here</a></h2>
      <div class="mad_post_action">
        <a href="#" class="admin">Admin</a>
        <a href="#" class="days">6 days ago</a>
        <a href="#" class="comment">10 comments</a>
      </div>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>

    </div>
  </div>

</div> -->
<!-- /Carousel Item -->
<!-- </div> -->
<!-- /Slide -->
<!-- Slide -->
<!-- <div> -->
<!-- Carousel Item -->
<!--  <div class="mad_blog_post">
   <figure>
     <img src="images/570x370_img1.jpg" alt="">
     <div class="mad_post_date">

       <div class="date">18 / JAN / 2016</div>

     </div>
   </figure>
   <div class="mad_post_content clearfix">
     <div class="mad_post_info">

       <h2><a href="#">Slider Blog Post Title Here</a></h2>
       <div class="mad_post_action">
         <a href="#" class="admin">Admin</a>
         <a href="#" class="days">6 days ago</a>
         <a href="#" class="comment">10 comments</a>
       </div>
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>

     </div>
   </div>

 </div>
  /Carousel Item
</div>
/Slide

</div>

</div>

</div>

</div>

</div>

</div>

</div> -->

<!-- Contact -->
<!--  <div id="contact">

   <div class="mad_section">

     <div class="container">

       <div class="row">

         <div class="col-md-6">

           <div class="mad_section_container">
             <article>
               <h3 class="mad_title_style1">Get in Touch</h3>
               <h2>Make Inquiry</h2>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam euismod dignissim massa ut venenatis. Vivamus eleifend sem id ligula sollicitudin feugiat. Sed viverra erat finibus, elementum velit sit amet.</p>
             </article>
           </div>

         </div>
         <div class="col-md-6">

           <form id="contact_form" class="contact_form">
             <ul>
               <li class="row">

                 <div class="col-sm-6">
                   <input type="text" name="cf_name" placeholder="First Name">
                 </div>
                 <div class="col-sm-6">
                   <input type="text" name="cf_name" placeholder="Last Name">
                 </div>

               </li>
               <li>
                 <input type="url" name="cf_email" placeholder="Emai">
               </li>
               <li>
                 <textarea name="cf_message" placeholder="Message..."></textarea>
               </li>
             </ul>
             <button class="mad_button">Make An Appointment</button>
           </form>

         </div>

       </div>

     </div>

   </div> -->



@endsection