@extends('layouts.app')

@section('contents')
@if(\App\Setting::ofValue('google_map') !='-')
    <div class="relative">

        <div class="map_container">
            {!! \App\Setting::ofValue('google_map') !!}
        </div>

    </div>
@endif
    <div class="mad_section_2">

        <div class="container">

            <!-- Contact info -->
            <div class="row">
                <div class="col-sm-6">
                    <h4>{{\App\Label::ofValue('contact:address_head')}}</h4>
                    <p style="line-height: 2.8; text-align: justify;">{!! nl2br(\App\Label::ofValue('contact:address_body')) !!}</p>
                    <div class="c_info_list">

                        <div class="row">
                            <div class="col-sm-6">

                                <div class="contact_item clearfix">
                                    <i class="icon-direction"></i>
                                    <p>{!! nl2br(Label::ofValue('global:address'))  !!}</p>
                                </div>

                                <div class="contact_item clearfix">
                                    <i class="icon-phone-1"></i>
                                    <p>Phone: {!! nl2br(Label::ofValue('global:mobile'))  !!}</p>
                                </div>

                                <div class="contact_item clearfix">
                                    <i class="icon-email"></i>
                                    <p>Email: <a class="contact_e" href="mailto:{!! nl2br(Label::ofValue('global:email'))  !!}">{!! nl2br(Label::ofValue('global:email'))  !!}</a></p>
                                </div>

                            </div>
                            <div class="col-sm-6">

                                <div class="contact_item clearfix">
                                    <i class="icon-clock"></i>
                                    <p>{!! nl2br(\App\Label::ofValue('home:footer_opening_body')) !!}</p>
                                </div>

                                <div class="contact_item clearfix">
                                    <i class="icon-fax"></i>
                                    <p>Fax: {!! nl2br(\App\Label::ofValue('global:fax')) !!}</p>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">

                    <h4>Contact Form</h4>
                    <form id="contact_form" class="contact_form" action="{{ route('contactmail') }}" method="POST" role="form">
                        {!! csrf_field() !!}
                        <ul>
                            <li>
                                <input type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" name="name" id="name">
                                @if ($errors->has('name'))
                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </li>
                            <li>
                                <input type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" id="email">
                                @if ($errors->has('email'))
                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </li>
                            <li>
                                <textarea class="{{ $errors->has('message') ? ' is-invalid' : '' }}"  name="message" placeholder="Message..." id="message"></textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </li>
                            <li>
                                <div class="form-group row">
                                    <div class="col-md-2 col-sm-6">
                                        <img src="{{url('captcha/image/jpg')}}" alt="Captcha">
                                    </div>

                                    <div class="col-md-10 col-sm-6">
                                        <input type="text" class="form-control text {{ $errors->has('captcha') ? ' is-invalid' : '' }}" id="captcha" name="captcha" placeholder="Enter Captcha" required="">
                                        @if ($errors->has('captcha'))
                                            <span class="help-block alert_box error"><strong>{{ $errors->first('captcha') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <button type="submit" class="mad_button small_button">Send</button>
                    </form>

                </div>
            </div>

        </div>
    </div>
@endsection