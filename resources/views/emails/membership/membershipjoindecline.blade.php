@component('mail::message')
# Dear {{ $membershipjoin->name }},

Your Membership request for **{{$membershipjoin->membership->title}}** has been decline for some reasons.<br>

<br>
For further information, Please feel free to call us.

{{ \App\Label::ofValue('global:mobile') }}
<br>
<br>
Sincerely,

Support Team <br>
{{ config('app.name') }}<br>
[{{ config('app.url') }}]({{ config('app.url') }})
@endcomponent
