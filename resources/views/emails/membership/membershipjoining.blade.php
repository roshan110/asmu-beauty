@component('mail::message')
#Membership Request

Membership request made by **{{ $request->fname }} {{$request->lname}}** and details are following:

**Name:** {{$request->fname}} {{$request->lname}}<br>
**Email:** {{$request->email}}<br>
**Phone:** {{$request->phone}}<br>
**Address:** {{$request->address}}<br>

**Membership Info:**<br>
**Type:** {{$membership->title}}<br>
**Price:** &pound;{{$membership->price}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
