@component('mail::message')
## Dear seller

A user placed an appointment book

{{--Follow the next link to view the appointment book details [#{{$appointment->id}}].--}}

{{--@component('mail::button', ['url' => route('appointments.show', [$appointment->id]), 'color' => 'green'])--}}
{{--View Appointment Details--}}
{{--@endcomponent--}}

## Appointment details are listed below.

**Appointment Id:** #{{$appointment->id}}<br>
**Payment Type:** {{$appointment->payment}} ({!!$appointment->paid? '<span style="color:green">Paid</span>' : '<span style="color:red">Unpaid</span>'!!})<br>

**Name:** {{$appointment->name}}<br>
**Phone:** {{$appointment->phone}}<br>
**Email:** {{$appointment->email}}<br>

<table width="100%" bgcolor="#ddd" cellspacing="1">
	<tr bgcolor="#ddd">
		<th align="right" style="padding: 10px">Employee</th>
		<th align="right" style="padding: 10px;">Date</th>
		<th align="right" style="padding: 10px;">Time</th>
		<th align="left" style="padding: 10px">Item</th>
		<th align="right" style="padding: 10px">Price</th>
	</tr>
	@foreach ($appointment->items as $item)
	<tr bgcolor="white">
		<td style="padding: 5px 10px" align="right">{{$item->employee->name}}</td>
		<td style="padding: 5px 10px;" align="right">{{$item->appointment_date}}</td>
		<td style="padding: 5px 10px;" align="right">{{$item->appointment_time}}</td>
		<td style="padding: 5px 10px">{{$item->item}}</td>
		<td style="padding: 5px 10px" align="right">&pound;{{number_format($item->price,2)}}</td>
	</tr>
	@endforeach
	<tr bgcolor="white">
		<td style="padding: 5px 10px" colspan="4" align="right">Total</td>
		<td style="padding: 5px 10px" align="right">&pound;{{$appointment->total}}</td>
	</tr>
</table>
<br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
