@component('mail::message')
## Dear {{$appointment->name}}

We thank you for choosing us.

## Your appointment booking details are listed below

**Appointment Id:** #{{$appointment->id}}<br>
**Payment Type:** {{$appointment->payment}} <br>

<table width="100%" bgcolor="#ddd" cellspacing="1">
	<tr bgcolor="#ddd">
		<th align="right" style="padding: 10px">Employee</th>
		<th align="right" style="padding: 10px;">Date</th>
		<th align="right" style="padding: 10px;">Time</th>
		<th align="left" style="padding: 10px">Service</th>
		<th align="right" style="padding: 10px">Price</th>
	</tr>
	@foreach ($appointment->items as $item)
	<tr bgcolor="white">
		<td style="padding: 5px 10px" align="right">{{$item->employee->name}}</td>
		<td style="padding: 5px 10px;" align="right">{{$item->appointment_date}}</td>
		<td style="padding: 5px 10px;" align="right">{{$item->appointment_time}}</td>
		<td style="padding: 5px 10px">{{$item->item}}</td>
		<td style="padding: 5px 10px" align="right">&pound;{{number_format($item->price,2)}}</td>
	</tr>
	@endforeach
	<tr bgcolor="white">
		<td style="padding: 5px 10px" colspan="4" align="right">Total</td>
		<td style="padding: 5px 10px" align="right">&pound;{{$appointment->total}}</td>
	</tr>

</table>
<br>

In case of emergency, Please feel free to call us.

{{ \App\Label::ofValue('global:mobile') }}
<br>
<br>
Sincerely,

Support Team <br>
{{ config('app.name') }}<br>
[{{ config('app.url') }}]({{ config('app.url') }})
@endcomponent