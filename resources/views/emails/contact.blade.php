@component('mail::message')

#Contact Mail

Contact Mail received from **{{ $data->name }}** and details are following:<br>

**Name:** {{$data->name}}<br>
**Email:** {{$data->email}}<br>

**Message:**
{{$data->message}}



Thanks,<br>
{{ config('app.name') }}
@endcomponent
