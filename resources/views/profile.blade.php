@extends('layouts.app_secondary')
@section('page_header')
        Profile
@endsection

@section('css')
    <style>
    /*
    Max width before this PARTICULAR table gets nasty
    This query will take effect for any screen smaller than 760px
    and also iPads specifically.
    */
    /*(min-device-width: 768px) and (max-device-width: 1024px)*/
    @media
    only screen and (max-width: 768px)
      {

    /* Force table to not be like tables anymore */
    table#appointmenthistory_table ,#appointmenthistory_table thead,#appointmenthistory_table tbody,#appointmenthistory_table th,#appointmenthistory_table td,#appointmenthistory_table tr {
    display: block;
    }

    /* Hide table headers (but not display: none;, for accessibility) */
    #appointmenthistory_table thead tr {
    position: absolute;
    top: -9999px;
    left: -9999px;
    }

   #appointmenthistory_table tr { border: 1px solid #6f6f6f; }

    #appointmenthistory_table td {
    /* Behave  like a "row" */
    border: none;
    border-bottom: 1px solid #6f6f6f;
    position: relative;
    padding-left: 50%;
    }

    #appointmenthistory_table td:before {
    /* Now like a table header */
    position: absolute;
    /* Top/left values mimic padding */
    top: 6px;
    left: 6px;
    width: 45%;
    padding-right: 10px;
    white-space: nowrap;
    }

    /*
    Label the data
    */
    #appointmenthistory_table td:nth-of-type(1):before { content: "Appoint.ID"; }
    #appointmenthistory_table td:nth-of-type(2):before { content: "Service"; }
    #appointmenthistory_table td:nth-of-type(3):before { content: "Price"; }
    #appointmenthistory_table td:nth-of-type(4):before { content: "Employee"; }
    #appointmenthistory_table td:nth-of-type(5):before { content: "Date"; }
    #appointmenthistory_table td:nth-of-type(6):before { content: "Payment"; }
        #appointmenthistory_table td:nth-of-type(7):before { content: "Total"; }
    }
    </style>
@endsection

@section('contents')
    <div class="mad_section_2">

        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <div class="align_center mad_item_offset_2">
                        <h3 class="mad_title_style1">My Account</h3>
                    </div>

                </div>
            </div>
            @if (!$user->isAdmin)
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="align_center mad_item_offset_2">
                                <h4 class="mad_title_style1">Password Change</h4>
                                @if (\Session::has('password_match'))
                                    <div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{\Session::get('password_match')}}</strong>
                                    </div>

                                @endif

                                @if (\Session::has('success'))
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>{{\Session::get('success')}}</strong>
                                    </div>
                                @endif
                            </div>
                            <form id="contact_form" class="contact_form" action="{{ route('changepassword') }}" method="POST" role="form">
                                {!! csrf_field() !!}
                                <ul>
                                    <li>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="password" class="form-control {{ $errors->has('old_password') ? ' has-error' : '' }}" placeholder="Old Password" name="old_password" id="">
                                                @if ($errors->has('old_password'))
                                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                        </span>
                                                @endif
                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" placeholder="New Password" name="password" id="">
                                                @if ($errors->has('password'))
                                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="password" class="form-control {{ $errors->has('password_confirmation') ? ' has-error' : '' }}" placeholder="Re-enter Password" name="password_confirmation" id="">
                                                @if ($errors->has('password_confirmation'))
                                                    <span class="help-block alert_box error">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                                <div class="membership_d_joinbtn">
                                    <button type="submit" class="mad_button small_button" style="background: #272627;">Change</button>
                                </div>

                            </form>
                        </div>
                        <div class="col-sm-6 membership_info_blocked" >
                            <div class="align_center mad_item_offset_2">
                                <h4 class="mad_title_style1">Profile Info</h4>
                            </div>
                            <div class="mad_item_offset">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="mad_no_space page page_contents_blocked" style="font-size: 26px;"><span style="color: #ce3d5f;">Name: </span>{{$user->name}}</div>
                                        <br>
                                        <div class="mad_no_space page page_contents_blocked" style="font-size: 26px;"><span style="color:#ce3d5f;">Email: </span>{{$user->email}}</div>
                                        @if($lastappointment)<br>
                                        <div class="mad_no_space page page_contents_blocked" style="font-size: 26px;"><span style="color: #ce3d5f;">Phone: </span>{{$lastappointment->phone}}</div>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12" style="">
                    <div class="align_center mad_item_offset_2" style="margin: 25px 0;">
                        <h4 class="mad_title_style1">Appointment History</h4>
                    </div>
                    @if(count($appointmenthistory)>0)
                    <div class="table-responsive">
                    <table class="table table-bordered " id="appointmenthistory_table">
                        <thead>
                        <tr>
                            <th>Appoint.ID</th>
                            <th>Service</th>
                            <th>Price</th>
                            <th>Employee</th>
                            <th>Date</th>
                            <th>Payment</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody class="profile_history_table ">
                        @foreach($appointmenthistory as $appointment)
                            @php
                            $row = $appointment->items()->count();
                            @endphp
                            @foreach ($appointment->items as $key=>$item)
                                @if ($key == 0)
                            <tr>

                            <td rowspan="{{$row}}">#{{$appointment->id}}</td>
                            <td>{{$item->item}}</td>
                            <td>&pound;{{$item->price}}</td>
                                <td>{{$item->employee['name']}}</td>
                                <td>{{$item->appointment_date}} {{$item->appointment_time}}</td>
                                <td rowspan="{{$row}}">{{$appointment->payment_status}}</td>
                                <td rowspan="{{$row}}">&pound;{{$appointment->total}}</td>
                            </tr>
                                @else
                                    <tr style="">
                                        <td>{{$item->item}}</td>
                                        <td>&pound;{{$item->price}}</td>
                                        <td>{{$item->employee['name']}}</td>
                                        <td>{{$item->appointment_date}} {{$item->appointment_time}}</td>
                                    </tr>
                                @endif
                            @endforeach
                       @endforeach
                        </tbody>
                        <tbody class="profile_history_table_two">
                        @foreach($appointmenthistory as $appointment)
                            @php
                            $row = $appointment->items()->count();
                            @endphp
                            @foreach ($appointment->items as $key=>$item)
                                @if ($key == 0)
                                    <tr>

                                        <td>#{{$appointment->id}}</td>
                                        <td>{{$item->item}}</td>
                                        <td>&pound;{{$item->price}}</td>
                                        <td>{{$item->employee['name']}}</td>
                                        <td>{{$item->appointment_date}} {{$item->appointment_time}}</td>
                                        <td >{{$appointment->payment_status}}</td>
                                        <td >&pound;{{$appointment->total}}</td>
                                    </tr>
                                @else
                                    <tr style="">
                                        <td>#{{$appointment->id}}</td>
                                        <td>{{$item->item}}</td>
                                        <td>&pound;{{$item->price}}</td>
                                        <td>{{$item->employee['name']}}</td>
                                        <td>{{$item->appointment_date}} {{$item->appointment_time}}</td>
                                        <td >{{$appointment->payment_status}}</td>
                                        <td >&pound;{{$appointment->total}}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                     @else
                        <div class="align_center">
                        <h5 class="">No records found.</h5>
                        </div>
                    @endif
                </div>
            </div>
            @else
                <div class="align_center mad_item_offset_2">
                    <h4 class="">Admin :)</h4>
                </div>
            @endif
        </div>

    </div>
@endsection


@section('script')
    <script src="{{asset('frontend/plugins/bootstrap.js')}}"></script>
@endsection
