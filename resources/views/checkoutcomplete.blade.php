@extends('layouts.app_secondary')
@section('page_header')
    Checkout Complete
@endsection
@section('contents')
<div class="mad_section_2">

    <div class="container">

        <div class="row">
            <div class="col-sm-12">

                <div class="align_center mad_item_offset_2">
                    <h3 class="mad_title_style1">Checkout Complete</h3>
                    <h5>Thank you for choosing us.</h5>
                    <p class="mad_no_space">Please check your email for appointment booking details.</p>
                </div>

            </div>
        </div>


    </div>

</div>
@endsection