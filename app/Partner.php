<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    public static $_thumbdir = [
        '700x525',
        '400x300',
        '100x80',
        '300x300',
    ];

    protected $maxfileSize = 2*1024*1024;

    protected $attributes = [
        'status' => 1,
    ];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function image($size = null)
    {

        $_image = false;
        if($size){

            $original = public_path('uploads/partners/'. $this->image);
            if($this->image && file_exists($original)){
                $_image = asset('uploads/partners/' . $size . '/' . $this->image);
                $_image_path = public_path('uploads/partners/' . $size . '/' . $this->image);
                if(!file_exists($_image_path)){
                    $_image = new  _image('partners', $size, $this->image);
                }
            }
        }else{
            $original = public_path('uploads/partners/'. $this->image);
            if($this->image && file_exists($original)){
                $_image = asset('uploads/partners/'. $this->image);
            }
        }
        return $_image;
    }

    protected function imageCompress($image)
    {
        if($image){
            $imagesize = getimagesize($image);
            $filesize = filesize($image);
            if($filesize > $this->maxFileSize){
                $newimage = $this->resize_image($image);
                if($imagesize['mime'] == "image/gif"){
                    imagegif($newimage, $image);
                }elseif($imagesize['mime'] == "image/jpeg"){
                    imagejpeg($newimage, $image);
                }elseif($imagesize['mime'] == "image/bmp"){
                    imagebmp($newimage, $image);
                }elseif($imagesize['mime'] == "image/png"){
                    imagepng($newimage, $image);
                }
            }
        }
    }

    function resize_image($file, $w=1000, $h=1000) {
        $imagesize = getimagesize($file);
        if($imagesize){

            $width = $imagesize[0];
            $height = $imagesize[1];
            $r = $width / $height;
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
            if($imagesize['mime'] == "image/gif"){
                $src = imagecreatefromgif($file);
            }elseif($imagesize['mime'] == "image/jpeg"){
                $src = imagecreatefromjpeg($file);
            }elseif($imagesize['mime'] == "image/bmp"){
                $src = imagecreatefrombmp($file);
            }elseif($imagesize['mime'] == "image/png"){
                $src = imagecreatefrompng($file);
            }elseif($imagesize['mime'] == "image/webp"){
                $src = \imagecreatefromwebp($file);
            }
            $dst = imagecreatetruecolor($newwidth, $newheight);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
            return $dst;

        }else{
            return false;

        }

    }
}
