<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Appointment extends Model
{
    protected $attributes = [
        'status' => 0,
        'paid' => 0,
    ];

    protected $appointment_status = [
        0 => 'Pending',
        1 => 'Process',
//        2 => 'Delivery',
        2 => 'Finish',
    ];
    protected $payment_status = [
        0 => 'Unpaid',
        1 => 'Paid',
    ];

    public function items()
    {
        return $this->hasMany('\App\Appointmentitem');
    }

    public function getTotalAttribute()
    {
        return number_format($this->items()->sum('price'), 2);
    }
    public function scopeToday($query)
    {
        return $query->where('created_at', '>=', date('Y-m-d'));
    }

    public function scopePending($query)
    {
        return $query->where('status', 0);
    }

    public function scopeNotPending($query)
    {
        return $query->where('status', '<>', 0);
    }
    public function scopePaid($query)
    {
        return $query->where('paid', 1);
    }

    public function scopeNotPaid($query)
    {
        return $query->where('paid', '<>', 1);
    }

    public function getAppointmentStatusAttribute()
    {
        return $this->appointment_status[$this->status];
    }

    public function getPaymentStatusAttribute()
    {
        return $this->payment_status[$this->paid];
    }



    public static function closedDay()
    {

            $closed_day = \App\Setting::ofValue('parlor_closed_day');

        $close = date('l', strtotime($closed_day));
        $days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ];
        $closed = '';
        if($closed_day == '-')
        {
            $closed='A';
        }
        else {
            for ($i = 0; $i < 7; $i++) {
                if ($close == $days[$i]) {
                    $closed = $i;
                }
            }
        }

//
        return $closed;
    }

    public static function employmentHoliday($employee)
    {
        $holiday[] = '';
        $employee_holidayed[]='';
        $employeess=\App\Employee::find($employee);
//        dd($employees);

            $employees_holiday = $employeess['holiday'];
        Log::info($employees_holiday);
//        dd($employees_holiday);
        $x=0;
        if($employees_holiday)
        {
            $day = [
                'Sunday',
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday'
            ];
            $employee_holidayed = explode(',',$employees_holiday);

            foreach($employee_holidayed as $employee_h)
            {
                for ($i = 0; $i < 7; $i++) {
                    $employees_holidays = date('l', strtotime($employee_h));
                    if ($employees_holidays == $day[$i]) {
                        $holiday[$x] = $i;
                    }
                }
                $x++;
                Log::info('loop');
                Log::info($holiday);
            }
        }
        else{
            $holiday[$x]='B';
        }

        return $holiday;
    }
}
