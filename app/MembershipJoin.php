<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembershipJoin extends Model
{
    protected $attributes = [
        'status' => 0,
        'paid' => 0,
    ];
    protected $fillable = [
        'membership_id', 'name', 'email', 'phone', 'address','service','postal',
    ];
    public function membership()
    {
        return $this->belongsTo('App\Membership','membership_id');
    }
    public function getNamesAttribute()
    {
        return ($this->name);
    }
}
