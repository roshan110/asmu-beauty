<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Setting;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $emails;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $emails = Setting::ofValue('emails');
        $emails = array_filter(array_map('trim',explode(';', $emails)));
        $this->emails = $emails;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.contact')->from($this->data->email, $this->data->name)
        ->to($this->emails) ->subject('New Message' . config('app.name'));
    }
}
