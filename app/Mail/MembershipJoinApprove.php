<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MembershipJoinApprove extends Mailable
{
    use Queueable, SerializesModels;
    public $membershipjoin;
    public $id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($membershipjoin,$id)
    {
        $this->membershipjoin = $membershipjoin;
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->membershipjoin->email, $this->membershipjoin, $this->id)
            ->subject('Membership Request Approved :: ' .config('app.name'))->markdown('emails.membership.membershipjoinapprove');
    }
}
