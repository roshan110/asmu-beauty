<?php

namespace App\Mail;

use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MembershipJoining extends Mailable
{
    use Queueable, SerializesModels;
    public $request;
    public $emails;
    public $membership;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $emails = Setting::ofValue('emails');
        $emails = array_filter(array_map('trim',explode(';', $emails)));
        $this->emails = $emails;
        $this->request = $request;
        $this->membership = \App\Membership::where('id', $this->request['membership_id'])->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->request->email, $this->request->name)
            ->to($this->emails)->subject('New Membership Request ' . config('app.name'))->markdown('emails.membership.membershipjoining');
    }
}
