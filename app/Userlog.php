<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Userlog extends Model
{
    protected $casts = [
        'login_time' => 'datetime',

    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }



}
