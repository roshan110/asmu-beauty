<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentTime extends Model
{
    protected static $ord_min = ['00','15','30','45'];
    protected static $min_interval = 15;

    public static function hour($employee,$date)
    {

//        dd($date);
        $time_option = [];

        $employee_info=Employee::find($employee);
//        return $employee_info;
        $start_time =  $employee_info->start_time;
//        dd($start_time);
        $end_time = $employee_info->end_time;
//        dd($end_time);

        $start_time = strtotime($start_time);
        $end_time = strtotime($end_time);

        $start_hr = (int) date('H', $start_time);
        $start_min = (int) date('i', $start_time);
        $end_hr = (int) date('H', $end_time);
        $end_min = (int) date('i', $end_time);
        if($end_min){
            $end_hr = $end_hr +1;
        }
        $today = date('m/d/Y');
        $date_check=date('Y-m-d', strtotime($date));
        if($date && $today <> $date){
            for ($i=$start_hr; $i < $end_hr; $i++) {
                foreach (self::$ord_min as $min) {
                        $time = $i.':'.$min;
                        $str_time = strtotime($i.':'.$min);
                        $time_check= date('H:i:s', strtotime($time));
                        $strs_time=$start_time;
                        $appointment_info=Appointmentitem::where('employee_id',$employee)->where('date',$date_check)->where('time',$time_check)->first();
                        $appointment_time=strtotime($appointment_info['time']);
                        if($str_time >= $strs_time && $end_time > $str_time && $str_time !=$appointment_time){
                            $time_option[$time] = $time;
                        }

                }
            }
        }else{

            for ($i=$start_hr; $i < $end_hr; $i++) {
                foreach (self::$ord_min as $min) {
                    $current_time = strtotime(date('H:i'));
//                    $current_time = strtotime('+ 45 Minutes', $current_time);
                    $time = $i.':'.$min;
                    $str_time = strtotime($i.':'.$min);
                    $time_check= date('H:i:s', strtotime($time));
                    $strs_time=$start_time;
                    $appointment_info=Appointmentitem::where('employee_id',$employee)->where('date',$date_check)->where('time',$time_check)->first();
                    $appointment_time=strtotime($appointment_info['time']);
                        if ($str_time != $appointment_time && $str_time >= $strs_time && $str_time > $current_time && $end_time > $str_time ) {
                            $time_option[$time] = $time;
                        }

                }

            }

        }
//         dd($time_option);

        return $time_option;
    }

    public static function next_load($time)
    {

        if($time != null)
        {
            $option = '<button type="button" class="mad_button style3 app_next" >Next</button>';
        }
        return $option;
    }
}
