<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointmentitem extends Model
{
    protected $attributes = [
        'status' => 0,
    ];

    public function employee()
    {
        return $this->belongsTo('\App\Employee','employee_id');
    }

    public function getFPriceAttribute()
    {
        return number_format($this->price,2);
    }
    public function getAppointmentDateAttribute()
    {
        return date('d M, Y', strtotime($this->date));
    }
    public function getAppointmentTimeAttribute()
    {
        return date('h:i A', strtotime($this->time));
    }
}
