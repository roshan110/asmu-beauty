<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CaptchaController extends Controller
{

//    public $pass;

    public function imagejpg(Request $request)
    {

        if(cache()->get('check_value')==1) {
            cache()->put('check_value',0);
            $passes = $this->generate_string();
            cache()->put('random_pass', $passes);

            $width = (int)config('captcha.width');
            $height = (int)config('captcha.height');

            $img = \Image::canvas($width, $height, '#272727');
            $img->text($passes, $width / 2, $height / 2, function ($font) {
                $font->file(public_path('font/gothambook.ttf'));
                $font->size(16);
                $font->color('#fff');
                $font->align('center');
                $font->valign('center');
                $font->angle(0);
            });
            return $img->response('jpg');
        }

    }


    protected function generate_string()
    {
        $chars = config('captcha.chars');
        $number_of_char = config('captcha.number_of_char');

        $chars_length = strlen($chars);

        $random_string = '';

        for ($i = 0; $i < $number_of_char; $i++) {

            $random_string .= $chars[mt_rand(0, $chars_length - 1)];

        }

        return $random_string;
    }
}
