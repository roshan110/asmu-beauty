<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Appointmentitem;
use App\AppointmentTime;
use App\Category;
use App\Employee;
use App\Item;
use App\Mail\AppointmentDetails;
use App\Mail\NewAppointment;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mail;
use Auth;
use Illuminate\Support\Facades\Log;

class AppointmentController extends Controller
{
    public function appointment(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['teams']=Employee::active()->display()->get();
        $data['categories']=Category::where('parent_id', 0)->orderby('order_by')->get();
        return view('appointment.appointments',$data);
    }

    public function fetchitem(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $data = DB::table('items')->where('category_id', $value)->get();
//        dd($data);
        $output = '<option value="">Select '.ucfirst('Service').'</option>';
        foreach($data as $row)
        {
            $output .= '<option value="'.$row->id.'">'.$row->title.' (&pound; '.number_format($row->price,2).')'.'</option>';
        }
        echo $output;
    }
    public function fetchemployee(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $data = Item::where('id',$value)->first();
//        dd($data);
        $output = '<option value="">Select '.ucfirst('Employee').'</option>';
        foreach($data->employees as $row)
        {
            $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
        }
        echo $output;
    }
    public function appointment_dates_load(Request $request)
    {
        $dates=Appointment::employmentHoliday($request->_employee);
        Log::info('controller');
        Log::info($dates);
        $dated[]='';
        $i=0;
        foreach($dates as $date)
        {
            if($date == 'B')
            {
                $dated[$i]=$date;

            }
            else{
                $dated[$i]=(int)$date;
                Log::info('return date');
                Log::info($dated);

            }
            $i++;
        }
        Log::info('Return Last');
        Log::info($dated);
        return $dated;


    }
    public function appointment_time_load(Request $request)
    {

        $data['appointment_time'] = AppointmentTime::hour($request->_employee,$request->_date);
        return view('appointment_time_load', $data);
    }
    public function appointment_next_load(Request $request)
    {
//        $request->_time
        $data['appointment_next'] = AppointmentTime::next_load($request->_time);
        return view('appointment_next_load', $data);
    }


    public function appointment_book(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'name' => 'required|min:3|regex:/^[a-zA-Z ]*$/',
            'email' => 'required|regex:/^[a-zA-Z]{1}/',
            'phone' => 'required',
        ]);



        $appointment= new Appointment();
        if (Auth::check()) {
            $user = Auth::user();
            $appointment->user_id = $user->id;
        }
        $appointment->payment = $request->radio;
        $appointment->name = $request->name;
        $appointment->phone = $request->phone;
        $appointment->email = $request->email;
        if($request->note != null)
        {
            $appointment->note = $request->note;
        }

//        dd($appointment);
        $result=$appointment->save();

        if($result == true)
        {
            $counter = 0;
            for($count = 0; $count < count($request->item); $count++)
            {
                $date = date('Y-m-d', strtotime($request->date[$count]));
                $time = date('H:i:s', strtotime($request->time[$count]));
                $date_time = $date .' '. $time;
                $service_id = $request->item[$count];
                $service=Item::find($service_id);

                $appointmentitem =new Appointmentitem();
                $appointmentitem->appointment_id = $appointment->id;
                $appointmentitem->item = $service->title;
                $appointmentitem->price = $service->price;
                $appointmentitem->employee_id = $request->employee[$count];
                $appointmentitem->date = $date;
                $appointmentitem->time = $time;
                $results=$appointmentitem->save();
                if($results ==true)
                {
                    $counter++;
                }
            }
                if($counter == count($request->item))
                {
                    Mail::send(new AppointmentDetails($appointment));
                    Mail::send(new NewAppointment($appointment));
                    return redirect('checkout/complete');
                }


        }
        else
        {
            return redirect()->back();
        }


    }

    public function complete()
    {
        return view('checkoutcomplete');
    }


    public function storeCostDetails(Request $request)
    {
        $items_id=$request->items;
            $cost=0;
            Log::info($items_id);
        $html='';
        $count=1;
        foreach ($items_id as $key=>$it)
        {
            $it=Item::findOrFail($it);
            $cost=$it->price + $cost;
            $html .= '<tr style="">
                                        <td>#'.$count.'</td>
                                        <td>'.$it->title.'</td>
                                        <td>&pound;'.number_format($it->price,2).'</td>

                                    </tr>';
            $count++;

        }
            $html .= '<tr>
                    <td colspan="2" style="text-align: right;"><strong style="color: #000;">Total</strong></td>
                    <td style="color: #000;">&pound;'.number_format($cost,2).'</td>
                    </tr>';

        return $html;
    }
}
