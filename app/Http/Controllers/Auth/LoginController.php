<?php

namespace App\Http\Controllers\Auth;
use App\Userlog;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm(Request $request)
    {
        if($request->continue){
            $request->session()->put('redirect_url', $request->continue);
        }
        return view('auth.login');
    }
    protected function authenticated(Request $request, $user)
    {
        if($request->session()->has('redirect_url')){
            $redirect_url = $request->session()->get('redirect_url');
            $request->session()->forget('redirect_url');

            $log = new Userlog();
            $log->user_id = $user->id;
            $log->user_ip = $request->ip();
            $log->login_status = 1;
            $log->save();

            return redirect($redirect_url);
        }

    }
    protected function attemptLogin(Request $request)
    {
        $admin=\App\User::where('email',$request->email)->first();
        if($admin->groups != '100' )
        {
            return $this->guard()->attempt(
                $this->credentials($request), $request->filled('remember')
            );
        }


    }
}
