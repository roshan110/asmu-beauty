<?php

namespace App\Http\Controllers;

use App\Appointment;
use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    public function profile()
    {
        $data['user'] = Auth::user();
        $data['lastappointment'] = Appointment::where('name', Auth::user()->name)->where('email',Auth::user()->email)->latest()->first();
        $data['appointmenthistory'] = Appointment::where('name', Auth::user()->name)->where('email',Auth::user()->email)->latest()->take(5)->get();
        return view('profile', $data);
    }
    public function changepassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $user = Auth::user();
        if(\Hash::check($request->old_password, $user->password)){

            $user->password = bcrypt($request->password);
            $user->save();
            return redirect()->route('user.profile')->with('success', 'Password Change');
        }else{
            return back()->with('password_match', 'Old password doesnot match.');
        }
    }
}
