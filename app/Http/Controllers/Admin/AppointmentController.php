<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Mail\AppointmentPayApprove;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today_appointments = Appointment::today()->paginate(10, ['*'], 'today_orders');
        $appointments = Appointment::paginate(20);
        return view('admin.appointment.index', compact('today_appointments', 'appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        return view('admin.appointment.show', compact('appointment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        if($appointment && $appointment->delete()){
            return redirect()->route('appointments.index')->with('success', 'Appointment details deleted.');
        }else{
            return redirect()->route('appointments.index')->with('error', 'Error while deleting Appointment Details.');
        }
    }

    public function approve($id)
    {

        $appointment = Appointment::find(decrypt($id));
        $id=decrypt($id);


        $data = array(
            'paid' => 1,
            'status' => 2,
        );


        if(Appointment::where('id',$appointment->id)->update($data))
        {
            Mail::send(new AppointmentPayApprove($appointment));
            return redirect()->route('appointments.index')->with('success','Appointment Payment was received.');
        }
    }
}

