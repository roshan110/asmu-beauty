<?php

namespace App\Http\Controllers\Admin;


use App\Fbvideo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FbvideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fbvideos = Fbvideo::all();
        return view('admin.fbvideo.index', compact('fbvideos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.fbvideo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:videos',
            'link' => 'required',
        ]);

        $fbvideo = new Fbvideo();
        $fbvideo->title = $request->title;
        $fbvideo->link = $request->link;
        $fbvideo->save();

        return redirect()->route('fbvideos.index')->with('success', 'Facebook Video added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Fbvideo $fbvideo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Fbvideo $fbvideo)
    {
        return view('admin.fbvideo.edit', compact('fbvideo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fbvideo $fbvideo)
    {
        $this->validate($request, [
            'title' => 'required|unique:videos,title,' . $fbvideo->id,
            'link' => 'required',
        ]);

        $fbvideo->title = $request->title;
        $fbvideo->link = $request->link;
        $fbvideo->save();

        return redirect()->route('fbvideos.index')->with('success', 'Facebook Video saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fbvideo $fbvideo)
    {
        if($fbvideo){

            if($fbvideo->image){
                $image = public_path('uploads/fbvideos/' . $fbvideo->image);
                if(file_exists($image)){
                    unlink($image);
                }
            }

            if($fbvideo->delete()){
                return redirect()->route('fbvideos.index')->with('success', 'Facebook Video deleted.');
            }else{
                return redirect()->route('fbvideos.index')->with('error', 'Error while deleting Facebook Video.');
            }

        }else{
            abort();
        }
    }

    public function showhide(Fbvideo $fbvideo)
    {
        if($fbvideo->show_in_home){
            $fbvideo->show_in_home = 0;
        }else{
            $fbvideo->show_in_home = 1;
        }
        $fbvideo->save();
        return redirect()->route('fbvideos.index')->with('success', 'Status Updated.');
    }
}
