<?php

namespace App\Http\Controllers\Admin;

use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::all();
        return view('admin.partner.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' =>'required|image'
        ]);

        $partner = new Partner();
        $partner->title = $request->title;
        $partner->link = $request->link;
        $partner->save();

        if ($request->hasFile('image')) {
            $image_name = 'partner-'.$partner->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/partners/', $image_name);
            $partner->image = $image_name;
            $partner->save();
        }

        return redirect()->route('partners.index')->with('success', 'Partner added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        return view('admin.partner.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partner $partner)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'image',
        ]);

        $partner->title = $request->title;
        
        $partner->link = $request->link;
        
        $partner->save();


        if ($request->hasFile('image')) {
            if($partner->image){
                foreach (config('imagesize') as $filename) {
                    $filename = implode('x', $filename);
                    if(file_exists($image_path = public_path('uploads/partners/' .$filename .'/' . $partner->image))){
                        unlink($image_path);
                    }
                }
                if(file_exists($image_path = public_path('uploads/partners/' . $partner->image))){
                    unlink($image_path);
                }
            }
            $image_name ='partner-'.$partner->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/partners/', $image_name);
            $partner->image = $image_name;
            $partner->save();
        }

        return redirect()->route('partners.index')->with('success', 'Partner saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partner $partner)
    {
        if($partner){
            if($partner->image){
                foreach (config('imagesize') as $filename) {
                    $filename = implode('x', $filename);
                    if(file_exists($image_path = public_path('uploads/partners/' .$filename .'/' . $partner->image))){
                        unlink($image_path);
                    }
                }
                if(file_exists($image_path = public_path('uploads/partners/' . $partner->image))){
                    unlink($image_path);
                }
            }

            if($partner->delete()){
                return redirect()->route('partners.index')->with('success', 'Partner deleted.');
            }else{
                return redirect()->route('partners.index')->with('error', 'Error while deleting Partner.');
            }

        }else{
            abort();
        }
    }
    public function photodelete(Partner $partner)
    {
        if($partner && $partner->image)
        {
            foreach (config('imagesize') as $filename) {
                $filename = implode('x', $filename);
                if(file_exists($image_path = public_path('uploads/partners/' .$filename .'/' . $partner->image))){
                    unlink($image_path);
                }
            }
            if(file_exists($image_path = public_path('uploads/partners/' . $partner->image))){
                unlink($image_path);
            }
            $partner->image = null;
            $partner->save();
        }
        return redirect()->back()->with('success','Image Deleted');
    }
}
