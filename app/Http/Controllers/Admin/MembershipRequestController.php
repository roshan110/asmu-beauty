<?php

namespace App\Http\Controllers\Admin;

use App\Mail\MembershipJoinApprove;
use App\Mail\MembershipJoinDecline;
use App\Membership;
//use App\Mail\MembershipAppDecline;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MembershipJoin;
//use App\Mail\MembershipAppApprove;
use Illuminate\Support\Facades\Mail;

class MembershipRequestController extends Controller
{
    public function index()
    {
    	$data['membershipjoins'] = MembershipJoin::latest()->paginate(10);
    	return view('admin.membership_join.index', $data);
    }

    public function view($id)
    {
    	$data['membershipjoins'] = MembershipJoin::find($id);
    	return view('admin.membership_join.view', $data);
    }

    public function decline($id)
    {
        $membershipjoin = MembershipJoin::find(decrypt($id));
//        dd($classbook);
        $id=decrypt($id);
        $membershipjoin_id=$membershipjoin->membership_id;
        $data = array(
            'status' => -1,
            'paid'=>-1,

        );
        if(MembershipJoin::where('id',$membershipjoin->id)->update($data))
        {
            Mail::send(new MembershipJoinDecline($membershipjoin, $id));
            return redirect()->route('membershipjoin.request')->with('success','Membership Application was declined');
        }
    }

    public function approve($id)
    {

        $membershipjoin = MembershipJoin::find(decrypt($id));
        $id=decrypt($id);
        $membershipjoin_id=$membershipjoin->membership_id;


        $data = array(
            'status' => 1,
            'paid' => 1,
        );


        if(MembershipJoin::where('id',$membershipjoin->id)->update($data))
        {
            Mail::send(new MembershipJoinApprove($membershipjoin, $id));
            return redirect()->route('membershipjoin.request')->with('success','Membership Application Request was approved');
        }
    }
}
