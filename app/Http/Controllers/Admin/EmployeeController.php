<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Item;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $employees = Employee::query();

        if($request->keyword){
            if(is_numeric($request->keyword)){
                $employees->where('id', $request->keyword);
            }else{
                $like = '%' . $request->keyword . '%';
                $employees->orWhere('name', 'like', $like);
            }

        }

        $data['employees'] = $employees->orderBy('created_at', 'desc')->paginate(20);
        return view('admin.employee.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ];
        $items=Item::active()->get();

        return view('admin.employee.create',compact('items','days'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'email' =>'required',
            'address' => 'required',
            'phone' => 'required',
            'details' => 'required',
            'image' => 'image',
        ]);

        $holiday_array[]='';
        if(!empty($request->holiday)) {
            for ($counts = 0; $counts < count($request->holiday); $counts++) {
                $holiday_array[$counts] = $request->holiday[$counts];
            }
            $holiday = implode(",", $holiday_array);
        }
        $employee = new Employee();
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->phone = $request->phone;
        $employee->country = $request->country;
        $employee->details = $request->details;
        if(!empty($request->holiday)) {
            $employee->holiday = $holiday;
        }
        else
        {
            $employee->holiday = null;
        }
        $employee->start_time = $request->start_time;
        $employee->end_time = $request->end_time;
        $employee->save();

//        dd($employee);


        if ($request->hasFile('image')) {
            $image_name = 'employee-'.$employee->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/employees/', $image_name);
            $employee->image = $image_name;
            $employee->save();
        }
        if($request->item)
        {
            $item=Item::find($request->item);
            $employee->items()->sync($item);
        }
        return redirect()->route('employees.index')->with('success', 'Employee added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $items=Item::active()->get();
        $days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ];
        $values = explode(",", $employee->holiday);
        return view('admin.employee.edit', compact('employee','items','days','values'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' =>'required',
            'address' => 'required',
            'phone' => 'required',
            'details' => 'required',
            'image' => 'image',
        ]);

        $holiday_array[]='';
        if(!empty($request->holiday)) {
            for ($counts = 0; $counts < count($request->holiday); $counts++) {
                $holiday_array[$counts] = $request->holiday[$counts];
            }
            $holiday = implode(",", $holiday_array);
        }

        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->phone = $request->phone;
        $employee->country = $request->country;
        $employee->details = $request->details;
        if(!empty($request->holiday) ) {
            $employee->holiday = $holiday;
        }
        else
        {
            $employee->holiday = null;
        }
        $employee->start_time = $request->start_time;
        $employee->end_time = $request->end_time;

//        dd($employee);
        $employee->save();

        if ($request->hasFile('image')) {
            if($employee->image){
                foreach (config('imagesize') as $filename) {
                    $filename = implode('x', $filename);
                    if(file_exists($image_path = public_path('uploads/employees/' .$filename .'/' . $employee->image))){
                        unlink($image_path);
                    }
                }
                if(file_exists($image_path = public_path('uploads/employees/' . $employee->image))){
                    unlink($image_path);
                }
            }
            $image_name = 'employee-'.$employee->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/employees/', $image_name);
            $employee->image = $image_name;
            $employee->save();
        }
        if($request->item)
        {
            $items=Item::find($request->item);
            $employee->items()->sync($items);
        }
        else
        {
            $employee->items()->detach();
        }
        return redirect()->route('employees.index')->with('success', 'Employee saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        if($employee){

            if($employee->image){
                foreach (config('imagesize') as $filename) {
                    $filename = implode('x', $filename);
                    if(file_exists($image_path = public_path('uploads/employees/' .$filename .'/' . $employee->image))){
                        unlink($image_path);
                    }
                }
                if(file_exists($image_path = public_path('uploads/employees/' . $employee->image))){
                    unlink($image_path);
                }
            }

            if($employee->delete()){
                $employee->items()->detach();
                return redirect()->route('employees.index')->with('success', 'Employee deleted.');
            }else{
                return redirect()->route('employees.index')->with('error', 'Error while deleting Employee.');
            }

        }else{
            abort(404);
        }
    }
    public function showhide(Employee $employee)
    {
        if($employee->status){
            $employee->status = 0;
        }else{
            $employee->status = 1;
        }
        $employee->save();
        return redirect()->route('employees.index')->with('success', 'Status Update.');
    }
    public function showhidepage(Employee $employee)
    {
        if($employee->show_in_page){
            $employee->show_in_page = 0;
        }else{
            $employee->show_in_page = 1;
        }
        $employee->save();
        return redirect()->route('employees.index')->with('success', 'Status Update.');
    }
    public function photodelete(Employee $employee)
    {
        if($employee && $employee->image)
        {
            foreach (config('imagesize') as $filename) {
                $filename = implode('x', $filename);
                if(file_exists($image_path = public_path('uploads/employees/' .$filename .'/' . $employee->image))){
                    unlink($image_path);
                }
            }
            if(file_exists($image_path = public_path('uploads/employees/' . $employee->image))){
                unlink($image_path);
            }
            $employee->image = null;
            $employee->save();
        }
        return redirect()->back()->with('success','Image Deleted');
    }
}
