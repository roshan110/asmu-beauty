<?php

namespace App\Http\Controllers\Admin;

use App\Membership;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembershipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $memberships = Membership::query();

        if($request->keyword){
            if(is_numeric($request->keyword)){
                $memberships->where('id', $request->keyword);
            }else{
                $like = '%' . $request->keyword . '%';
                $memberships->orWhere('title', 'like', $like);
            }
            
        }

        $data['memberships'] = $memberships->orderBy('created_at', 'desc')->paginate(20);
        return view('admin.membership.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.membership.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'title' => 'required|unique:memberships',
            'details' => 'required',
            'price' => 'required',
        ]);

        if(isset($request->meta_title))
        {$meta_title=$request->meta_title;}
        else{$meta_title=$request->title;}
        if(isset($request->meta_description))
        {$meta_description=$request->meta_description;}
        else{$meta_description=$request->title;}
        if(isset($request->meta_keyword))
        {$meta_keyword=$request->meta_keyword;}
        else{$meta_keyword=$request->title;}

        $membership = new Membership();
        $membership->title = $request->title;
        $membership->slug = str_slug($request->title);
        $membership->details = $request->details;
        $membership->price = $request->price;
        $membership->meta_title = $meta_title;
        $membership->meta_keyword= $meta_keyword;
        $membership->meta_description = $meta_description;
//        dd($membership);
        $membership->save();

        if ($request->hasFile('image')) {
            $image_name = 'membership-'.$membership->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/memberships/', $image_name);
            $membership->image = $image_name;
            $membership->save();
        }

        return redirect()->route('memberships.index')->with('success', 'Membership added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function show(Membership $membership)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Membership $membership
     * @return \Illuminate\Http\Response
     */
    public function edit(Membership $membership)
    {
        return view('admin.membership.edit', compact('membership'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Membership $membership)
    {
        // dd($request);
        $this->validate($request, [
            'title' => 'required|unique:memberships,title,' . $membership->id,
            'details' => 'required',
            'price' => 'required'
        ]);

        $membership->title = $request->title;
        $membership->details = $request->details;
        $membership->price = $request->price;
        $membership->meta_title = $request->meta_title;
        $membership->meta_keyword= $request->meta_keyword;
        $membership->meta_description = $request->meta_description;
        $membership->save();

        if ($request->hasFile('image')) {
            if($membership->image){
                foreach (config('imagesize') as $filename) {
                    $filename = implode('x', $filename);
                    if(file_exists($image_path = public_path('uploads/memberships/' .$filename .'/' . $membership->image))){
                        unlink($image_path);
                    }                        
                }
                if(file_exists($image_path = public_path('uploads/memberships/' . $membership->image))){
                    unlink($image_path);                        
                }
            }
            $image_name = 'membership-'.$membership->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/memberships/', $image_name);
            $membership->image = $image_name;
            $membership->save();
        }

        return redirect()->route('memberships.index')->with('success', 'Membership saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function destroy(Membership $membership)
    {
        if($membership){

            if($membership->image){
                foreach (config('imagesize') as $filename) {
                    $filename = implode('x', $filename);
                    if(file_exists($image_path = public_path('uploads/memberships/' .$filename .'/' . $membership->image))){
                        unlink($image_path);
                    }                        
                }
                if(file_exists($image_path = public_path('uploads/memberships/' . $membership->image))){
                    unlink($image_path);                        
                }
            }

            if($membership->delete()){
                return redirect()->route('memberships.index')->with('success', 'Membership deleted.');
            }else{
                return redirect()->route('memberships.index')->with('error', 'Error while deleting Membership.');
            }

        }else{
            abort(404);
        }
    }

    public function showhide(Membership $membership)
    {
        if($membership->status){
            $membership->status = 0;
        }else{
            $membership->status = 1;
        }
        $membership->save();
        return redirect()->route('memberships.index')->with('success', 'Status Update.');
    }

    public function photodelete(Membership $membership)
    {
        if($membership && $membership->image)
        {
            if(file_exists(public_path('/uploads/memberships/'.$membership->image)))
            {
                unlink(public_path('/uploads/memberships/'.$membership->image));
            }
            $membership->image = null;
            $membership->save();
        }
        return redirect()->back()->with('success','Image Deleted');
    }
}
