<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Userlog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = User::where('groups',100)->get();
        $admin_id=Auth::user()->id;
        $loged=Userlog::where('user_id',$admin_id)->first();
        $logs=Userlog::all();
        return view('admin.admin.index', compact('admins','loged','logs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',            
            'email' => 'required|unique:users',            
            'password' => 'required',
        ]);

        $admin = new User;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->groups = 100;
        $admin->save();


        return redirect()->route('admins.index')->with('success', 'Admin added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Social  $social
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admins=Userlog::where('user_id',$id)->latest()->paginate(15);
        $loged=Userlog::where('user_id',$id)->first();
        return view('admin.admin.view', compact('admins','loged'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Social  $social
     * @return \Illuminate\Http\Response
     */
    public function edit(User $admin)
    {
        return view('admin.admin.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Social  $social
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $admin)
    {
        $this->validate($request, [
            'name' => 'required',            
            'email' => 'required| unique:users,email,$admin->id',            
            'password' => 'required',
        ]);

        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->groups = 100;
        $admin->save();

        return redirect()->route('admins.index')->with('success', 'Admin saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Social  $social
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $admin)
    {

            if($admin->delete()){
                return redirect()->route('admins.index')->with('success', 'Admin deleted.');
            }else{
                return redirect()->route('admins.index')->with('error', 'Error while deleting admin.');
            }

    }

    public function activity()
    {
        $admin_id=Auth::user()->id;
        $loged=Userlog::where('user_id',$admin_id)->first();
        $logs=Userlog::where('user_id',$admin_id)->latest()->paginate(15);

        return view('admin.useractivity',compact('logs','loged'));
    }
}
