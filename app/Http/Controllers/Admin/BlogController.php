<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $blogs = Blog::query();

        if($request->keyword){
            if(is_numeric($request->keyword)){
                $blogs->where('id', $request->keyword);
            }else{
                $like = '%' . $request->keyword . '%';
                $blogs->orWhere('title', 'like', $like);
            }
            
        }

        $data['blogs'] = $blogs->orderBy('created_at', 'desc')->paginate(20);
        return view('admin.blog.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'title' => 'required|unique:blogs',
            'details' => 'required',
            'image' => 'image',
        ]);

        if(isset($request->meta_title))
        {$meta_title=$request->meta_title;}
        else{$meta_title=$request->title;}
        if(isset($request->meta_description))
        {$meta_description=$request->meta_description;}
        else{$meta_description=$request->title;}
        if(isset($request->meta_keyword))
        {$meta_keyword=$request->meta_keyword;}
        else{$meta_keyword=$request->title;}

        $blog = new Blog();
        $blog->title = $request->title;
        $blog->slug = str_slug($request->title);
        $blog->details = $request->details;
        $blog->author = $request->author;
        $blog->meta_title = $meta_title;
        $blog->meta_keyword= $meta_keyword;
        $blog->meta_description = $meta_description;
//        dd($blog);
        $blog->save();

        if ($request->hasFile('image')) {
            $image_name = 'blog-'.$blog->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/blogs/', $image_name);
            $blog->image = $image_name;
            $blog->save();
        }

        return redirect()->route('blogs.index')->with('success', 'Blog added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return view('admin.blog.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        // dd($request);
        $this->validate($request, [
            'title' => 'required|unique:blogs,title,' . $blog->id,
            'details' => 'required',
            'image' => 'image',
        ]);

        $blog->title = $request->title;
        $blog->details = $request->details;
        $blog->author = $request->author;
        $blog->meta_title = $request->meta_title;
        $blog->meta_keyword= $request->meta_keyword;
        $blog->meta_description = $request->meta_description;
        $blog->save();

        if ($request->hasFile('image')) {
            if($blog->image){
                foreach (config('imagesize') as $filename) {
                    $filename = implode('x', $filename);
                    if(file_exists($image_path = public_path('uploads/blogs/' .$filename .'/' . $blog->image))){
                        unlink($image_path);
                    }                        
                }
                if(file_exists($image_path = public_path('uploads/blogs/' . $blog->image))){
                    unlink($image_path);                        
                }
            }
            $image_name = 'blog-'.$blog->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/blogs/', $image_name);
            $blog->image = $image_name;
            $blog->save();
        }

        return redirect()->route('blogs.index')->with('success', 'Blog saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        if($blog){

            if($blog->image){
                foreach (config('imagesize') as $filename) {
                    $filename = implode('x', $filename);
                    if(file_exists($image_path = public_path('uploads/blogs/' .$filename .'/' . $blog->image))){
                        unlink($image_path);
                    }                        
                }
                if(file_exists($image_path = public_path('uploads/blogs/' . $blog->image))){
                    unlink($image_path);                        
                }
            }

            if($blog->delete()){
                return redirect()->route('blogs.index')->with('success', 'Blog deleted.');
            }else{
                return redirect()->route('blogs.index')->with('error', 'Error while deleting Blog.');
            }

        }else{
            abort(404);
        }
    }

    public function showhide(Blog $blog)
    {
        if($blog->status){
            $blog->status = 0;
        }else{
            $blog->status = 1;
        }
        $blog->save();
        return redirect()->route('blogs.index')->with('success', 'Status Update.');
    }

    public function photodelete(Blog $blog)
    {
        if($blog && $blog->image)
        {
            if(file_exists(public_path('/uploads/blogs/'.$blog->image)))
            {
                unlink(public_path('/uploads/blogs/'.$blog->image));
            }
            $blog->image = null;
            $blog->save();
        }
        return redirect()->back()->with('success','Image Deleted');
    }
}
