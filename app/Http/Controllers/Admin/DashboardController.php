<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\MembershipJoin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class DashboardController extends Controller
{
    public function index()
    {
        $data['memberships'] = MembershipJoin::where('created_at', '>=', date('Y-m-d'))->orderBy('created_at','desc')->take(5)->get();
        $data['appointments'] = Appointment::where('created_at', '>=', date('Y-m-d'))->orderBy('created_at','desc')->take(5)->get();
        $data['user_details']=User::all();
    	return view('admin.dashboard', $data);
    }

    public function passwordchange()
    {
    	return view('auth.passwords.change');
    }

    public function passwordstore(Request $request)
    {
    	$this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
		]);
		
		$user = \Auth::user();
		if (Hash::check($request->old_password, $user->password)) {
			$user->password = Hash::make($request->password);
			$user->save();
			\Session::flash('success', 'Password Changed');
			return redirect('admin');
		}else{
			\Session::flash('error', 'Password does not match');
			return redirect('admin');
		}
    }

    public function phpinfo()
    {
    	phpinfo();
    }

}
