<?php

namespace App\Http\Controllers;

use App\Album;
use App\Blog;
use App\Category;
use App\Employee;
use App\Fbvideo;
use App\Mail\AutoResponse;
use App\Mail\Contact;
use App\Mail\MembershipJoining;
use App\Membership;
use App\MembershipJoin;
use App\Page;
use App\Partner;
use App\Photo;
use App\Review;
use App\Slide;
use App\Video;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Log;


class PageController extends Controller
{
    public function home()
    {
        $page = Page::find(1);
        if(!$page){
            $page = Page::where('slug', '/')->first();
        }
        $data['page'] = $page;
        $data['slides']=Slide::active()->latest()->take(3)->get();
        $data['services']=Page::where('parent_id',5)->get();
        $data['reviews'] = Review::active()->latest()->take(3)->get();
        $data['video']=Video::where('show_in_home',1)->active()->latest()->first();
        $data['fbvideo']=Fbvideo::where('show_in_home',1)->active()->latest()->first();
        $data['photos']=Photo::active()->latest()->take(8)->get();
        $data['partners'] = Partner::active()->latest()->get();
        return view('index',$data);
    }
    public function index(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        return view('page', $data);
    }
    public function contact(Request $request)
    {
        if($request->isMethod('get')) {
            cache()->put('check_value',1);
            $data['page'] = $page = Page::where('slug', $request->path())->first();
            return view('contact', $data);
        }
    }

    public function contactmail(Request $request)
    {

        if($request->isMethod('post')) {
//            echo(cache()->get('random_pass'));
//            dd($request->all());
            $this->validate($request, [
                'name' => 'required|min:3|regex:/^[a-zA-Z ]*$/',
                'email' => 'required|regex:/^[a-zA-Z]{1}/',
                'message' => 'required|regex:/^[a-zA-Z]{1}/',
                'captcha' => 'required',
            ]);

            if (cache()->get('random_pass')  <> $request->captcha) {
                return redirect()->back()->withErrors(['captcha' => 'Invalid Captcha'])->withInput();
            }
            Mail::send(new Contact($request));
            Mail::send(new AutoResponse($request));
            return redirect()->route('thankyou');
        }
    }

    public function aboutus(Request $request)
    {   if($request->isMethod('get')) {
        $data['page'] = $page = Page::where('slug', $request->path())->first();
        return view('aboutus', $data);
        }
    }

    public function ourteam(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['teams']=Employee::active()->display()->get();
        return view('ourteam', $data);
    }

    public function review(Request $request)
    {
        cache()->put('check_value',1);
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['reviews'] = Review::active()->get();
        return view('review', $data);
    }

    public function thankyou(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        return view('thankyou', $data);

    }
    public function reviewsave(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'name' => 'required|min:3|regex:/^[a-zA-Z ]*$/',
            'title'=>'required|regex:/^[a-zA-Z ]*$/',
            'email' => 'required|regex:/^[a-zA-Z]{1}/',
            'image' => 'image|max:1024|mimes:jpeg,jpg,png',
            'message' => 'required',
            'captcha' => 'required',
        ]);

        if(cache()->get('random_pass')  <> $request->captcha){
            return redirect()->back()->withErrors(['captcha' => 'Invalid Cpatcha'])->withInput();
        }

        $review = new Review();
        $review->title=$request->title;
        $review->name = $request->name;
        $review->email = $request->email;
        $review->details = $request->message;
        $review->designation = $request->designation;
        $review->status = 0;
        $review->save();

        if ($request->hasFile('image')) {
            $image_name = 'review-'.$review->id . '.'.$request->image->extension();
            $path = $request->image->move('uploads/reviews/', $image_name);
            $review->image = $image_name;
            $review->save();
        }
        Mail::send(new AutoResponse($request));
        return redirect()->route('thankyou');

    }

    public function photogallery(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['albums'] = Album::all();
        $data['photos'] = Photo::all();
        return view('gallery.photo-gallery', $data);

    }
    public function videogallery(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['fbvideos'] = Fbvideo::all();
        $data['videos'] = Video::all();
        return view('gallery.video-gallery', $data);

    }
    public function blog(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['blogs'] = Blog::active()->paginate(2);
        $data['services_c'] = Page::where('parent_id',5)->where('status',1)->get();
        $data['latest_blogs'] = Blog::latest()->take(5)->get();
        return view('blog.blogs', $data);

    }
    public function singleblog(Request $request,Blog $blog)
    {
//        dd($request->path());
        $path_named = explode('/',$request->path());
        $first_pathname = $path_named[0];
        $last_pathname = $path_named[1];
        $data['page'] = $page =  Page::where('slug',$first_pathname)->first();
        $data['services_c'] = Page::where('parent_id',5)->where('status',1)->get();
        $data['blog']=Blog::find($blog->id);
        if(!$blog){
            abort('404');
        }
//
        $blog->visitors = $blog->visitors + 1;
        $blog->save();
//        $related_blog = Post::related($post);
        $data['latest_blogs'] = Blog::latest()->take(5)->get();

        return view('blog.blog_single',$data);
    }
    public function membership(Request $request)
    {
        $data['page'] = $page =  Page::where('slug',$request->path())->first();
        $data['memberships']=Membership::active()->latest()->get();
        return view('membership.memberships',$data);
    }
    public function membershipdetails($id)
    {
        cache()->put('check_value',1);
        $membership_id=decrypt($id);
//        dd($membership_id);
        $data['membershiped']=Membership::find($membership_id);
//        dd($data);
        $data['services_c'] = Page::where('parent_id',5)->where('status',1)->get();
        return view('membership.membership-details',$data);
    }

    public function membershipcheckout(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'fname' => 'required|min:3|regex:/^[a-zA-Z ]*$/',
            'lname'=>'required|regex:/^[a-zA-Z ]*$/',
            'email' => 'required|regex:/^[a-zA-Z]{1}/',
            'address' => 'required|min:3|alpha_num',
            'phone' => 'required',
            'postal' => 'required',
            'captcha' => 'required',
        ]);
        if (cache()->get('random_pass') <> $request->captcha) {
            return redirect()->back()->withErrors(['captcha' => 'Invalid Captcha'])->withInput();
        }
        $member_name=$request->fname.' '.$request->lname;
//        dd($member_name);
        $data = array(
            'name' => $member_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'membership_id' => $request->membership_id,
            'service' => $request->service,
            'postal' => $request->postal,
        );
//        $membershipjoin_check=MembershipJoin::where('email',$request->email)->where('course_id',$request->course_id)->where('shift',$request->shift)->first();

        $result=MembershipJoin::create($data);
        if($result == true)
        {
            Mail::send(new MembershipJoining($request));
            Mail::send(new AutoResponse($request));
            return redirect()->route('thankyou');
        }
        else
        {
            return redirect()->back();
        }
    }

    public function service()
    {
        $data['services']=Page::where('parent_id',5)->get();
        return view('service',$data);
    }


}

