<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
	protected $attributes = [
	'status' => 1,
	];



	public static function facebook()
	{
		$social = self::where('title','Facebook')->first();
		return $social?$social->link:null;
	}

}
