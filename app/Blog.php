<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	public static $_thumbdir = [
		'700x525',
		'400x300',
		'100x80',
		'300x300',
	];

    protected $attributes = [
    	'status' => 1,
		'visitors' => 200,
    ];
	protected $maxfileSize = 2*1024*1024;

    protected $fillable = [
    	'title', 'slug', 'image', 'details','visitors','author', 'meta_title', 'meta_keyword', 'meta_description',
    ];

	public function getNepHumansDateAttribute()
	{
		\Date::setLocale('ne');
		$ndate = new NDate();
		return $ndate->toNepNum(\Date::parse($this->created_at)->diffForHumans());
		// return $this->created_at->diffForHumans();
	}

	public function getNepDateAttribute()
	{
		return $nepDate = new Ndate($this->created_at);
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function updater()
	{
		return $this->belongsTo('App\User', 'updated_by', 'id');
	}


	public function getAuthorNameAttribute()
	{
		return ($this->author ? $this->author : 'admin');
	}

	public function getCreatedByAttribute()
	{
		return ($this->created_at->format('Y-d-m H:m'));
	}

	public function getUpdateddByAttribute()
	{
		return ($this->updater ? $this->updater->name . '[' . $this->updated_at->format('Y-d-m H:m') . ']' : '-');
	}

	public function getTimeAttribute()
	{
		$creat_at=$this->created_at;
		$today=\Carbon\Carbon::now();
		$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $today);
		$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $creat_at);
		$diff_in_days = $to->diffInDays($from);
		return $diff_in_days;
	}
	public function scopeActive($query)
	{
		return $query->where('status', 1);
	}


	public function getFTitleAttribute()
	{
		return (trim($this->title) == '�?��?��?��?��?勤務中�?�爆� �予告' && !empty($this->title_asdf)) ? $this->title_asdf : $this->title;
	}
	public function image($size = null)
	{

		$_image = false;
		if($size){

			$original = public_path('uploads/blogs/'. $this->image);
			if($this->image && file_exists($original)){
				$_image = asset('uploads/blogs/' . $size . '/' . $this->image);
				$_image_path = public_path('uploads/blogs/' . $size . '/' . $this->image);
				if(!file_exists($_image_path)){
					$_image = new  _image('blogs', $size, $this->image);
				}
			}
		}else{
			$original = public_path('uploads/blogs/'. $this->image);
			if($this->image && file_exists($original)){
				$_image = asset('uploads/blogs/'. $this->image);
			}
		}
		return $_image;
	}


	public function url()
	{
		return url('blog/' . $this->id);
	}

	public function getUrlAttribute()
	{
		return url('blog/' . $this->id);
	}

//	public function image($size = '450x300')
//	{
//		if($this->image){
//			$image = public_path('uploads/blogs/' . $this->image);
//			if (file_exists($image)) {
//				return asset('uploads/blogs/' . $this->image);
//			}else{
//				return 'holder.js/' . $size . '/image not found';
//			}
//		}else{
//			return 'holder.js/' . $size . '/image not found';
//		}
//	}

	public static function related($blog)
	{
		$key = str_replace('"', '', $blog->title);
		return $blogs = Blog::select(
			array(
				'*',
				\DB::raw('MATCH(title) AGAINST("' . $key . '" IN BOOLEAN MODE) AS relevance')
			)
		)->whereRaw(\DB::raw('MATCH(title) AGAINST("' . $key . '" IN BOOLEAN MODE)'))
			->where('id','<>',$blog->id)
			->orderBy('relevance')
			->active()
			->take(4)
			->get();
	}

	protected function imageCompress($image)
	{
		if($image){
			$imagesize = getimagesize($image);
			$filesize = filesize($image);
			if($filesize > $this->maxFileSize){
				$newimage = $this->resize_image($image);
				if($imagesize['mime'] == "image/gif"){
					imagegif($newimage, $image);
				}elseif($imagesize['mime'] == "image/jpeg"){
					imagejpeg($newimage, $image);
				}elseif($imagesize['mime'] == "image/bmp"){
					imagebmp($newimage, $image);
				}elseif($imagesize['mime'] == "image/png"){
					imagepng($newimage, $image);
				}
			}
		}
	}

	function resize_image($file, $w=1000, $h=1000) {
		$imagesize = getimagesize($file);
		if($imagesize){

			$width = $imagesize[0];
			$height = $imagesize[1];
			$r = $width / $height;
			if ($w/$h > $r) {
				$newwidth = $h*$r;
				$newheight = $h;
			} else {
				$newheight = $w/$r;
				$newwidth = $w;
			}
			if($imagesize['mime'] == "image/gif"){
				$src = imagecreatefromgif($file);
			}elseif($imagesize['mime'] == "image/jpeg"){
				$src = imagecreatefromjpeg($file);
			}elseif($imagesize['mime'] == "image/bmp"){
				$src = imagecreatefrombmp($file);
			}elseif($imagesize['mime'] == "image/png"){
				$src = imagecreatefrompng($file);
			}elseif($imagesize['mime'] == "image/webp"){
				$src = \imagecreatefromwebp($file);
			}
			$dst = imagecreatetruecolor($newwidth, $newheight);
			imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			return $dst;

		}else{
			return false;

		}

	}
}
