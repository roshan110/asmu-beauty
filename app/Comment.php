<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $attributes = [
	'status' => 1,
	];

	public function post()
	{
		return $this->belongsTo('App\Blog');
	}

}
