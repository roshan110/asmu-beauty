<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home')->name('_root_');


//Routes for Admin
// routes for admin
Route::get('admin/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Admin\LoginController@login');
Route::get('admin/logout', 'Admin\LoginController@logout')->name('admin.logout');
Route::post('admin/logout', 'Admin\LoginController@logout');


// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'namespace' => 'Admin'], function(){
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::get('/phpinfo', 'DashboardController@phpinfo');
    Route::resource('pages', 'PageController');
    Route::post('pages/order', 'PageController@order')->name('pages.order');
    Route::get('pages/showhide/{page}', 'PageController@showhide')->name('pages.showhide');
    Route::get('pages/photodelete/{page}', 'PageController@photodelete')->name('pages.photodelete');

    Route::resource('blogs', 'BlogController');
    Route::get('blogs/showhide/{blog}', 'BlogController@showhide')->name('blogs.showhide');
    Route::get('blogs/photodelete/{blog}', 'BlogController@photodelete')->name('blogs.photodelete');

    Route::resource('slides', 'SlideController');
    Route::post('slides/order', 'SlideController@order')->name('slides.order');
    Route::get('slides/showhide/{slide}', 'SlideController@showhide')->name('slides.showhide');
    Route::resource('videos', 'VideoController');
    Route::get('videos/showhide/{video}', 'VideoController@showhide')->name('videos.showhide');
    Route::resource('fbvideos', 'FbvideoController');
    Route::get('fbvideos/showhide/{fbvideo}', 'FbvideoController@showhide')->name('fbvideos.showhide');
    Route::resource('albums','AlbumController');
    Route::post('albums/photodelete', 'AlbumController@photodelete')->name('albums.photodelete');

    Route::resource('labels', 'LabelController');
    Route::resource('settings', 'SettingController');
    Route::post('logo', 'SettingController@logoChange')->name('change.logo');
    Route::post('headerbg', 'SettingController@headerbgChange')->name('change.headerbg');
    Route::resource('reviews', 'ReviewController');
    Route::get('reviews/approve/{review}', 'ReviewController@approve')->name('reviews.approve');
    Route::get('reviews/cancel/{review}', 'ReviewController@cancel')->name('reviews.cancel');

    Route::resource('memberships', 'MembershipController');
    Route::get('memberships/showhide/{membership}', 'MembershipController@showhide')->name('memberships.showhide');
    Route::get('memberships/photodelete/{membership}', 'MembershipController@photodelete')->name('memberships.photodelete');

    Route::get('membershipjoin-request','MembershipRequestController@index')->name('membershipjoin.request');
    Route::get('membershipjoin-request/{id}','MembershipRequestController@view')->name('membershipjoin.request.view');
    Route::post('membershipjoin-request/{id}/approve', 'MembershipRequestController@approve')->name('membershipjoin.request.approve');
    Route::post('membershipjoin-request/{id}/decline', 'MembershipRequestController@decline')->name('membershipjoin.request.decline');

    Route::resource('socials', 'SocialController');
    Route::get('partners/photodelete/{partner}', 'PartnerController@photodelete')->name('partners.photodelete');
    Route::resource('partners', 'PartnerController');
    Route::resource('admins', 'AdminController');

    Route::resource('categories', 'CategoryController');
    Route::post('categories/order', 'CategoryController@order')->name('categories.order');
    Route::resource('items', 'ItemController');
    Route::post('items/order', 'ItemController@order')->name('items.order');

    Route::resource('employees', 'EmployeeController');
    Route::get('employees/showhide/{employee}', 'EmployeeController@showhide')->name('employees.showhide');
    Route::get('employees/showhidepage/{employee}', 'EmployeeController@showhidepage')->name('employees.showhidepage');
    Route::get('employees/photodelete/{employee}', 'EmployeeController@photodelete')->name('employees.photodelete');

    Route::resource('appointments', 'AppointmentController');
    Route::post('appointment/{id}/paid', 'AppointmentController@approve')->name('appointment.paid.approve');


    Route::get('password/change', 'DashboardController@passwordchange')->name('password.change');
    Route::get('activity', 'AdminController@activity')->name('admin.activity');
    Route::post('password/change', 'DashboardController@passwordstore');

});


$router = app()->make('router');
$pages = App\Page::all();
foreach ($pages as $page) {
    if($page->slug == 'contact'){
        $router->get($page->slug, 'PageController@contact')->name('contact');
    }elseif($page->slug == 'about-us')
    {
        $router->get($page->slug,'PageController@aboutus');
    } elseif($page->slug == 'our-team')
    {
        $router->get($page->slug,'PageController@ourteam');
    } elseif($page->slug == 'testimonials')
    {
        $router->get($page->slug,'PageController@review');
    }elseif($page->slug == 'blog')
    {
        $router->get($page->slug,'PageController@blog')->name('blogs.main');
    }
    elseif($page->slug == 'single-blog')
    {
        $router->get($page->slug.'/{blog}','PageController@singleblog')->name('singleblog');
    }
    elseif($page->slug == 'photo-gallery')
    {
        $router->get($page->slug,'PageController@photogallery')->name('photod');
    }elseif($page->slug == 'video-gallery')
    {
        $router->get($page->slug,'PageController@videogallery')->name('videod');
    }elseif($page->slug == 'membership')
    {
        $router->get($page->slug,'PageController@membership');
    }elseif($page->slug == 'thank-you')
    {
        $router->get($page->slug,'PageController@thankyou')->name('thankyou');
    }elseif($page->slug == 'appointment')
    {
        $router->get($page->slug,'AppointmentController@appointment')->name('appointment');
    }
    elseif($page->slug <> '/'){
        $router->get($page->slug, 'PageController@index');
    }
}

Auth::routes();

Route::get('captcha/image/jpg', 'CaptchaController@imagejpg')->name('captcha_img');
Route::post('contactmail', 'PageController@contactmail')->name('contactmail');



Route::group(['middlewareGroups' => 'web'], function () {

    Route::post('reviewstore', 'PageController@reviewsave')->name('reviewstore');
    Route::get('service','PageController@service')->name('service.main');
    Route::get('membership/{id}','PageController@membershipdetails')->name('membership.details');
    Route::post('membership/checkout','PageController@membershipcheckout')->name('membership.checkout');
    Route::post('dynamic_dependent/fetch/item',[
        'as'=>'dynamindependent.fetch_item','uses'=>'AppointmentController@fetchitem'
    ]);
    Route::post('dynamic_dependent/fetch/employee',[
        'as'=>'dynamindependent.fetch_employee','uses'=>'AppointmentController@fetchemployee'
    ]);
    Route::post('appointment_dates_load','AppointmentController@appointment_dates_load')->name('appointment_dates_load');
    Route::post('appointment_time_load','AppointmentController@appointment_time_load')->name('appointment_time_load');

    Route::post('appointment_next_load','AppointmentController@appointment_next_load')->name('appointment_next_load');
    Route::post('appointment/book','AppointmentController@appointment_book')->name('appointment_book');
    Route::get('checkout/complete','AppointmentController@complete')->name('checkout.complete');

//    Route::get('blogs/{blog}', 'PageController@singleblog')->name('singleblog');
});
Route::group(['middleware' => ['auth']], function(){
    Route::get('profile', 'UserController@profile')->name('user.profile');
    Route::post('changepassword', 'UserController@changepassword')->name('changepassword');
});

Route::fallback(function(){
    return response()->view('errors.404', [], 404);
})->name('fallback');


Route::get('/appointmentservice','AppointmentController@storeCostDetails')->name('appointmentService');
