<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('captcha/image/jpg',function()
{
    if(cache()->get('check_value')==1) {
        cache()->put('check_value',0);
        $passes = \App\Http\Controllers\CaptchaController::generate_string();
        cache()->put('random_pass', $passes);

        $width = (int)config('captcha.width');
        $height = (int)config('captcha.height');

        $img = \Image::canvas($width, $height, '#272727');
        $img->text($passes, $width / 2, $height / 2, function ($font) {
            $font->file(public_path('font/gothambook.ttf'));
            $font->size(16);
            $font->color('#fff');
            $font->align('center');
            $font->valign('center');
            $font->angle(0);
        });
        return $img->response('jpg');
    }
    Log::info(cache()->get('random_pass'));
});
