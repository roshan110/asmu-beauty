<?php

return [
    '1'=>'icon-night',
    '2'=>'icon-women',
    '3'=>'icon-other-1',
    '4'=>'icon-medical',
    '5'=>'icon-cut-2',
    '6'=>'icon-people',
    '7'=>'icon-cog',
    '8'=>'icon-cut-1',
    '9'=>'icon-other-2',
];