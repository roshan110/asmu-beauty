$(document).ready(function () {
	"use strict";
	if (screen.width >= 720) {
		var height = $(window).height();
		var navbar_top = $('.navbar').outerHeight();
		var slide_bar = $('.side-bar').height();
		$('.content').css('min-height', height);
		$('#side-bar').css('top', navbar_top);

		if (height < slide_bar) {
			$('.side-bar').css('height', height + 20)
		}

		$('#side-bar').enscroll({
			verticalTrackClass: 'track',
			verticalHandleClass: 'handle',
			minScrollbarLength: 28
		});
	}
});